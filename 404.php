<?php /*
PAGE TEMPLATE WHEN BROKEN LINK USED
*/ ?>

<?php get_header(); ?>

<div class="wrap">
	<article class="post">
		<div class="entry" style="text-align: center;">
			<h2>Page Not Found</h2>
			<p>We're sorry, but the page you're requesting cannot be found. Please <a href="<?php echo home_url(); ?>">click here</a> to return to the home page and try again.</p>
		
		</div>
	</article>
</div>

<?php get_footer(); ?>