<?php /*
Template Name: Friendbuy
*/

get_header(); ?>
<?php if ( is_user_logged_in() ) { ?>
	<div class="friendbuy-dz7-mwz"></div>
	<script>
			window['friendbuy'] = window['friendbuy'] || [];
			window['friendbuy'].push(['widget', "dz7-mwz"]);
	</script>
<?php } else { ?>
	<h2 style="text-align: center; padding: 1em 0 0 0;">To refer a friend, you must be logged in.</h2>
	<a href="/my-account" class="btn" style="margin: 1em auto 2em auto; display: table;">Login Now</a>
<?php } ?>
<?php get_footer('new'); ?>

