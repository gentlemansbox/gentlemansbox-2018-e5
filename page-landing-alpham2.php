<?php /* Template Name: AlphaM2 Landing */ ?>

 <?php 
 //
 // IS THIS TEMPLATE NEEDED? | REMOVE
 //
 ?>

 

<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

	<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">
	<meta name="google-site-verification" content="wR2EUsYVd5w3rq-K6svDz8l6FH_16EG7-2dDi5DK52Q" />
	<meta charset="<?php bloginfo('charset'); ?>">
	
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">
	
	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.png"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<!-- Pinterest Verification -->
	<meta name="p:domain_verify" content="913427df30aef7b573fb64ddebbcbd01"/>

    <!--Apple Mobile Web App Metadata-->	
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch4.png" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch5.png" />
	<link rel="apple-touch-startup-image" media="(device-width: 375px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6.png">
	<link rel="apple-touch-startup-image" media="(device-width: 414px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6plus.png">
    <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-landscape.png"  />
    <link rel="apple-touch-startup-image"  media="(device-width: 768px) and (device-height: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-portrait.png"  />
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/library/images/touch-icon.png">


	<!-- CSS -->
	<link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
	<link rel="profile" href="//gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="stylesheet" type="text/css" href="../wp-content/themes/gentlemansbox/library/css/alpham2.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script>

	 // DOM ready
	 $(function() {
	   
      // Create the dropdown base
      $("<select />").appendTo("nav#countrylist");
      
      // Create default option "Go to..."
      $("<option />", {
         "selected": "selected",
         "value"   : "",
         "text"    : "Ships to..."
      }).appendTo("nav#countrylist select");
      
      // Populate dropdown with menu items
      $("nav#countrylist a").each(function() {
       var el = $(this);
       $("<option />", {
           "value"   : el.attr("href"),
           "text"    : el.text()
       }).appendTo("nav#countrylist select");
      });
      
	   // To make dropdown actually work
	   // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
      $("nav#countrylist select").change(function() {
        window.location = $(this).find("option:selected").val();
      });
	 
	 });


	</script>

	<?php if (is_page(406)){ ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/fontbomb/qcc5ovd.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php } ?>

<!-- Hotjar Tracking Code for gentlemansbox.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:72091,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	<!-- start Mixpanel -->
	<script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
	for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
	mixpanel.init("e05fe5f9ba5b2cf88e4b9d8119040f16");</script>
	<!-- end Mixpanel -->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '726903087431698');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=726903087431698&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- CONTACT THANK YOU PAGE -->
	<?php if(is_page(145)){ ?>
	<!-- Facebook Subscription Pixel -->
	<script>fbq('track','Contact');</script>
	<?php } else {
		//nothing
	} ?>

<script>(function (u, n, i, v, e, r, s, a, l) {u[r] = {}; u[r].uid = 'e60cab98-2e6f-40b7-81a7-9d42cc738356';a = n.createElement(v); a.src = e; a.async = s;n.getElementsByTagName(i)[0].appendChild(a);})(window, document, 'head', 'script', 'https://cdn.getambassador.com/us.js', 'mbsy', true);</script>


	<?php wp_head(); ?>



</head>
<body <?php body_class(); ?>>

	<header class="alpham2-header">
		<div class="overlay">
		<h1 class="alpham2-h1">Discover yourself as a <br> true gentleman for just $25</h1>
		<p class="alpham2-p">Convenience and value delivered right to your door.</p>
		<a href="https://gentlemansbox.com/join/" class="alpham2-join">JOIN NOW</a>
		</div>
	</header>
	<main class="alpham2-main">
		<div class="alpham2-checkout">
			<h2 class="alpham2-h2">CHECK OUT ALPHA MALE'S LATEST VIDEO FEATURING GENTLEMAN'S BOX & USE PROMO CODE "ALPHA5" FOR $5 OFF YOUR FIRST MONTH'S BOX!</h2>
			<img src="https://gentlemansbox.com/wp-content/uploads/2016/11/AlphaM3.png">			
		</div>
		<div class="alpham2-subscribe">
			<h3 class="alpham2-h3">ONE BOX. ALL THE ESSENTIALS.</h3>
			<div class="alpham2-joinnow">
				<img src="../wp-content/themes/gentlemansbox/library/images/whattoexpectbox.png">
				<a href="https://gentlemansbox.com/join/" class="alpham2-join">JOIN NOW</a>
			</div>
			<div class="alpham2-monthly">
				<a href="https://gentlemansbox.com/?add-to-cart=64660&quantity=1" class="alpham2-month-join">
				<img src="../wp-content/themes/gentlemansbox/library/images/monthly-plan.png">
				</a>
			</div>
			<div class="alpham2-annual">
				<a href="https://gentlemansbox.com/?add-to-cart=76&quantity=1" class="alpham2-annual-join">
				<img src="../wp-content/themes/gentlemansbox/library/images/annual-plan.png">
				</a>
			</div>
		</div>
		<div style="overflow:hidden;height:440px;width:75%;max-width:100%;max-height:100%;margin-left:13%"><iframe style="height:100%;width:100%;max-width:100%;border:0;" frameborder="0" src="https://www.youtube.com/embed/tZHRGgfcsqc"></iframe></div><style>#youtube_canvas img{max-width:none!important;background:none!important}</style></div>
	</main>
	<footer class="alpham2-footer">




	</footer>	
<?php wp_footer(); ?>
<!-- JS -->
<script src="<?php bloginfo('template_directory'); ?>/library/js/kissmetrics.js"></script>

<?php if (is_page(406)){ ?>
<script>
window.FONTBOMB_HIDE_CONFIRMATION = true;
(function () {try{var s = document.createElement('script');s.setAttribute('src', '<?php bloginfo('template_url'); ?>/library/js/fontbomb/main.js');document.body.appendChild(s);}catch(err){alert("Your browser is not compatible, watch the video or try with Chrome.")}}());
</script>
<?php } ?>
<script src="//assets.pcrl.co/js/jstracker.min.js"></script>


</body>
</html>