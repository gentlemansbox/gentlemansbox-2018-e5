<?php /*
Template Name: Groomsmen
*/

get_header(); ?>
<main class="full-width">
  <section class="landing-top">
    <div class="background clearfix">
      <div class="full-bg full-width" style="background-image:url(<?php the_field('full_image'); ?>);">
      </div>
    </div>
    <div class="page-title vertical-align-parent">
      <div class="vertical-align-content">
        <?php the_field('main_title'); ?>
      </div>
    </div>
  </section>
  <section class="give-try dark-bg">
    <h2>Our Groomsmen Packages</h2>
  </section>
  <!-- 3 TIERS -->
  <div class="fh5co-section section3 landing-3-column">
    <div class="container">
      <div class="row">
        <?php if( have_rows('3_column') ) {
          while ( have_rows('3_column') ) : the_row();
            $image = get_sub_field('step_image');?>
            <div class="col-md-4 col-sm-12">
              <figure>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <figcaption>
                  <h4><?php the_sub_field('step_title') ?></h4>
                  <p><?php the_sub_field('step_content') ?></p>
                </figcaption>
              </figure>
            </div>
          <?php endwhile;
        } else {
            // NOTHING
        } ?>
      </div>
    </div>
  </div>
  <!-- HOW IT WORKS -->
  <?php if ( have_rows('blocks_b4') ): ?>
  <div class="nprows nprows2">
      <div class="wrp">
          <h2 class="nprows__title"><?php the_field('title_b4'); ?></h2>
          <h4 class="nprows__subtitle"><?php the_field('subtitle_b4'); ?></h4>
          <div class="nprows__row">
          <?php $i = 1; ?>
          <?php while ( have_rows('blocks_b4') ): the_row(); ?>
              <div class="nprows__el">
                  <div class="nblock">
                      <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                      <span class="block__num"><?php echo $i; ?></span>
                      <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                      <p class="block__subtitle"><?php the_sub_field('text'); ?></p>
                  </div>
              </div>
              <?php $i++; ?>
          <?php endwhile; ?>
          </div>
      </div>
  </div>
  <?php endif; ?>
  <!-- CONTACT FORM -->
  <?php if ( $post->post_content=="" ) { ?>
    <?php // NOTHING ?>
  <?php } else { ?>
    <section class="full-width grey-bg">
      <div class="max-width">
       <?php the_content(); ?>
      </div>
    </section>
  <?php } ?>
</main>
<?php get_footer('new'); ?>
