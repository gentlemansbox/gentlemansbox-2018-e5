<?php
/*
* Template Name: Home Page - new template
*/
get_header(); ?>

<?php
    $bgcolor = get_field( 'background_color_b1' );
    if ( !$bgcolor) {
        $bgcolor = '#fff';
    }
?>
<div id="topbanner" style="background-color:<?php echo $bgcolor; ?>;">
    <div class="wrp">
        <div class="banner_cont">
            <img class="banner_text__img banner_text__img__m" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
            <div class="banner_text">
                <h3 class="banner_text__subtitle"><span style="background-color:<?php echo $bgcolor; ?>;"><?php the_field( 'banner_subtitle_b1' ); ?></span></h3>
                <h1 class="banner_text__title"><?php the_field( 'banner_title_b1' ); ?></h1>
                <p class="banner_text__text"><?php the_field( 'banner_text_b1' ); ?></p>
                <p class="banner_text__button"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn"><?php the_field( 'button_text_b1' ); ?></a></p>
            </div>
            <img class="banner_text__img banner_text__img__d" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
        </div>
    </div>
</div>

<?php
    $quotest = new WP_Query( array( 'post_type' => 'testimonial', 'orderby' => 'rand' ) );
    if ( $quotest->have_posts() ):
?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php if ( have_rows('blocks_b2') ): ?>
<div class="nprows nprows1">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('title_b2'); ?></h2>
        <h4 class="nprows__subtitle"><?php the_field('subtitle_b2'); ?></h4>
        <div class="nprows__row">
        <?php while ( have_rows('blocks_b2') ): the_row(); ?>
            <div class="nprows__el">
                <div class="nblock">
                    <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                    <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                    <p class="block__subtitle"><?php the_sub_field('subtitle'); ?></p>
                    <a class="block__link" href="<?php the_sub_field('link'); ?>"></a>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="nprowsb">
    <div class="wrp">
        <div class="nprowsb__cont">
            <img class="nprowsb__cont__img" src="<?php the_field('bottom_logo_b2'); ?>" alt="" title="" />
            <?php the_field('bottom_text_b2'); ?>
        </div>
    </div>
</div>

<div id="ntesti">
    <div class="wrp">
        <div class="ntesti_init">
        <div style="min-height: 100px; overflow: hidden;" class="shopperapproved_widget sa_rotate sa_count3 sa_horizontal sa_count3 sa_bgGray sa_colorBlack sa_borderGray sa_noborder sa_mdY sa_fill sa_showlinks sa_large"></div><script type="text/javascript">var sa_interval = 5000;function saLoadScript(src) { var js = window.document.createElement('script'); js.src = src; js.type = 'text/javascript'; document.getElementsByTagName("head")[0].appendChild(js); } if (typeof(shopper_first) == 'undefined') saLoadScript('//www.shopperapproved.com/widgets/testimonial/3.0/27452.js'); shopper_first = true; </script><div style="text-align:right;"><a class="sa_footer" href="https://www.shopperapproved.com/reviews/gentlemansbox.com/" target="_blank" rel="nofollow"><img class="sa_widget_footer" style="border: 0;" alt="" src=https://www.shopperapproved.com/widgets/widgetfooter-darklogo.png></a></div>        </div>
    </div>
</div>

<?php if ( have_rows('blocks_b3') ): ?>
<div class="winside" style="background-image:url('<?php the_field('background_image_b3'); ?>');">
    <div class="wrp">
        <h2 class="winside__title"><?php the_field('title_b3'); ?></h2>
        <div class="pcont dsctp_block">
        <?php $i = 1; ?>
        <?php while ( have_rows('blocks_b3') ): the_row(); ?>
            <div class="pblock pblock<?php echo $i; ?>">
                <?php $title = str_replace( "<br/>", " ", get_sub_field('title') ); ?>
                <img class="pblock__img" src="<?php the_sub_field('image'); ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
                <h4 class="pblock__title"><?php the_sub_field('title'); ?></h4>
            </div>
            <?php $i++; ?>
        <?php endwhile; ?>
        </div>
        
        <div class="pcont table_block mobile_block">
        <?php $i = 1; ?>
        <?php $j = 1; ?>
        <?php while ( have_rows('blocks_b3') ): the_row(); ?>
            <?php if ( $i == 1 || $i == 2 || $i == 5 ) { ?><div class="pblock__mobile__cont pblock__mobile__cont<?php echo $j; ?>"><?php } ?>
            <div class="pblock pblock<?php echo $i; ?>">
                <?php $title = str_replace( "<br/>", " ", get_sub_field('title') ); ?>
                <img class="pblock__img" src="<?php the_sub_field('image'); ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
                <h4 class="pblock__title"><?php the_sub_field('title'); ?></h4>
            </div>
            <?php if ( $i == 1 || $i == 4 || $i == 7 ) { ?>
                <?php $j++; ?>
                </div>
            <?php } ?>
            <?php $i++; ?>
        <?php endwhile; ?>
        </div>
        
        <p class="winside__bsec"><a class="nbtn" href="<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a></p>
    </div>
</div>
<?php endif; ?>

<?php if ( have_rows('blocks_b4') ): ?>
<div class="nprows nprows2">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('title_b4'); ?></h2>
        <h4 class="nprows__subtitle"><?php the_field('subtitle_b4'); ?></h4>
        <div class="nprows__row">
        <?php $i = 1; ?>
        <?php while ( have_rows('blocks_b4') ): the_row(); ?>
            <div class="nprows__el">
                <div class="nblock">
                    <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                    <span class="block__num"><?php echo $i; ?></span>
                    <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                    <p class="block__subtitle"><?php the_sub_field('text'); ?></p>
                </div>
            </div>
            <?php $i++; ?>
        <?php endwhile; ?>
        </div>
        <p class="nprows__bsec"><a class="nbtn" href="<?php the_field('button_link_b4'); ?>"><?php the_field('button_texrt_b4'); ?></a></p>
    </div>
</div>
<?php endif; ?>

<?php if ( have_rows('blocks_b5') ): ?>
<div class="nprows nprows3">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('title_b5'); ?></h2>
        <h4 class="nprows__subtitle"><?php the_field('subtitle_b5'); ?></h4>
        <div class="nprows__row">
        <?php while ( have_rows('blocks_b5') ): the_row(); ?>
            <div class="nprows__el">
                <div class="nblock">
                    <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                    <p class="block__subtitle"><?php the_sub_field('subtitle'); ?></p>
                    <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                    <a class="block__link" href="<?php the_sub_field('link'); ?>"></a>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
        <p class="nprows__bsec"><a class="nbtn" href="<?php the_field('button_link_b5'); ?>"><?php the_field('button_text_b5'); ?></a></p>
    </div>
</div>
<?php endif; ?>

<?php $last_posts = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3 ) ); ?>
<?php if ( $last_posts->have_posts() ): ?>
<div class="nprows nprows4">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('title_b6'); ?></h2>
        <div class="nprows__row">
        <?php while ( $last_posts->have_posts() ): $last_posts->the_post(); ?>
            <div class="nprows__el">
                <div class="nblock">
                <a class="readmore" href="<?php the_permalink(); ?>" style="color:#808080;">
                    <?php if ( has_post_thumbnail() ) { the_post_thumbnail( array(318, 178) ); } else { ?><div class="nblock__withimg"></div><?php } ?>
                    <span class="block__num"><?php echo get_the_date('F j'); ?></span>
                    <h3 class="block__title"><?php the_title(); ?></h3>
                    <?php $excerpt = str_replace(array("&nbsp;", "\n"), "", get_the_excerpt()); ?>
                    <p class="block__subtitle"><?php echo $excerpt; ?>read more</a></p>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php get_footer('new'); ?>
