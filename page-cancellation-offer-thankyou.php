<?php
/**
 * Template Name: Cancellation offer thankyou
 */
 get_header(); 
 

 $order_id = $_REQUEST['order_id'];
 
 global $wpdb;
    $order = new WC_Order( $order_id );
    $items = $order->get_items(); 
  //echo "<pre>";
  //print_r($items);
  foreach ($items as $key => $product )
  {
	  $item_id = $key;
	}
	//echo $item_id;
 wc_update_order_item_meta( $item_id, '_subscription_recurring_amount', '20' );
 wc_update_order_item_meta( $item_id, '_recurring_line_total', '20' );
 wc_update_order_item_meta( $item_id, '_recurring_line_subtotal', '20' );
 wc_update_order_item_meta( $item_id, '_line_subtotal', '20' );
 wc_update_order_item_meta( $item_id, '_line_total', '20' );
 update_post_meta($order_id, '_order_total', '20.00');
 update_post_meta($order_id, '_order_recurring_total', '20');
 
 ?>

<div class="wrap">
	
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</article>
	<?php endwhile; endif; ?>
</div>

<!-- Bing -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4044369"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4044369&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<?php get_footer(); ?>
