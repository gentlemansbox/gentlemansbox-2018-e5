<?php
/**
 * Template Name: Gift 2015
 */
 get_header();
 global $post;
 ?>
<div class="wrap">

    <div id="countrycontainer">
<?php if ( is_page(25966) || is_page(629780)) { ?>
        <div class="shipto gift-subscription">
          <?php } else { ?>
            <div class="shipto international">
          <?php } ?>
            <a href="#">Ship to</a>
        </div>

        <!-- US is the Default Join Page -->
        <div id="countrylist">
            <ul>
            <style>

			#countrycontainer .International a{background:url(<?php bloginfo('stylesheet_directory'); ?>/library/images/international-poly-flag.jpg) no-repeat center right 15px;background-size:21% 81%;}
@media (max-width:480px){#countrycontainer .india a{background-size:contain;}}
			</style>


                <li class="united-states gift-2015 gift <?php if ( is_page(25966) || is_page(629780)) { echo 'active'; } ?>"><a href="<?php echo get_permalink(25966); ?>">United States</a></li>
                 <li class="International gift-2015 <?php if ( is_page(64276) || is_page(645663)) { echo 'active'; } ?>"><a href="<?php echo get_permalink(64276); ?>">International</a></li>
        </div><!--#countrylist-->

        <div class="gift">

            <!-- Conditional Statements to Pull Products -->
            <?php
			global $post;
                if(is_page(25966)): include (TEMPLATEPATH . '/library/partials/gift/united-states.php' );
				        elseif(is_page( 64276 )):include (TEMPLATEPATH . '/library/partials/gift/international.php' );
                elseif(is_page( 629780 )):include (TEMPLATEPATH . '/library/partials/gift/united-states-b.php' );
                elseif(is_page( 645663 )):include (TEMPLATEPATH . '/library/partials/gift/international-b.php' );
                else : include (TEMPLATEPATH . '/library/partials/gift/international.php' );
            endif; ?>

        </div>


    </div><!--#countrycontainer-->
</div>


<?php get_footer(); ?>
