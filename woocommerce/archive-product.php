<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="wrap shop-page-wrapper">
<div class="bread_crum"><?php /*if( function_exists('woocommerce_breadcrumb_gentleman') ) { echo woocommerce_breadcrumb_gentleman();  }*/ ?></div>
  <div class="sidebar" id="sidebar-left-show">
    <?php 
	
	///echo "I am here..:)*********";
	
	
	if ( is_active_sidebar( 'shop_page_sidebar' ) ) : ?>
    <div class="widget-area" role="complementary">
      <ul class="shop_page-left-sidebar">
        <?php //woocommerce_cart_totals(); ?>
        <?php dynamic_sidebar( 'shop_page_sidebar' ); ?>
      </ul>
    </div>
    <?php endif; ?>
  </div>
  <div class="right-main-container">
    <?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
    <h1 class="page-title">
      <?php woocommerce_page_title(); ?>
    </h1>
    <?php endif; ?>
    <?php do_action( 'woocommerce_archive_description' ); ?>
    <div class="clearfix group-top-shop-now-box">
      <?php
	   // Get The Category Of Products
	   $taxonomies = array('product_cat');
	   $args = array(
					'orderby'           => 'name', 
					'order'             => 'ASC',
					'hide_empty'        => true, 
					'exclude'           => array(), 
					'exclude_tree'      => array(), 
					'include'           => array(),
					'number'            => '', 
					'fields'            => 'all', 
					'slug'              => '',
					'parent'            => '',
					'hierarchical'      => true, 
					'child_of'          => 0,
					'childless'         => false,
					'get'               => '', 
					'name__like'        => '',
					'description__like' => '',
					'pad_counts'        => false, 
					'offset'            => '', 
					'search'            => '', 
					'cache_domain'      => 'core'
					); 
		if(!is_tax('product_cat'))
		{
					
		$terms = get_terms($taxonomies, $args);
		if(!empty($terms))
		{
			foreach($terms as $key=>$term)
			{
				if($key > 2)
				{
					continue;
				}
				$term_link =  get_term_link( $term, 'product_cat' );
	  	 	 	?>	
             	<div class="one-third top-shop-now-box">
                	<span class="top-heading-top-products"><?php echo $term->name; ?></span>
                    <span class="price-text-top-product"><?php echo $term->description; ?></span>				 <a class="button add-to-cart-button" href="<?php echo $term_link; ?>">SHOP NOW</a>
               </div>
             <?php
			}
		}
		}
		?>    
    </div>
    <div class="products_html clearfix">
    	<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
	  
	  //All Products show here.
	if ( have_posts() ) { ?>
      <?php ///do_action( 'woocommerce_before_shop_loop' );?>
      <?php //woocommerce_product_loop_start(); ?>
      <?php // woocommerce_product_subcategories(); ?>
      <?php while ( have_posts() ) { 
	  		the_post();
	  		global $post;
			
			$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
			$feat_image_src = $full_image_url[0];
	   ?>
      <div class="main-product-box-outer">
        <div class="button-hover-show-box">
          <div class="product-box">
            <div class="image-box-product">
              <?php if(!empty($feat_image_src)){ ?>
              <a href="<?php the_permalink(); ?>"><img src="<?php echo $feat_image_src; ?>" /></a>
              <?php } ?>
            </div>
            <div class="bottom-desc-box" onclick="window.location.href='<?php the_permalink(); ?>';">
              <p class="product_title">
                <?php the_title() ?>
              </p>
              <p class="product-price-text">
                <?php 
			    $product = new WC_Product( get_the_ID() );
				$price = $product->price;
				echo  wc_price($price);
				?>
              </p>
              
			<?php 
				
					//global $product;
					//$product = new WC_Product( $post->ID );
					//$price = $product->price; 
					//echo $product->get_rating_html();
					
            	?>
                    <!--<span class="rating-star">
                    <?php //woocommerce_get_template( '/loop/rating.php' ); ?>
                    </span>-->
               <?php
               
                ?>
                
                
              <div><span class="rating-star"> <?php woocommerce_get_template( '/loop/rating.php' ); ?></span></div>
            </div>
          </div>
          <div class="buttons-box"> <a href="<?php the_permalink(); ?>" class="product-box-cart-button view-button-product">
            <?php _e('View'); ?>
            </a> 
            <?php woocommerce_get_template( '/loop/add-to-cart.php' ); ?>
            </div>
        </div>
      </div>
      <?php } // end of the loop. ?>
      <?php //woocommerce_product_loop_end(); ?>
      <?php //do_action( 'woocommerce_after_shop_loop' ); ?>
      <?php }elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) { ?>
      <?php wc_get_template( 'loop/no-products-found.php' ); ?>
      <?php };
	
	  ?>
    </div>
    <?php do_action( 'woocommerce_after_shop_loop' ); ?>
    <?php //wc_get_template( 'loop/no-products-found.php' ); ?>
    <?php
		
		do_action( 'woocommerce_after_main_content' );
	?>
  </div>
  <div class="sidebar" id="sidebar-right-show">
    <?php 
	
	///echo "I am here..:)*********";
	
	
	if ( is_active_sidebar( 'shop_page_sidebar' ) ) : ?>
	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

    <div  class="widget-area" role="complementary">
      <ul class="shop_page-left-sidebar">
        <?php dynamic_sidebar( 'shop_page_sidebar' ); ?>
      </ul>
    </div>
    <?php endif; ?>
  </div>
</div>
</div>
<?php get_footer(); ?>
