<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! $order = wc_get_order( $order_id ) ) {
	return;
}
$order = wc_get_order( $order_id );
$coupon = $order->get_used_coupons();
$items = $order->get_items();
?> 

<?php 
$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template( 'order/order-downloads.php', array( 'downloads' => $downloads, 'show_title' => true ) );
}
?>
<?php if(is_page(6)) { ?>
<!-- SHOPPER APPROVED -->
	<div id="outer_shopper_approved"></div> 
	<script type="text/javascript"> 
		<?php global $current_user;
			get_currentuserinfo();
			$email= $current_user->user_email; //SET CURRENT USER EMAIL ADDRESS
			$name = $order->get_formatted_billing_full_name();
		?> 
		var sa_values = { "site":27452, "token":"810VsfxW", 'orderid':'<?php echo $order->get_order_number(); ?>', 'name':'<?php echo $name; ?>', 'email':'<?php echo $email; ?>' }; function saLoadScript(src) { var js = window.document.createElement("script"); js.src = src; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); } var d = new Date(); if (d.getTime() - 172800000 > 1477399567000) saLoadScript("//www.shopperapproved.com/thankyou/inline/27452.js"); else saLoadScript("//direct.shopperapproved.com/thankyou/inline/27452.js?d=" + d.getTime()); 
	</script>
<!-- SHOPPER APPROVED -->
<?php } ?>

<section class="woocommerce-order-details">
<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>
	<h2 class="woocommerce-order-details__title"><?php _e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );
				foreach ( $order_items as $item_id => $item ) {
					$product = $item->get_product();

					wc_get_template( 'order/order-details-item.php', array(
						'order'			     => $order,
						'item_id'		     => $item_id,
						'item'			     => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'	     => $product ? $product->get_purchase_note() : '',
						'product'	         => $product,
					) );
				}
			?>
			<?php do_action( 'woocommerce_order_details_after_order_table_items', $order ); ?>
		</tbody>

		<tfoot>
			<?php
				foreach ( $order->get_order_item_totals() as $key => $total ) {
					?>
					<tr>
						<th scope="row"><?php echo $total['label']; ?></th>
						<td><?php echo $total['value']; ?></td>
					</tr>
					<?php
				}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php _e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wptexturize( $order->get_customer_note() ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>

<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
?>

<?php if(is_page(6)){ ?>

<!--
Impact Radius Universal Tracking Tag required to be installed on the page.
Removal or modification of this code will disrupt marketing activities. This code is property of Impact Radius, please do not remove or modify without first contacting Impact Radius Technical Services.
-->
<script type="text/javascript">

ire('trackConversion', 11308, {
                        orderId: "<?php echo $order->get_order_number(); ?>",
												orderPromoCode:"<?php echo $coupon[0]; ?>",
                         items: [
                        	<?php foreach($items as $item){
                       			$product_id =$product_id = $item['product_id'];
                       			$product = new WC_Product($product_id);
    												$sku = $product->get_sku();
    												$terms = get_the_terms( $product_id, 'product_cat' );
    													foreach ($terms as $key => $term) {
    														$product_cat=$term->slug;
    								}

    							?>
                           {

                            subTotal:<?php echo $item['subtotal']; ?>,
                            category: "<?php echo $product_cat; ?>",
                            sku: "<?php echo $sku; ?>",
                            quantity: <?php echo $item['qty']; ?>
                           },
                           <?php } ?>
                        ]
}
);
</script>
	<img src="//cdsusa.veinteractive.com/DataReceiverService.asmx/Pixel?journeycode=9CE527F9-2408-429D-BAFC-D0DF9C1B7854" width="1" height="1"/>
	<img src="//cdsusa.veinteractive.com/DataReceiverService.asmx/Pixel?journeycode=9CE527F9-2408-429D-BAFC-D0DF9C1B7854" width="1" height="1"/>


<!--SNAP PURCHASE -->
	<script type="text/javascript">
	snaptr('track', 'PURCHASE', {
		'currency': 'USD',
		'price': <?php echo $order->get_total(); ?>,
		'item_ids': [
			<?php foreach($items as $item){
				$product_id =$product_id = $item['product_id']; ?>
				'<?php echo $product_id; ?>'
			<?php } ?>
			],
		'number_items': <?php echo count($items);?>,
		'transaction_id':'<?php echo $order->get_order_number(); ?>' });
	</script>
	<!-- SNAP PURCHASE -->

<!-- CRITEO TAG FOR SALES TRANSACTION -->
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
		//SET CURRENT USER EMAIL ADDRESS
			<?php global $current_user;
				get_currentuserinfo();
				$email= $current_user->user_email;
			?>
			window.criteo_q = window.criteo_q || [];
			var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
			window.criteo_q.push(
				{ event: "setAccount", account: 36897 },
				{ event: "setEmail", email: "<?php echo $email; ?>" },
				{ event: "setSiteType", type: deviceType },
				{ event: "trackTransaction", id: <?php echo $order->get_order_number(); ?>,
					item: [
						<?php foreach($items as $item){
                       		$product_id =$product_id = $item['product_id'];
                       		$_line_subtotal = $item['subtotal'];
                       		$quantity= $item['qty'];
                       		$price = $_line_subtotal/$quantity;
                       	?>
   							{id: <?php echo $product_id; ?>, price:<?php echo $price ?>,quantity: <?php echo $quantity; ?> },
    					<?php } ?>
					]}
			);
		</script>
<!-- FRIENDBUY CONVERSION TRACKER -->
		<script>
		//SET CURRENT USER EMAIL ADDRESS
			<?php global $current_user;
				get_currentuserinfo();
				$email= $current_user->user_email;
			?>
				window['friendbuy'] = window['friendbuy'] || [];
				window['friendbuy'].push(['track', 'order',
					{
						id: '<?php echo $order->get_order_number(); ?>', //INPUT ORDER ID
						amount: '<?php echo $order->get_total(); ?>', //INPUT ORDER AMOUNT
						coupon_code: '<?php echo $coupon[0]; ?>', //OPTIONAL, coupon code if used for order
						// new_customer: false, //OPTIONAL, true if this is the customer's first purchase
						email: '<?php echo $email; ?>' //INPUT EMAIL
					}
				]);
		</script>
<!-- FRIENDBUY CONVERSION TRACKER -->
<?php if ( is_user_logged_in() ) { ?>
<!-- FRIENDBUY POST PURCHASE OVERLAY -->
<script>
   window['friendbuy'] = window['friendbuy'] || [];
   window['friendbuy'].push(['widget', "dz7-mwA"]);
</script>
<div class="friendbuy-dz7-mwA"></div>
<!-- FRIENDBUY POST PURCHASE OVERLAY -->
<?php } ?>
<!-- FACEBOOK PIXEL FOR SALES TRANSACTIONS -->
<script type="text/javascript">
fbq('track', 'Purchase', {
value: <?php echo $order->get_total(); ?>,
currency: 'USD',
order_id: "<?php echo $order->get_order_number(); ?>"
});

</script>
<!-- FACEBOOK PIXEL FOR SALES TRANSACTIONS -->
<!-- PINTEREST PIXEL FOR SALES TRANSACTIONS -->
<script>
pintrk('track', 'checkout', {
	order_id: "<?php echo $order->get_order_number(); ?>",
	value: <?php echo $order->get_total(); ?>,
	order_quantity: <?php echo count($items);?>,
	currency: 'USD',
	promo_code: '<?php echo $coupon[0]; ?>',
	line_items: [
		<?php foreach($items as $item){
			$product_id =$product_id = $item['product_id'];
			$_line_subtotal = $item['subtotal'];
			$quantity= $item['qty'];
			$price = $_line_subtotal/$quantity;
			?>
		{
			product_id: <?php echo $product_id; ?>,
			product_price: <?php echo $price; ?>,
			product_quantity: <?php echo $quantity; ?>
		},
		<?php } ?>
	]
});
</script>
<!-- PINTEREST PIXEL FOR SALES TRANSACTIONS -->


<!-- Start Quantcast Tag -->
<script type="text/javascript">
 var _qevents = _qevents || [];

 (function(){
   var elem = document.createElement('script');
   elem.src = (document.location.protocol == 'https:' ? 'https://secure' : 'http://edge') + '.quantserve.com/quant.js';
   elem.async = true;
   elem.type = 'text/javascript';
   var scpt = document.getElementsByTagName('script')[0];
   scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({qacct:'p-82-8dhfPge5nN',
labels:'_fp.event.Subscription Confirmation',
orderid:'<?php echo $order->get_order_number(); ?>',
revenue:'<?php echo $order->get_total(); ?>',
event:'refresh'}
);

</script>
<noscript>
<img src="//pixel.quantserve.com/pixel/p-82-8dhfPge5nN.gif?labels=_fp.event.Subscription+Confirmation&orderid=<?php echo $order->get_order_number(); ?>+ID&revenue=<?php echo $order->get_total(); ?>" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
</noscript>
<!-- End Quantcast tag -->

<!-- GLOBAL WIDE MEDIA CONVERSION PIXEL -->
<img src="http://convert.ctypy.com/aff_l?offer_id=11251" width="1" height="1" />
<!-- GLOBAL WIDE MEDIA CONVERSION PIXEL -->

<!-- OUTBRAIN CONVERSION -->
<script type="text/javascript">
obApi('track', 'Purchase', {
	orderValue:'<?php echo $order->get_total(); ?>',
	currency: 'USD',
	orderId: '<?php echo $order->get_order_number(); ?>'
});
</script>
<!-- OUTBRAIN CONVERSION -->

<!-- FOSINA TRACKING PIXEL -->
<iframe src="https://afftracr.com/p.ashx?o=14311&e=105&t=<?php echo $order->get_order_number(); ?>" height="1" width="1" frameborder="0"></iframe>
<!-- FOSINA TRACKING PIXEL -->

<!-- Google Code for Purchases Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 973864522;
var google_conversion_label = "rkWFCK-eu4ABEMr8r9AD";
var google_conversion_value = <?php echo $order->get_total(); ?>;
var google_conversion_currency = "USD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/973864522/?value=0.00&amp;currency_code=USD&amp;label=rkWFCK-eu4ABEMr8r9AD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Purchases Conversion Page -->
<?php } ?>