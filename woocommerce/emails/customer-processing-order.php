<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<table>
	<tr>
		<td>
			<p>Your order has been received and is now being processed!</p>

<p>Let's be transparent for a minute. We truly appreciate each customer that orders with us. So much, that we celebrate each time someone new joins the team! Don't believe us? See for yourself by clicking on our team below <a href = "https://gentlemansbox.com/welcome/" target="_blank">or click here!</a></p></p>
<br>
<br>
<a href = "https://gentlemansbox.com/welcome/" target="_blank"><img src="https://gentlemansbox.com/wp-content/uploads/2017/03/GBOX-Excited-No-Cap.gif" alt="" width="480" height="270" class="aligncenter size-full wp-image-355549" /></a></p>
<br>
<br>
<p>For <strong>subscriptions & gift subscriptions</strong>: Any subscriptions placed between the 10th and last day of the month will result in your first box being shipped in 1-3 business days. If your subscription order is placed between 12AM PST on the first of the month and the 9th, your subscription will start with the new month and ship between the 10th and 13th.
<br>
Your subscription to GQ Magazine will be distributed by GQ magazine and begin to arrive in 4 to 6 weeks for U.S members (6 to 10 weeks for Canadian members). GQ service is not available outside of the US and Canada.</p>
<p>For <strong>store orders & non-subscription orders</strong>: Orders will take 1-3 business days to process and ship out from the day you place your order. GQ magazines are not included in non-subscription orders.</p>

<p>Order as a gift? You can download this certificate to give to your recipient if you wish!<a href = "https://gentlemansbox.com/wp-content/uploads/2017/02/gentlemansboxontheway.pdf" target="_blank"> Download Certificate</a></p>

			<p>Your order details are shown below for your reference:</p>
		</td>
	</tr>
</table>

<?php
/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
