<?php get_header(); ?>

<?php

// Function from WP Instagram Widget plugin by Scott Evans
// https://wordpress.org/plugins/wp-instagram-widget/
function scrape_instagram( $username ) {

    $username = strtolower( $username );
    $username = str_replace( '@', '', $username );

    if ( false === ( $instagram = get_transient( 'instagram-a5-'.sanitize_title_with_dashes( $username ) ) ) ) {

        $remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );

        if ( is_wp_error( $remote ) )
            return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'wp-instagram-widget' ) );

        if ( 200 != wp_remote_retrieve_response_code( $remote ) )
            return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'wp-instagram-widget' ) );

        $shards = explode( 'window._sharedData = ', $remote['body'] );
        $insta_json = explode( ';</script>', $shards[1] );
        $insta_array = json_decode( $insta_json[0], TRUE );

        if ( ! $insta_array )
            return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );

        if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
            $images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
        } else {
            return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
        }

        if ( ! is_array( $images ) )
            return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );

        $max_imgs = 10;
        $tmp_index = 1;
        echo "<div class='instcont'>";
        foreach ( $images as $image ) {
            if ( $tmp_index <= $max_imgs ) {
                $large = preg_replace( '/^https?\:/i', '', $image['display_src'] );
                if ( $image['is_video'] == true ) {
                    $addclass = ' vid';
                } else{
                    $addclass = '';
                }
                $src = "https:".$large;
                $link = trailingslashit( '//instagram.com/p/' . $image['code'] );
                echo "<div class='inst_img".$addclass."' style='background-image:url(\"".$src."\");'><img src='".$src."' alt='' title='' /><a href='".$link."' target='_blankl'></a></div>";
                $tmp_index++;
            }
        }
        echo "</div>";
    }
}

?>
<style>
    figure#headerimg {display:none;}
</style>

<div class="nptopbanner">
    <div class="nptopbanner__img" style="background-image:url('<?php the_field('pt_top_image'); ?>');">
        <img src="<?php the_field('pt_top_image'); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
    </div>
    <div class="nptopbanner__cont">
        <div class="vcont">
            <div class="vcont__inc">
                <div class="nptopbanner__sec">
                    <h3 class="nptopbanner__subtitle"><span>The</span></h3>
                    <h1 class="nptopbanner__title"><?php the_title(); ?></h1>
                    <?php if ( get_field('pt_top_button_link') ) { ?>
                        <p class="product woocommerce add_to_cart_inline"><?php the_field('pt_top_price'); ?><a class="add_to_cart_button" href="<?php the_field('pt_top_button_link'); ?>"><?php the_field('pt_top_button_text'); ?></a></p>
                    <?php } else { ?>
                        <?php echo do_shortcode('[add_to_cart id="'.get_the_ID().'" style="border:none;padding:0;margin:0"]'); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nprows nprows2">
  <div class="wrp">
    <div class="nptopbanner__text"><?php the_content(); ?></div>
  </div>
</div>
<!--
<?php if ( have_rows('pt_prod_sec_products') ): ?>
<div class="winside" style="background-image:url('<?php the_field('pt_prod_sec_background_image'); ?>');">
    <div class="wrp">
        <h2 class="winside__title"><?php the_field('pt_prod_sec_title'); ?></h2>
        <div class="pcont dsctp_block">
        <?php $i = 1; ?>
        <?php while ( have_rows('pt_prod_sec_products') ): the_row(); ?>
            <div class="pblock pblock<?php echo $i; ?>">
                <?php $title = str_replace( "<br/>", " ", get_sub_field('title') ); ?>
                <img class="pblock__img" src="<?php the_sub_field('image'); ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
                <h4 class="pblock__title"><?php the_sub_field('title'); ?></h4>
            </div>
            <?php $i++; ?>
        <?php endwhile; ?>
        </div>
        <div class="pcont table_block mobile_block">
        <?php $i = 1; ?>
        <?php $j = 1; ?>
        <?php while ( have_rows('pt_prod_sec_products') ): the_row(); ?>
            <?php if ( $i == 1 || $i == 2 || $i == 5 ) { ?><div class="pblock__mobile__cont pblock__mobile__cont<?php echo $j; ?>"><?php } ?>
            <div class="pblock pblock<?php echo $i; ?>">
                <?php $title = str_replace( "<br/>", " ", get_sub_field('title') ); ?>
                <img class="pblock__img" src="<?php the_sub_field('image'); ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
                <h4 class="pblock__title"><?php the_sub_field('title'); ?></h4>
            </div>
            <?php if ( $i == 1 || $i == 4 || $i == 7 ) { ?>
                <?php $j++; ?>
                </div>
            <?php } ?>
            <?php $i++; ?>
        <?php endwhile; ?>
        </div>
        <p class="winside__bsec"><a class="nbtn" href="<?php the_field('pt_prod_sec_button_link'); ?>"><?php the_field('pt_prod_sec_button_text'); ?></a></p>
    </div>
</div>
<?php endif; ?>
<div class="testiblock">
    <h2 class="testiblock__title"><?php the_field('pt_test_title'); ?></h2>
    <?php if ( have_rows('pt_test_videos') ): ?>
    <div class="testiblock__videos">
        <?php while ( have_rows('pt_test_videos') ): the_row(); ?>
        <div class="tblock">
            <div class="tblock__img">
                <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
            </div>
            <h3 class="tblock__title"><?php the_sub_field('title'); ?></h3>
            <a href="<?php the_sub_field('video'); ?>" target="_blank" class="link"></a>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>

    <?php if ( have_rows('pt_test_reviews') ): ?>
    <div class="testiblock__rew">
        <?php while ( have_rows('pt_test_reviews') ): the_row(); ?>
        <div class="tblock">
            <div class="tblock__meta">
                <span class="tblock__stars tblock__stars<?php the_sub_field('count_of_start'); ?>"></span>
                <span class="tblock__time"><?php the_sub_field('time_text'); ?></span>
            </div>
            <h3 class="tblock__title"><?php the_sub_field('title'); ?></h3>
            <p class="tblock__text"><?php the_sub_field('content'); ?></p>
            <p class="tblock__author"><?php the_sub_field('author'); ?></p>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
    <p class="testiblock__bsec"><a class="nbtn" href="<?php the_field('pt_prod_sec_button_link'); ?>"><?php the_field('pt_prod_sec_button_text'); ?></a></p>
</div>
  -->
<?php if ( have_rows('pt_pb_boxes') ): ?>
<div class="nprows nprows3">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('pt_pb_title'); ?></h2>
        <h4 class="nprows__subtitle"><?php the_field('pt_pb_subtitle'); ?></h4>
        <div class="nprows__row">
        <?php while ( have_rows('pt_pb_boxes') ): the_row(); ?>
            <div class="nprows__el">
                <div class="nblock">
                    <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                    <p class="block__subtitle"><?php the_sub_field('subtitle'); ?></p>
                    <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                    <a class="block__link" href="<?php the_sub_field('link'); ?>"></a>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
        <p class="nprows__bsec"><a class="nbtn" href="<?php the_field('pt_pb_button_link'); ?>"><?php the_field('pt_pb_button_text'); ?></a></p>
    </div>
</div>
<?php endif; ?>

<?php if ( get_field('pt_inst_show_instagram_content') ): ?>
<div class="ptinstbl">
    <h2 class="ptinstbl__title"><?php the_field('pt_inst_title'); ?></h2>
    <?php scrape_instagram(get_field('ptinstagram_name')); ?>
    <p class="ptinstbl__bsec"><a class="nbtn" href="<?php the_field('pt_inst_button_link'); ?>"><?php the_field('pt_inst_button_text'); ?></a></p>
</div>
<?php endif; ?>

<div id="ngpag"></div>
<div id="ngpagcont"><a href="#" class="nclose"></a><div class="cont"></div></div>
<?php get_footer('new'); ?>
