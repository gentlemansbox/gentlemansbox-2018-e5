<?php 
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce;

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form id="woocommerce_checkout" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
    
    <div class="shopbox">
        <p class="shopbox__title">my items</p>
        <?php do_action( 'woocommerce_before_cart_contents' ); ?>
        <?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        ?>
        <div class="sbox <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
            <div class="product-thumbnail">
                <?php
                    $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                    if ( ! $_product->is_visible() ) {
                        echo $thumbnail;
                    } else {
                        printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                    }
                ?>
            </div>
            <div class="productcont">
                <div class="productcont__title">
                    <?php
                        if ( ! $_product->is_visible() ) {
                            echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                        } else {
                            echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                        }
                        echo WC()->cart->get_item_data( $cart_item );
                        if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                            echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                        }
                    ?>
                </div>
                <div class="productcont__meta">
                    <span class="productcont__meta__text">qty:</span>
                    <?php
                        if ( $_product->is_sold_individually() ) {
                            $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                        } else {
                            $product_quantity = woocommerce_quantity_input( array(
                                'input_name'  => "cart[{$cart_item_key}][qty]",
                                'input_value' => $cart_item['quantity'],
                                'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                'min_value'   => '0'
                            ), $_product, false );
                        }
                        echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                    ?>
                    <?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ), esc_attr( $product_id ), esc_attr( $_product->get_sku() ) ), $cart_item_key ); ?>
                </div>
                <div class="prod productcont__price">
                    <span class="text">price:</span> <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
                </div>
                <div class="prod productcont__total">
                    <span class="text">total:</span> <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
                </div>
            </div>
        </div>
        <?php
            }
        }
        do_action( 'woocommerce_cart_contents' );
        ?>
        <?php $tamount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) ); ?>
        <div class="shopbox__subtotalr"><span class="subtotalr__text">subtotal</span> <?php echo $woocommerce->cart->get_cart_total(); ?></div>
        <?php if ( wc_coupons_enabled() ) { ?>
            <div class="coupon">
                <label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
                <?php do_action( 'woocommerce_cart_coupon' ); ?>
            </div>
        <?php } ?>
        <div class="shopbox__btn">
            <input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
            <input type="submit" id="woocommerce_checkout_process" class="checkout-button button alt wc-forward" name="proceed" value="Proceed to Checkout">
            <?php do_action( 'woocommerce_cart_actions' ); ?>
            <?php wp_nonce_field( 'woocommerce-cart' ); ?>
        </div>
        <?php do_action( 'woocommerce_after_cart_contents' ); ?>
    </div>
    
    <table class="shop_table shop_table_responsive cart ncartt" cellspacing="0">
        <thead>
            <tr>
                <th class="product-name" colspan="2"><?php _e( 'Item', 'woocommerce' ); ?></th>
                <th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
                <th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
                <th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
                <th class="product-remove">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php do_action( 'woocommerce_before_cart_contents' ); ?>
            <?php
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                    ?>
                    <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                        <td class="product-thumbnail">
                            <?php
                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                                if ( ! $_product->is_visible() ) {
                                    echo $thumbnail;
                                } else {
                                    printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                                }
                            ?>
                        </td>
                        <td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
                            <?php
                                if ( ! $_product->is_visible() ) {
                                    echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                                } else {
                                    echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                                }
                                // Meta data
                                echo WC()->cart->get_item_data( $cart_item );
                                // Backorder notification
                                if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                    echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                                }
                            ?>
                        </td>
                        <td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
                            <?php
                                echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                            ?>
                        </td>
                        <td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
                            <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {
                                    $product_quantity = woocommerce_quantity_input( array(
                                        'input_name'  => "cart[{$cart_item_key}][qty]",
                                        'input_value' => $cart_item['quantity'],
                                        'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                        'min_value'   => '0'
                                    ), $_product, false );
                                }
                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                            ?>
                        </td>
                        <td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
                            <?php
                                echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                            ?>
                        </td>
                        <td class="product-remove">
                            <?php
                                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                    esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                    __( 'Remove this item', 'woocommerce' ),
                                    esc_attr( $product_id ),
                                    esc_attr( $_product->get_sku() )
                                ), $cart_item_key );
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            do_action( 'woocommerce_cart_contents' );
            ?>
            <tr>
                <td colspan="6" class="addition_space"></td>
            </tr>
            <tr>
                <td colspan="6" class="subtotalr">
                <?php $tamount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) ); ?>
                <span class="subtotalr__text">subtotal</span> <?php echo $woocommerce->cart->get_cart_total(); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="actions">
                    <?php if ( wc_coupons_enabled() ) { ?>
                        <div class="coupon">
                            <label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
                            <?php do_action( 'woocommerce_cart_coupon' ); ?>
                        </div>
                    <?php } ?>
                    <div class="hl-fbm-checkbox" data-type="cart" style="height: 100px;"></div>
                    <input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
                    <input type="submit" id="woocommerce_checkout_process" class="checkout-button button alt wc-forward" name="proceed" value="Proceed to Checkout">
                    <?php do_action( 'woocommerce_cart_actions' ); ?>
                    <?php wp_nonce_field( 'woocommerce-cart' ); ?>
                </td>
            </tr>
            <?php do_action( 'woocommerce_after_cart_contents' ); ?>
        </tbody>
    </table>
    <?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<?php if( have_rows('items', 'options') ): ?>
<div class="npropprod npropprod__m">
    <h2 class="npropprod__title"><?php the_field('cp_addition_products_title', 'option'); ?></h2>
    <h4 class="npropprod__subtitle"><?php the_field('cp_addition_products_subtitle', 'option'); ?></h4>
    <div class="npropprod__row">
    <?php while ( have_rows('items', 'options') ) : the_row(); ?>
        <div class="npropprod__el">
            <div class="box">
                <?php $image = get_sub_field('product_image'); ?>
                <?php if ( $image ) { ?>
                    <div class="box__img"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" /></div>
                <?php } ?>
                <h3 class="box__title"><?php the_sub_field('item_name');?></h3>
                <p class="box__price">$<?php the_sub_field('item_price'); ?></p>
                <p class="box__btncnt"><a href="<?php echo do_shortcode('[add_to_cart_url id="'.get_sub_field('product_id').'"]'); ?>" class="box__btn">Add to Box</a></p>
            </div>
        </div>
    <?php endwhile; ?>
    </div>
</div>
<?php endif; ?>

<?php
$crosssells = WC()->cart->get_cross_sells();
if ( 0 === sizeof( $crosssells ) ) return;
$meta_query = WC()->query->get_meta_query();
$args = array(
    'post_type' => 'product',
    'ignore_sticky_posts' => 1,
    'no_found_rows' => 1,
    'posts_per_page' => apply_filters( 'woocommerce_cross_sells_total', 3 ),
    'post__in' => $crosssells,
    'meta_query' => $meta_query
);
$products = new WP_Query( $args );
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_cross_sells_columns', 3 );
if ( $products->have_posts() ) : ?>
<div class="npropprod bnpropprod">
    <div class="wrp">
        <h2 class="npropprod__title"><?php the_field('cp_other_products_title', 'option'); ?></h2>
        <div class="npropprod__row">
            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
            <div class="npropprod__el">
                <div class="box">
                    <?php if ( has_post_thumbnail() ) { ?>
                    <div class="box__img">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_post_thumbnail(array(198,138)); ?>
                        </a>
                    </div>
                    <?php } ?>
                    <h3 class="box__title"><?php the_title();?></h3>
                    <?php echo do_shortcode('[add_to_cart id="'.get_the_ID().'"]'); ?>
                </div>
            </div>
            <?php endwhile; // end of the loop. ?>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="cart-collaterals" style="display:none;">
    <?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>

<div style="clear:both;"></div>

<?php do_action( 'woocommerce_after_cart' ); ?>