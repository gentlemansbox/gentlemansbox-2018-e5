<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Loading premium template if checkbox is active
if ( get_field('activate_premium_theme') ) {
    
     include_once('new-sinle-product.php');
     
} else {
    
    get_header();
    /**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
 ?>
 
    <div class="wrap">
    <div class="bread_crum"><?php //echo woocommerce_breadcrumb_gentleman(); ?></div>
      <div class="top-deatil-box clearfix">
        <?php
      //Get the detail of products
      if(have_posts())
      {
         while(have_posts())
         {
            //Make a code for product
            the_post();
            global $post;
            $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );		
            $feat_image_src = $full_image_url[0];
            ?>
        <div class="left-product-image-box">
          <?php if( !empty($feat_image_src) ){ ?>
          <img alt="Blog Content" src="<?php echo $feat_image_src; ?>">
          <?php } ?>
        </div>
        <div class="right-detail-box">
          <div class="product-title-detail">
            <?php the_title(); ?>
          </div>
          <p class="product-price-detail">
            <?php 
							$product = new WC_Product( get_the_ID() );
							$price = $product->price;
							$title = $product->get_title();
							
							//if is subscription
							$terms = wp_get_post_terms( $post->ID, 'product_cat' );
							foreach ( $terms as $term ) $categories[] = $term->slug;
							if ( in_array( 'premium-subscription', $categories ) || in_array( 'subscriptions', $categories ) || in_array( 'sock-subscription', $categories ) || in_array( 'tie-subscription', $categories ) ) {  
								if ( strpos($product->get_title(), 'Annual') !== false ) {
									echo  wc_price($price);
								} else {
									echo  wc_price($price) . ' per box';
								}
							} else {
								echo  wc_price($price);
							}
           ?>
          </p>
          <div class="rating-with-description">
            <div class="desc-product">
              <?php the_content(); ?>
            </div>
            <div class="quantity"> <span class="quantity-text">QUANTITY: </span>
              <select onchange="jQuery(<?php echo $product->ID; ?>).attr('data-quantity', jQuery(this).val())">
                <?php
                for($i=1; $i<10; $i++)
                {
                ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php
                }?>
              </select>
              
              <input type="number" value="1"  onchange="jQuery(<?php echo $product->ID; ?>).attr('data-quantity', jQuery(this).val())" />
            </div>
						<div class="detail-button-box">
							<a href="<?php the_permalink(); ?>/?add-to-cart=<?php echo $product->id;  ?>" rel="nofollow" data-product_id="<?php echo $product->id; ?>"  data-product_sku="<?php echo $product->get_sku($product->ID); ?>" data-quantity="1" class="button add_to_cart_button product_type_simple add-cart-button">
								<?php if ( in_array( 'premium-subscription', $categories ) || in_array( 'subscriptions', $categories ) || in_array( 'sock-subscription', $categories ) || in_array( 'tie-subscription', $categories ) ) {  
									echo  'ADD SUBSCRIPTION TO CART';
								} else {
									echo 'ADD PRODUCT TO CART'; 
								} ?>
							</a> 
                 <!--<a href="javascript:void(0)"  class="add-fav-button"><?php _e('ADD TO FAVORITES'); ?></a>--> 
            </div>
            <div class="social-media-box"><?php if ( shortcode_exists( 'cresta-social-share' ) ) { echo do_shortcode("[cresta-social-share]"); } ?>  </div>
          </div>
        </div>
        <?php
         }
      }
      ?>
			<?php if ( is_product() && get_the_id() == 581103 ) : 
				//PREMIUM BOX PRODUCT PAGE 
				//DONT DISPLAY RELATED
			?>
			<?php else : ?>
				<div class="related-item-detail-page">
					<div class="heading-related-item">
						<?php _e('People Who Bought This Item Also Bought'); ?>
					</div>
					<div class="clearfix related-product-box">
						<?php
							global $post;
							$old_post_id = $post->ID;
							$args = array( 'post_type'=>'product', 'orderby'=>'rand',  'post_status' =>'publish', 'posts_per_page' => 4,'meta_query'=>  array(array('key' => '_visibility','value' => array('hidden', 'search'),'compare'=>'NOT IN')), 'post__not_in' => array(  $product->ID));
							$the_other_item_query = new WP_Query( $args );
							if ( $the_other_item_query->have_posts() ) {
								while ( $the_other_item_query->have_posts() ) {
									$the_other_item_query->the_post();
									global $post;
									if( $old_post_id == $post->ID) {
										continue;
									}
									$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
									$feat_image_src = $full_image_url[0];
									?>
										<div class="main-product-box-outer">
											<div class="button-hover-show-box">
												<div class="product-box">
													<?php if(!empty($feat_image_src)){ ?>
														<div class="image-box-product"> <a href="<?php the_permalink(); ?>"><img alt="<?php the_title(); ?>" src="<?php echo $feat_image_src; ?>"></a> </div>
													<?php } ?>
													<div class="bottom-desc-box">
														<p class="product_title">
															<?php the_title(); ?>
														</p>
														<p class="product-price-text"><span class="amount">
															<?php 
																global $product;
																$product = new WC_Product( get_the_ID() );
																$price = $product->price; 
																echo  wc_price($price);
															?>
															</span></p>
														<div><span class="rating-star">
															<?php woocommerce_get_template( '/loop/rating.php' ); ?>
														</span></div>
													</div>
												</div>
												<div class="buttons-box"> 
													<a class="product-box-cart-button view-button-product" href="<?php the_permalink(); ?>">
														<?php _e('View'); ?>
													</a> 
													<a href="<?php the_permalink(); ?>/?add-to-cart=<?php echo $product->id;  ?>" rel="nofollow" data-product_id="<?php echo $product->id; ?>"  data-product_sku="<?php echo $product->get_sku($product->id); ?>" data-quantity="1" class="button product-box-cart-button add-button-product add_to_cart_button product_type_simple add-cart-button">
														<?php _e('Add to cart'); ?>
													</a> 
												</div>
											</div>
										</div>
									<?php
								}	
							}
							wp_reset_postdata();
						?>
					</div>
				</div>
			<?php endif; ?>
      <?php
      if ( comments_open() )
      {
      ?><div class="reviews-simple-feedback">
            <div class="feedback-box">
              <div class="comments_list">
                 <?php   comments_template( '/woocommerce/single-product-reviews.php' );   //woocommerce_get_template( 'single-product-reviews.php' ); ?>
              </div>
            </div>
        </div>
      <?php
      }
      ?>
    </div>
    <script type="text/javascript">
    jQuery(document).ready(function(){
        
        jQuery('.write-review-button').click(function(){
            
            jQuery('#comment_form_with_comment_list').slideDown('slow');
        
        });

    });
    </script>

        <?php
            /**
             * woocommerce_after_main_content hook.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action( 'woocommerce_after_main_content' );
        ?>

        <?php
            /**
             * woocommerce_sidebar hook.
             *
             * @hooked woocommerce_get_sidebar - 10
             */
            //do_action( 'woocommerce_sidebar' );
        ?>

    </div>


<?php
    get_footer();
}
?>

