<?php
/**
 * My Subscriptions section on the My Account page
 *
 * @author 		Prospress
 * @category 	WooCommerce Subscriptions/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
    $user_ID = get_current_user_id();
    // Field: availed_discounted_subscription
    $user_availed_discount = SUBSCRIPTION_DISCOUNT_DEBUG ? false : get_field(SUBSCRIPTION_AVAILED_FIELD, "user_$user_ID");
    //$user_availed_discount = true; // TODO uncomment the above line - After v2 Release and when Subscription discount is fixed after 7 Jan 2016
    $usermeta_availed_discount = get_user_meta ($user_ID, "availed_discounted_subscription", true);
//    var_dump($availed);
//    var_dump($user_ID);
//    var_dump($user);
$user = wp_get_current_user();
$user_email=$user->user_email;
$user_firstname=$user->user_firstname;
$user_lastname=$user->user_lastname;
$order = 0;
$order = new WC_Order($_GET[sub]);
$order = $order->id;
if($order !== 0){
  apply_subscription_discount($order);
}
?>

<style>
.satisfydiv{
	border: 2px solid #000;
    margin-top: 10px;
    padding: 10px;
    width: 50%;
	}
/*.try_nocancel{margin-top: 10px;}*/
.satisfydiv p{ padding-top:5px;}
.satisfydiv p:first-child{ font-weight:bold;}
.radio_text{margin: 0 10px 12px;}
.btn{padding: 5px 10px !important; font-size:90% !important;}
.cancel-subscription-error-message {
    color: #ff0000;
    margin: 10px 0;
}

#colorbox,
#cboxClose, #cboxClose:active, #cboxClose:visited, #cboxClose:focus,
.lbp-inline-link-1, .lbp-inline-link-1:active, .lbp-inline-link-1:visited, .lbp-inline-link-1:focus {
    outline: 0;
}
</style>
<div class="woocommerce_account_subscriptions">

	<h2><?php esc_html_e( 'My Subscriptions', 'woocommerce-subscriptions' ); ?></h2>

	<?php if ( ! empty( $subscriptions ) ) : ?>
	<table class="shop_table shop_table_responsive my_account_subscriptions my_account_orders">

	<thead>
		<tr>
			<th class="subscription-id order-number"><span class="nobr"><?php esc_html_e( 'Subscription', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-status order-status"><span class="nobr"><?php esc_html_e( 'Status', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-next-payment order-date"><span class="nobr"><?php esc_html_e( 'Next Payment', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-total order-total"><span class="nobr"><?php esc_html_e( 'Total', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-actions order-actions">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
        <?php $subscription_discount_details = array(); ?>
        <?php $cancel_reason_button_config = array(); ?>
	<?php foreach ( $subscriptions as $subscription_id => $subscription ) :
            $order = wc_get_order( $subscription );
            $orderID = $order->id;
            $items = $order->get_items();
             $product_title ='';
             $productID = '';
             foreach( $items as $custom_key =>$custom_val )
             {
                if(!empty($custom_val['name']))
                {
                    $product_title = $custom_val['name'];
                }
                if(!empty($custom_val['product_id']))
                {
                    $productID = $custom_val['product_id'];
                }

             }
	?>
                <?php
            if (!empty($productID) && empty($subscription_discount_details[''.$productID])) {

                        $discount_amount = get_post_meta($productID, 'discount_amount', true ); //field_55ee0405b8812
                        $discount_text = get_post_meta( $productID, "discount_text" , true); //field_55ee0480b8813
                        $discount_duration = get_post_meta( $productID, "discount_duration" , true);//field_55ee04a2b8814

                        $subscription_discount_details[''.$productID] = array(
                            'details' => $subscription_details,
                            'discount_amount' => $discount_amount,
                            'discount_text' => $discount_text,
                            'discount_duration' => $discount_duration
                        );
                    }

            if (!empty($productID) && empty($cancel_reason_button_config[''.$productID])) {

                        $discount_button_config = array(
                            "fee is too high" => array("meta_key" => "fee_is_too_high"),
                            "billing issues" => array("meta_key" => "billing_issues"),
                            "setup issues" => array("meta_key" => "setup_issues"),
                            "unhappy with products" => array("meta_key" => "unhappy_with_products"),
                            "unhappy with delivery" => array("meta_key" => "unhappy_with_delivery"),
                            "other" => array("meta_key" => "other_reason"),

                        );
                        $discount_offer_for = get_post_meta($productID, "show_discount_offer_for" , true);//field_55fb4b3f1f19e
                        if (!is_array($discount_offer_for)) {
                            $discount_offer_for = array($discount_offer_for);
                        }

                        foreach($discount_offer_for as $reason) {
                            if (array_key_exists(strtolower($reason), $discount_button_config)) {
                                if (!$user_availed_discount) {
                                    $discount_button_config[strtolower($reason)]['button'] = true;
                                }
                            }
                        }
                        foreach($discount_button_config as $key => $reason) {
//                            var_dump($reason['meta_key'] . "_caption");
//                            var_dump(get_post_meta( $productID, $reason['meta_key'] . "_caption" , true));
//                            var_dump(get_field($reason['meta_key'] . "_caption", $productID));
                            if (!is_array($discount_button_config[$key])) {
                                $discount_button_config[$key] = array();
                            }

                            $discount_button_config[$key]['caption'] = get_post_meta( $productID, $reason['meta_key'] . "_caption" , true);
                            $discount_button_config[$key]['offer'] = get_post_meta( $productID, $reason['meta_key'] . "_offer" , true);
                        }
                        //var_dump($discount_button_config);
                        $cancel_reason_button_config[''.$productID] = $discount_button_config;
                        //var_dump($discount_button_config);
                    }

                    //var_dump(array($user_ID, $productID, $orderID, $user_availed_discount, $usermeta_availed_discount, array($da, $dt, $dd, $dspt, $ds, $dr)));
                    //var_dump(get_post_meta($productID));



        ?>
		<tr class="order">
			<td class="subscription-id order-number" data-title="<?php esc_attr_e( 'ID', 'woocommerce-subscriptions' ); ?>">
				<a href="<?php echo esc_url( $subscription->get_view_order_url() ); ?>"><?php echo  $product_title, " ", "#", esc_html( $subscription->get_order_number() ); ?></a>
				<?php do_action( 'woocommerce_my_subscriptions_after_subscription_id', $subscription ); ?>
			</td>
			<td class="subscription-status order-status" style="text-align:left; white-space:nowrap;" data-title="<?php esc_attr_e( 'Status', 'woocommerce-subscriptions' ); ?>">
				<?php echo esc_attr( wcs_get_subscription_status_name( $subscription->get_status() ) ); ?>
                <?php

                    $this_subscription_is_discounted = get_post_meta($subscription_id, '_this_subscription_is_discounted', true);
                    $is_discounted = !empty($this_subscription_is_discounted);

                    if ($is_discounted) {
                        $d_subscription_interval = get_post_meta($subscription_id, '_d_subscription_interval', true);
                        $d_subscription_period = get_post_meta($subscription_id, '_d_subscription_period', true);

                    }
                    echo $is_discounted ? "<br/><small>($d_subscription_interval $d_subscription_period discount offer)</small>" : "";

                ?>
			</td>
			<td class="subscription-next-payment order-date" data-title="<?php esc_attr_e( 'Next Payment', 'woocommerce-subscriptions' ); ?>">
				<?php echo esc_attr( $subscription->get_date_to_display( 'next_payment' ) ); ?>
				<?php if ( ! $subscription->is_manual() && $subscription->has_status( 'active' ) && $subscription->get_time( 'next_payment' ) > 0 ) : ?>
					<?php $payment_method_to_display = sprintf( __( 'Via %s', 'woocommerce-subscriptions' ), $subscription->get_payment_method_to_display() ); ?>
					<?php $payment_method_to_display = apply_filters( 'woocommerce_my_subscriptions_payment_method', $payment_method_to_display, $subscription ); ?>
				&nbsp;<small><?php echo esc_attr( $payment_method_to_display ); ?></small>
				<?php endif; ?>
			</td>
			<td class="subscription-total order-total">
				<?php echo wp_kses_post( $subscription->get_formatted_order_total() ); ?>
			</td>
			<?php
			$is_gift=false;
			$is_annual=false;
			$subscription_product_title = '';
			foreach ( $subscription->get_items() as $item_id => $item ) {
				$subscription_product_title= strtolower($item['name']);
				if (strpos($subscription_product_title, 'gift') !== false) {
					$is_gift = true;
				}
				if (strpos($subscription_product_title, 'annual') !== false) {
					$is_annual = true;
				}
			}; ?>
			<td class="subscription-actions order-actions <?php if($is_gift===true){echo 'gift_sub';} elseif($is_annual===true) {echo 'annual_sub';} ?>">
                <?php $actions = wcs_get_all_user_actions_for_subscription( $subscription, $user_ID); ?>
                <?php if ( ! empty( $actions ) ) : ?>
									<?php foreach ( $actions as $key => $action ) : ?>
					                    <?php if(esc_html( $action['name'] )=='Cancel'): ?>
					                    <?php $current_user = wp_get_current_user();
															$user_ID = $current_user->ID;
															$billing_country = get_user_meta($user_ID,'billing_country',true);
															?>

					                <div style="display:none"></div>
													<!-- <?php if($billing_country=='US') { ?>
														<a class="typeform-share button" href="https://gentlemansbox1.typeform.com/to/XOrLgY?sub=<?php echo esc_html( $subscription->get_order_number() ); ?>&email=<?php echo $user_email; ?>&firstname=<?php echo $user_firstname; ?>&lastname=<?php echo $user_lastname; ?>" data-mode="popup" target="_blank">Cancel</a>
														<script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
													<?php } else { ?>
														<a class="typeform-share button" href="https://gentlemansbox1.typeform.com/to/QXOJdv?sub=<?php echo esc_html( $subscription->get_order_number() ); ?>&email=<?php echo $user_email; ?>&firstname=<?php echo $user_firstname; ?>&lastname=<?php echo $user_lastname; ?>" data-mode="popup" target="_blank">Cancel </a>
					 								 <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
													<?php } ?> -->
													<p>CALL TO CANCEL</p>

					                    <?php else: ?>
					                 <a href="<?php echo esc_url( $action['url'] ); ?>" class="button <?php echo sanitize_html_class( $key ) ?>"><?php echo esc_html( $action['name'] ); ?></a>
					                    <?php endif; ?>
									<?php endforeach; ?>
                <?php else : ?>
                    <a href="<?php echo esc_url( $subscription->get_view_order_url() ) ?>" class="button view"><?php esc_html_e( 'View', 'woocommerce-subscriptions' ); ?></a>
                <?php endif; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>

	</table>


	<?php else : ?>

		<p class="no_subscriptions"><?php printf( esc_html__( 'You have no active subscriptions. Find your first subscription in the %sstore%s.', 'woocommerce-subscriptions' ), '<a href="' . esc_url( apply_filters( 'woocommerce_subscriptions_message_store_url', get_permalink( woocommerce_get_page_id( 'shop' ) ) ) ) . '">', '</a>' ); ?></p>

	<?php endif; ?>

</div>

<?php
