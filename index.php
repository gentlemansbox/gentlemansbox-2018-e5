<?php get_header(); ?>

<div class="wrap blog">

	<div class="section7">
		<div class="left">
			<h3>Featured Post</h3>
			<?php 
				$args = array(
					'posts_per_page' => 1, 
					'category__not_in' => array(28,29),
					'meta_query' => array(
		        array(
	            'key' => 'featured',
	            'value' => 'Yes',
		        )
			    )
				);
				query_posts($args);
				if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			?>
				<article class="featured-post">
		      <a href="<?php the_permalink(); ?>">
		        <figure>
		          <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large', false, '' ); ?> 
		          <div class="blogPic1" style="background-image: url(<?php echo $src[0]; ?>);">
		            <p class="text-center"><span class="month"><?php the_time('M'); ?></span><br/>
		            <span class="day"><?php the_time('d'); ?></span></p>
		          </div>
		          <figcaption>
		            <h4><?php the_title(); ?></h4>
		            <p><?php echo get_excerpt(); ?></p>
		          </figcaption>
		        </figure>
		      </a>
		    </article>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="right blog-categories">
			<h3>Categories</h3>
			<ul class="product-categories">
			  <?php
			    $args = array (
			      'title_li'           => __( '' ),
			      'exclude'    => array( 29, 28 ),
			    );
			    wp_list_categories( $args ); 
			  ?>
			</ul>
		</div>
		<div style="clear: both; margin-bottom: 75px;"></div>
		<h3>More Posts</h3>
		<div class="post-preview-container">
			<div class="blog-loader-contain">
				<img src="<?php bloginfo('template_url'); ?>/library/images/loading-gears.gif" alt="Loading Icon">
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>
