<?php /*
Template Name: Compare
*/ ?>

<?php get_header('compare'); ?>

<section class="page-title">
	<div class="title">
		<div class="title-container">
			<h1>
				<span>compare</span>
				Gentleman's Box<br>
				vs. <?php the_title(); ?>
			</h1>
			<a href="#compare-chart" class="btn smoothScroll">Discover the Difference</a>
		</div>
	</div>
	<div class="image">
		<?php $image = get_field('header_image'); ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt'] ?>">
	</div>
</section>
<section class="banner">
	<div>
		<?php the_field('banner_1'); ?>
	</div>
</section>
<div id="compare-chart"></div>
<section class="comparison">
	<?php 
		$check = "<span class='check'>"."✓"."</span>";
		$cross = "<span class='cross'>"."✗"."</span>";
	?>
	<table>
		<tr>
			<th></th>
			<th>Gentleman's Box</th>
			<th><?php the_title(); ?></th>
		</tr>
		<tr>
			<td>Custom Groomsmen Gift</td>
			<td><?php echo $check; ?></td>
			<td>
				<?php 
					if ( get_field('groomsmen_gift') ) :
						echo $check;
					else : 
						echo $cross;
					endif; 
				?>
			</td>
		</tr>
		<tr>
			<td>Free Shipping</td>
			<td><?php echo $check; ?></td>
			<td>
				<?php 
					if ( get_field('free_shipping') ) :
						echo $check;
					else : 
						echo $cross;
					endif; 
				?>
			</td>
		</tr>
		<tr>
			<td>Guide in each box</td>
			<td><?php echo $check; ?></td>
			<td>
				<?php 
					if ( get_field('guide') ) :
						echo $check;
					else : 
						echo $cross;
					endif; 
				?>
			</td>
		</tr>
		<tr>
			<td>Phone and text support</td>
			<td><?php echo $check; ?></td>
			<td>
				<?php 
					if ( get_field('customer_support') ) :
						echo $check;
					else : 
						echo $cross;
					endif; 
				?>
			</td>
		</tr>
		<tr>
			<td>Magazine Subscription Included</td>
			<td>1-year GQ</td>
			<td>
				<?php 
					if ( get_field('magazine') ) :
						the_field('magazine');
					else : 
						echo $cross;
					endif; 
				?>
			</td>
		</tr>
		<tr>
			<td>Price</td>
			<td>$100/quarterly</td>
			<td><?php the_field('price'); ?></td>
		</tr>
		<tr>
			<td>Frequency</td>
			<td>Quarterly</td>
			<td><?php the_field('frequency'); ?></td>
		</tr>
	</table>
	<a href="https://gentlemansbox.com/premium-subscription-application/" class="btn">See if I qualify</a>
</section>
<section class="banner">
	<div>
		<?php the_field('banner_2'); ?>
	</div>
</section>
<section class="previous-boxes">
	<h2>See what gentlemen recieved in past Premium Boxes</h2>
	<?php while ( have_rows('previous_boxes') ) : the_row(); ?>
		<div class="previous-box">
				<?php $image = get_sub_field('image'); ?>
			<img src="<?php echo $image['sizes']['large'] ?>" alt="<?php echo $image['alt'] ?>" />
			<p><?php the_sub_field('title'); ?></p>
		</div>
	<?php endwhile; ?>
</section>
<section class="closing">
	<?php the_field('closing'); ?>
</section>

<?php get_footer('new'); ?>