<?php
/**
 * Template Name: Subscriptions page
*/
get_header();

global $woocommerce;
if ( isset($_POST['fws']) && $_POST['fws'] == "1" ) {
    $loadcart = True;
    if ( isset($_POST['subscriptions']) ) { $subscr = $_POST['subscriptions']; }
    else { $subscr = []; }
    if ( isset($_POST['addons']) ) { $addons = $_POST['addons']; }
    else { $addons = []; }
    $_POST['fws'] = 0;
    foreach ( $subscr as $k => $v ) {
        $product_id = $v;
        $found = false;
        if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
            foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
                $_product = $values['data'];
                if ( $_product->id == $product_id ) {
                    $found = true;
                }
            }
            if ( ! $found ) {
                WC()->cart->add_to_cart( $product_id );
                echo "<script type='text/javascript'>
                fbq('track', 'AddToCart', {
                  content_ids:".$product_id.",
                  currency: 'USD',
                  content_type:'product'
                });
                </script>
                <script>
                pintrk('track', 'AddToCart', {
                  product_id:".$product_id.",
                  currency: 'USD'
                });
                </script>";
            }
        } else {
            WC()->cart->add_to_cart( $product_id );
            echo "<script type='text/javascript'>
            fbq('track', 'AddToCart', {
              content_ids:".$product_id.",
              currency: 'USD',
              content_type:'product'
            });
            </script>
            <script>
            pintrk('track', 'AddToCart', {
              product_id:".$product_id.",
              currency: 'USD'
            });
            </script>";
        }
    }
    foreach ( $addons as $k => $v ) {
        $product_id = $v;
        $found = false;
        if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
            foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
                $_product = $values['data'];
                if ( $_product->id == $product_id ) {
                    $found = true;
                }
            }
            if ( ! $found ) {
                WC()->cart->add_to_cart( $product_id );
                echo "<script type='text/javascript'>
                fbq('track', 'AddToCart', {
                  content_ids:".$product_id.",
                  currency: 'USD',
                  content_type:'product'
                });
                </script>
                <script>
                pintrk('track', 'AddToCart', {
                  product_id:".$product_id.",
                  currency: 'USD'
                });
                </script>";
            }
        } else {
            WC()->cart->add_to_cart( $product_id );
            echo "<script type='text/javascript'>
            fbq('track', 'AddToCart', {
              content_ids:".$product_id.",
              currency: 'USD',
              content_type:'product'
            });
            </script>
            <script>
            pintrk('track', 'AddToCart', {
              product_id:".$product_id.",
              currency: 'USD'
            });
            </script>";
        }
    }
} else {
    $loadcart = False;
}

if ( $loadcart ) { ?>

    <div class="subscrib__pag subscrib__pag__wh">
        <ul class="pag">
            <li><span class="link link1" data-targ="1"><span class="desctop">1. Select Your Subscriptions</span><span class="mobile">1. Subscriptions</span></span></li>
            <li><span class="link link2" data-targ="2"><span class="desctop">2. Select Your Add-Ons</span><span class="mobile">2. Add-Ons</span></span></li>
            <li><span class="link link3 active" data-targ="3"><span class="desctop">3. Review Your Plan</span><span class="mobile">3. Review</span></span></li>
        </ul>
    </div>

    <div class="subscrpage subscrpage__cart">
        <div class="wrap">
            <div class="nptitle">
                <h2 class="nptitle__title">Review Your Plan</h2>
            </div>
            <form action="#" method="post" id="subscrpagef">
            <?php if ( count($subscr) > 0 ) { ?>
                <?php $subscription_posts = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post__in' => $subscr, 'orderby' => 'menu_order', 'order' => 'ASC' ) ); ?>
                <?php if ( $subscription_posts->have_posts() ) { ?>
                <div class="subsfintable subs">
                    <h3 class="subsfintable__ttitle">Your Subscriptions</h3>
                    <?php while ( $subscription_posts->have_posts() ) { ?>
                        <?php $subscription_posts->the_post(); ?>
                        <div class="subsfintable__row">
                            <div class="clfx">
                                <div class="left">
                                    <div class="clfx">
                                        <?php if ( has_post_thumbnail() ) { ?>
                                            <div class="subsfintable__img desc"><?php the_post_thumbnail(array(124, 90)); ?></div>
                                            <div class="subsfintable__img mob"><?php the_post_thumbnail(100, 100); ?></div>
                                        <?php } ?>
                                        <h4 class="subsfintable__title"><?php the_title(); ?>
                                        <p class="subsfintable__addtext">Ships every month</p>
                                        <div class="subsfintable__price mob"><?php echo do_shortcode('[add_to_cart id="'.get_the_id().'" style=""]'); ?></div>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="subsfintable__price desc"><?php echo do_shortcode('[add_to_cart id="'.get_the_id().'" style=""]'); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php } ?>
            <?php } ?>

            <?php if ( count($addons) > 0 ) { ?>
                <?php $addons_posts = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post__in' => $addons ) ); ?>
                <?php if ( $addons_posts->have_posts() ) { ?>
                <div class="subsfintable subs">
                    <h3 class="subsfintable__ttitle">Your Add-Ons</h3>
                    <?php while ( $addons_posts->have_posts() ) { ?>
                        <?php $addons_posts->the_post(); ?>
                        <div class="subsfintable__row">
                            <div class="clfx">
                                <div class="left">
                                    <div class="clfx">
                                        <?php if ( has_post_thumbnail() ) { ?>
                                            <div class="subsfintable__img desc"><?php the_post_thumbnail(array(124, 90)); ?></div>
                                            <div class="subsfintable__img mob"><?php the_post_thumbnail(100, 100); ?></div>
                                        <?php } ?>
                                        <h4 class="subsfintable__title"><?php the_title(); ?>
                                        <p class="subsfintable__addtext">Ships the first month</p>
                                        <div class="subsfintable__price mob"><?php echo do_shortcode('[add_to_cart id="'.get_the_id().'" style=""]'); ?></div>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="subsfintable__price desc"><?php echo do_shortcode('[add_to_cart id="'.get_the_id().'" style=""]'); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php } ?>
            <?php } ?>
            <div class="clfx">
                <?php woocommerce_cart_totals(); ?>
            </div>
            </form>
        </div>
    </div>

<?php } else { ?>

    <div class="subscrib__pag">
        <ul class="pag">
            <li><span class="link link1 active" data-targ="1"><span class="desctop">1. Select Your Subscriptions</span><span class="mobile">1. Subscriptions</span></span></li>
            <li><span class="link link2" data-targ="2"><span class="desctop">2. Select Your Add-Ons</span><span class="mobile">2. Add-Ons</span></span></li>
            <li><span class="link link3" data-targ="3"><span class="desctop">3. Review Your Plan</span><span class="mobile">3. Review</span></span></li>
        </ul>
    </div>

    <div class="subscrpage">
        <div class="wrap">
            <form method="post" id="subscontform">
                <div class="shippingcont shippingcont1 active">
                    <div class="nptitle">
                        <h2 class="nptitle__title">Select Your Subscription</h2>
                        <h4 class="nptitle__subtitle">Select as many as you’d like</h4>
                    </div>
                    <div class="ovwshipping">
                        <p class="shipping__text">Ship To: </p>
                        <select id="chose_shipping_type" name="shipping_type">
                            <option value="us">U.S.</option>
                            <option value="international">International</option>
                        </select>
                    </div>
                    <div class="subscont subscont__ship">
                      <!-- MONTHLY US -->
                      <div class="subs" style="">
                        <div class="subs__img">
                          <img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1.png 300w, https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
                        </div>
                        <h3 class="subs__title">Classic Subscription (U.S.)</h3>
                        <p class="product woocommerce add_to_cart_inline " style="">
                          <span class="woocommerce-Price-amount amount">
                            <span class="woocommerce-Price-currencySymbol">$</span>
                            25.00
                          </span>
                          <span class="subscription-details"> / month</span>
                          <a rel="nofollow" href="/subscriptions/?add-to-cart=64660" data-product_id="64660" data-product_sku="MONTHLYUSA" data-quantity="1" class="button product_type_subscription add_to_cart_button ajax_add_to_cart product-box-cart-button add-button-product add_to_cart_button product_type_subscription">Add to Cart</a>
                        </p>
                        <input type="checkbox" name="subscriptions[]" value="64660" id="subscription1">
                        <label for="subscription1"></label>
                        <input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="u.s.">
                      </div>
                      <!-- MONTHLY INT -->
                      <div class="subs" style="">
                        <div class="subs__img">
                          <img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1.png 300w, https://gentlemansbox.com/wp-content/uploads/ClassicTransparent-1-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
                        </div>
                        <h3 class="subs__title">Classic (International)</h3>
                        <p class="product woocommerce add_to_cart_inline " style="">
                          <span class="woocommerce-Price-amount amount">
                            <span class="woocommerce-Price-currencySymbol">$</span>
                            33.00
                          </span>
                          <span class="subscription-details"> / month</span>
													<a rel="nofollow" href="/subscriptions/?add-to-cart=1162" data-product_id="1162" data-product_sku="MONTHLYINTL" data-quantity="1" class="button product_type_subscription add_to_cart_button ajax_add_to_cart product-box-cart-button add-button-product add_to_cart_button product_type_subscription">Add to Cart</a>
												</p> 
												<input type="checkbox" name="subscriptions[]" value="1162" id="subscription2">
                        <label for="subscription2"></label>
                        <input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="international">
											</div>
                      <!-- PREMIUM BOX US-->
											<div class="subs" style="">
												<a href="/premium-membership-application">
													<div class="subs__img">
														<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/PremiumTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/PremiumTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/PremiumTransparent-150x150.jpg 150w" sizes="(max-width: 236px) 100vw, 236px">
													</div>
													<h3 class="subs__title" style="margin-bottom:0;">Premium Box</h3>
													<h4 class="subs__title">Limited Quantities</h4>
													<p class="product woocommerce add_to_cart_inline " style="color:#000;">
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>
															100.00
														</span>
														<span class="subscription-details"> / qtr</span>
													</p>
													<label for=""></label>
													<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="u.s.">
												</a>
											</div>
                      <!-- PREMIUM BOX INT-->
											<div class="subs" style="">
												<a href="/premium-membership-application">
													<div class="subs__img">
														<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/PremiumTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/PremiumTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/PremiumTransparent-150x150.jpg 150w" sizes="(max-width: 236px) 100vw, 236px">
													</div>
													<h3 class="subs__title" style="margin-bottom:0;">Premium Box</h3>
													<h4 class="subs__title">Limited Quantities</h4>
													<p class="product woocommerce add_to_cart_inline " style="color:#000;">
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>
															130.00
														</span>
														<span class="subscription-details"> / qtr</span>
													</p>
													<label for=""></label>
													<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="international">
												</a>
											</div>
											<!-- GIFTS US-->
											<div class="subs" style="">
												<a href="/gift-subscription">
													<div class="subs__img">
														<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/GiftTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/GiftTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/GiftTransparent-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
													</div>
													<h3 class="subs__title" style="margin-bottom:0;">Gifts</h3>
													<h4 class="subs__title">Starting at</h4>
													<p class="product woocommerce add_to_cart_inline " style="color:#000;">
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>
															23.00
														</span>
														<span class="subscription-details"> / month</span>
													</p>
													<label for=""></label>
													<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="u.s.">
												</a>
											</div>
											<!-- GIFTS INT-->
											<div class="subs" style="">
												<a href="/gift-subscription/international">
													<div class="subs__img">
														<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/GiftTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/GiftTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/GiftTransparent-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
													</div>
													<h3 class="subs__title" style="margin-bottom:0;">Gifts</h3>
													<h4 class="subs__title">Starting at</h4>
													<p class="product woocommerce add_to_cart_inline " style="color:#000;">
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>
															31.00
														</span>
														<span class="subscription-details"> / month</span>
													</p>
													<label for=""></label>
													<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="international">
												</a>
											</div>
											<!-- ANNUAL US -->
											<div class="subs" style="">
												<div class="subs__img">
													<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/AnnualTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/AnnualTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/AnnualTransparent-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
												</div>
												<h3 class="subs__title">Annual Subscription (U.S.)</h3>
												<p class="product woocommerce add_to_cart_inline " style="">
													<span class="woocommerce-Price-amount amount">
														<span class="woocommerce-Price-currencySymbol">$</span>
														23.00
													</span>
													<span class="subscription-details"> / month</span>
													<a rel="nofollow" href="/subscriptions/?add-to-cart=76" data-product_id="76" data-product_sku="ANNUALUS" data-quantity="1" class="button product_type_subscription add_to_cart_button ajax_add_to_cart product-box-cart-button add-button-product add_to_cart_button product_type_subscription">Add to Cart</a>
												</p>
												<input type="checkbox" name="subscriptions[]" value="76" id="subscription7">
												<label for="subscription7"></label>
												<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="u.s.">
											</div>
											<!-- ANNUAL INT -->
											<div class="subs" style="">
												<div class="subs__img">
													<img width="236" height="236" src="https://gentlemansbox.com/wp-content/uploads/AnnualTransparent.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/AnnualTransparent.png 300w, https://gentlemansbox.com/wp-content/uploads/AnnualTransparent-150x150.png 150w" sizes="(max-width: 236px) 100vw, 236px">
												</div>
												<h3 class="subs__title">Annual Subscription (International)</h3>
												<p class="product woocommerce add_to_cart_inline " style="">
													<span class="woocommerce-Price-amount amount">
														<span class="woocommerce-Price-currencySymbol">$</span>
														31.00
													</span>
													<span class="subscription-details"> / month</span>
													<a rel="nofollow" href="/subscriptions/?add-to-cart=1161" data-product_id="1161" data-product_sku="ANNUALINTL" data-quantity="1" class="button product_type_subscription add_to_cart_button ajax_add_to_cart product-box-cart-button add-button-product add_to_cart_button product_type_subscription">Add to Cart</a>
												</p>
												<input type="checkbox" name="subscriptions[]" value="1161" id="subscription8">
												<label for="subscription8"></label>
												<input type="hidden" name="cnbicwn_shipping_type" class="cnbicwn_shipping_type" value="international">
											</div>
										</div>
										<p class="btncont"><a href="#" class="nextstep" data-targ="2">next step</a></p>
                </div>
                <div class="shippingcont shippingcont2">
                    <div class="nptitle">
                        <h2 class="nptitle__title">Select Your Add-Ons</h2>
                        <h4 class="nptitle__subtitle">These will be added to your first month’s box</h4>
                    </div>
                    <?php $addons = get_field( 'addons' ); ?>
                    <?php if ( $addons && count($addons) > 0 ) { ?>
                        <?php $addons_posts = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post__in' => $addons ) ); ?>
                        <?php if ( $addons_posts->have_posts() ) { ?>
                        <div class="subscont">
                            <?php $ind = 1; ?>
                            <?php while ( $addons_posts->have_posts() ) { ?>
                            <?php $addons_posts->the_post(); ?>
                                <div class="subs">
                                    <?php if ( has_post_thumbnail() ) { ?>
                                        <div class="subs__img"><?php the_post_thumbnail(array(318, 236)); ?></div>
                                    <?php } ?>
                                    <h3 class="subs__title"><?php the_title(); ?></h3>
                                    <?php echo do_shortcode('[add_to_cart id="'.get_the_id().'" style=""]'); ?>
                                    <input type="checkbox" name="addons[]" value="<?php the_ID(); ?>" id="addon<?php echo $ind; ?>" />
                                    <label for="addon<?php echo $ind; ?>"></label>
                                </div>
                                <?php $ind++; ?>
                            <?php } ?>
                        </div>
                        <!-- HEADLINER LABS -->
                        <div class="hl-fbm-checkbox" data-type="add_to_cart"></div>
                        <!-- HEADLINER LABS -->
                        <p class="btncont"><a href="#" class="prewstep" data-targ="1">previous</a><input type="submit" name="submit" value="next step" /></p>
                        <?php } ?>
                        <?php wp_reset_postdata(); ?>
                    <?php } ?>
                </div>
                <input type="hidden" name="fws" value="1" />
            </form>
        </div>
    </div>

<?php } ?>

<?php get_footer('new'); ?>
