<?php get_header(); ?>

<div class="wrap single">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<p class="date"><span class="month"><?php the_time('M'); ?></span><span class="day"><?php the_time('d'); ?></span></p>
				<div class="single-post-content"><?php the_content(); ?></div>
				<?php edit_post_link(__('Edit this entry'),'','.'); ?>
			</div>
			<?php $comments = wp_count_comments($post->ID);
						if(!empty($comments->total_comments))
						{
							?>
			                <div class="total_comments">
			                    <span class="comments_count">
									<?php
			                        echo $comments->total_comments. ' REPLY'
			                        ?>
			                    </span>
			                    /
			                	<span class="goto_comment_form">WRITE A REPLY </span>
			                </div>
							<?php
						}
					  ?>
					  <?php
					  if ( comments_open() )
					  {
					  ?><div class="reviews-simple-feedback">
					        <div class="feedback-box">
					          <div class="comments_list">
					          	 <?php   comments_template( '/woocommerce/single-product-reviews.php' );   //woocommerce_get_template( 'single-product-reviews.php' ); ?>
					          </div>
					        </div>
					    </div>
					  <?php
					  }
					  ?>
		</article>
	<?php endwhile; endif; ?>

</div>

<div class="section7 similar-post-container">
	<h2 class="text-center">More posts you might like</h2>
	<?php 
		$args = array(
			'posts_per_page' => 3, 
			'category__in' => wp_get_post_categories($post->ID),
			'post__not_in' => array($post->ID)
		);
		$related = new WP_Query($args);
		if( $related->have_posts() ) { 
		  while( $related->have_posts() ) { $related->the_post(); ?>
		
		  	<article class="col-md-4 col-sm-12">
		      <a href="<?php the_permalink(); ?>">
		        <figure>
		          <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large', false, '' ); ?> 
		          <div class="blogPic1" style="background-image: url(<?php echo $src[0]; ?>);">
		            <p class="text-center"><span class="month"><?php the_time('M'); ?></span><br/>
		            <span class="day"><?php the_time('d'); ?></span></p>
		          </div>
		          <figcaption>
		            <h4><?php the_title(); ?></h4>
		            <p><?php echo get_excerpt(); ?></p>
		          </figcaption>
		        </figure>
		      </a>
		    </article>

		  <?php }
		}
		wp_reset_postdata(); ?>

</div>

<?php get_footer(); ?>