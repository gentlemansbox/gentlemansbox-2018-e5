<?php
/*
* Template Name: Premium Box
*/
get_header(); ?>

<?php
    $bgcolor = get_field( 'background_color_b1' );
    if ( !$bgcolor) {
        $bgcolor = '#fff';
    }
?>

<?php
$order = 0;
$order = new WC_Subscription($_GET[sub]);
$order_id = $order->id;
if($order_id !== 0){
  $order->update_status('cancelled');
}
?>
<style type="text/css">
    .page-template-page-premium-box form input[type=submit] {
        background-color: #313131!important;
        border-radius: 0px!important;
        font-size: .875em!important;
        padding: 10px 35px!important;
        border: 2px solid #313131!important;
        color: #fff!important;
        display: inline-block!important;
        font-weight: 700!important;
        margin: 0 10px 0 0!important;
        text-decoration: none!important;
        text-transform: uppercase!important;
        font-family: brandon_grotesquemedium!important;
        letter-spacing: 1px!important;
    }
</style>
<div id="topbanner" style="background-color:<?php echo $bgcolor; ?>;">
    <div class="wrp">
        <div class="banner_cont">
            <img class="banner_text__img banner_text__img__m" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
            <div class="banner_text">
                <h3 class="banner_text__subtitle"><span style="background-color:<?php echo $bgcolor; ?>;"><?php the_field( 'banner_subtitle_b1' ); ?></span></h3>
                <h1 class="banner_text__title"><?php the_field( 'banner_title_b1' ); ?></h1>
                <p class="banner_text__text"><?php the_field( 'banner_text_b1' ); ?></p>
            </div>
            <img class="banner_text__img banner_text__img__d" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content(); ?>
        </div>
    </article>
    <?php endwhile; endif; ?>
    </div>
</div>


<?php get_footer('new'); ?>
