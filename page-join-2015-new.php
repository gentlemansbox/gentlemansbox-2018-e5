<?php
/**
 * Template Name: Join new 2015
 */
get_header();
 ?>

 <div class="wrap">

 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 	<article class="post" id="post-<?php the_ID(); ?>">
 		<div class="entry">
 			<?php the_content(); ?>
 		</div>
 	</article>
 	<?php endwhile; endif; ?>
 </div>

 <?php if ( have_rows('blocks_b2') ): ?>
 <div class="nprows nprows1" style="padding:0;">
     <div class="wrp">
         <h2 class="nprows__title"><?php the_field('title_b2'); ?></h2>
         <h4 class="nprows__subtitle"><?php the_field('subtitle_b2'); ?></h4>
         <div class="nprows__row">
         <?php while ( have_rows('blocks_b2') ): the_row(); ?>
             <div class="nprows__el">
                 <div class="nblock">
                     <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                     <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                     <p class="block__subtitle"><?php the_sub_field('subtitle'); ?></p>
                     <a class="block__link" href="<?php the_sub_field('link'); ?>"></a>
                 </div>
             </div>
         <?php endwhile; ?>
         </div>
     </div>
 </div>
 <?php endif; ?>


<?php get_footer(); ?>
