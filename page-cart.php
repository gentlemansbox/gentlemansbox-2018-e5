<?php
/**
 * Template Name: Cart page
 */
 get_header(); ?>

<div class="nptitle">
    <h2 class="nptitle__title"><?php the_field('cp_visible_page_title', 'option'); ?></h2>
</div>

<?php if( have_rows('items', 'options') ): ?>
<div class="npropprod npropprod__d">
    <div class="wrp">
        <h2 class="npropprod__title"><?php the_field('cp_addition_products_title', 'option'); ?></h2>
        <h4 class="npropprod__subtitle"><?php the_field('cp_addition_products_subtitle', 'option'); ?></h4>
        <div class="npropprod__row">
        <?php while ( have_rows('items', 'options') ) : the_row(); ?>
            <div class="npropprod__el">
                <div class="box">
                    <?php $image = get_sub_field('product_image'); ?>
                    <?php if ( $image ) { ?>
                        <div class="box__img"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" /></div>
                    <?php } ?>
                    <h3 class="box__title"><?php the_sub_field('item_name');?></h3>
                    <p class="box__price">$<?php the_sub_field('item_price'); ?></p>
                    <p class="box__btncnt"><a href="<?php echo do_shortcode('[add_to_cart_url id="'.get_sub_field('product_id').'"]'); ?>" class="box__btn">Add to Box</a></p>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>
 
<div class="wrap">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</article>
<?php endwhile; endif; ?>
</div>

<?php get_footer('new'); ?>
