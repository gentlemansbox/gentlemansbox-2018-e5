<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
        <input type="search" id="s" name="s" value="" placeholder="Search..." />
        <input type="hidden" value="<?php _e('Search','html5reset'); ?>" id="searchsubmit" />
    </div>
</form>