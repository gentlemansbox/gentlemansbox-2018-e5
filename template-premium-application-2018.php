<?php /*
Template Name: Premium Aplication 2018
*/

get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.min.css" />

<main class="full-width premium-application-2018">
  <section class="landing-top">
    <div class="background clearfix">
      <div class="full-bg full-width" style="background-image:url(<?php the_field('header_image'); ?>);">
      </div>
      <div class="full-bg full-width mobile" style="background-image:url(<?php the_field('header_image_mobile'); ?>);">
      </div>
    </div>
    <div class="page-title vertical-align-parent">
      <div class="vertical-align-content">
        <?php the_field('header_content'); ?>
      </div>
    </div>
  </section>
  <section class="premium_how_it_works">
    <h2><?php the_field('how_it_works_title'); ?></h2>
    <p class="subtitle"><?php the_field('how_it_works_subtitle'); ?></p>
    <?php if( have_rows('how_it_works') ) { ?>
      <div class="clearfix max-width hiw-flex">
        <?php while ( have_rows('how_it_works') ) : the_row(); ?>
            <div class="hiw-item">
              <?php $image = get_sub_field('how_it_works_image'); ?>
              <img src="<?php echo $image; ?>" />
              <h3><?php the_sub_field('how_it_works_content_title'); ?></h3>
              <p><?php the_sub_field('how_it_works_content'); ?></p>
            </div>
        <?php endwhile; ?>
      </div>
    <?php } else {
        //nothing
      }
    ?>
       <?php if(is_page(783785)){ ?>
      <a class="premium-qualify" href="https://gentlemansbox.com/fb-premium-membership-application/">See if you qualify</a>
   <?php } else { ?>
    <a class="premium-qualify" href="https://gentlemansbox.com/premium-subscription-application/">See if you qualify</a>
    <?php } ?> 
  <section class="premium_past_boxes">
    <h2><?php the_field('past_boxes_title'); ?></h2>
    <?php if( have_rows('past_boxes_images') ) { ?>
      <div class="clearfix max-width past-boxes-flex">
        <?php while ( have_rows('past_boxes_images') ) : the_row(); ?>
            <div class="past-boxes-item">
              <?php $image = get_sub_field('box_image'); ?>
              <img src="<?php echo $image; ?>" />
              <p><?php the_sub_field('box_caption'); ?></p>
            </div>
        <?php endwhile; ?>
      </div>
      <h3 class="subtitle"><?php the_field('past_boxes_subtitle'); ?></h3>
      <p><?php the_field('past_boxes_content'); ?></p>
    <?php } else {
        //nothing
      }
    ?>
  </section>
  <section class="give-try dark-bg red-background">
    <p class='subtitle'><?php the_field('banner_text'); ?></p>
    <?php if(is_page(783785)){ ?>
      <a class="premium-qualify" href="https://gentlemansbox.com/fb-premium-membership-application/">See if you qualify</a>
   <?php } else { ?>
    <a class="premium-qualify" href="https://gentlemansbox.com/premium-subscription-application/">See if you qualify</a>
    <?php } ?> 
  </section>
  <section class="premium_gallery">
    <?php the_field('instagram_gallery'); ?>
  </section>
</main>
<?php get_footer('new'); ?>
