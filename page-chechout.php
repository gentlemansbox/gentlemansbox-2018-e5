<?php
/**
 * Template Name: New Checkout
*/
get_header();
?>

<div class="nptitle">
    <h2 class="nptitle__title">Checkout</h2>
</div>

<div class="wrap newchkout chccont">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="chccont__pag">
        <ul class="pag">
            <li><a href="#checkout-stage1" class="link link1 active" data-targ="1"><span class="desctop">1. Shipping Details</span><span class="mobile">1. Shipping</span></a></li>
            <li><a href="#checkout-stage2" class="link link2" data-targ="2"><span class="desctop">2. Payment Details</span><span class="mobile">2. Payment</span></a></li>
            <li><a href="#checkout-stage3" class="link link3" data-targ="3"><span class="desctop">3. Review Your Plan</span><span class="mobile">3. Review</span></a></li>
        </ul>
        <?php if ( is_user_logged_in() ) { ?>
            <a class="pag__login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a>
        <?php } else { ?>
            <a class="pag__login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>">have an account? login here</a>
        <?php } ?>
    </div>
	<div class="modifcheckout loading">
        <?php the_content(); ?>
    </div>
<?php endwhile; endif; ?>
</div>

<?php get_footer('new'); ?>
