<?php
//Shop page on remove subscriber products
function exclude_subscriber_products( $query ) {
	global  $wp_query;
	if($query->is_main_query() && (!empty($query->query_vars['post_type']) && $query->query_vars['post_type']= 'product') || (isset($_REQUEST['min_price']) && isset($_REQUEST['max_price'])) || $query->is_search || (!empty($query->query_vars['post__not_in']) && is_singular( 'product' )))
	{
		  global $wpdb, $wp_query;
		  $custom_result =  array('74','175',
'125',
'124',
'1156',
'1159',
'1160',
'14829',
'19600',
'19703',
'19704',
'19705',
'19706',
'19707',
'19708',
'19709',
'19710',
'19711',
'20265',
'20266',
'20267',
'20268',
'20269',
'20272',
'20273',
'20274',
'20275',
'20276',
'26107',
'26108',
'26109',
'26110',
'26111',
'26112',
'26113',
'26114',
'26115',
'26116',
'26117',
'26118',
'26119',
'26120',
'26121',
'26122',
'26124',
'26125',
'26126',
'26127',
'26128',
'26129',
'26130',
'26131',
'26132',
'26133',
'26134',
'26135',
'26136',
'26137',
'26138',
'26139',
'26140',
'26141',
'26142',
'26143',
'26146',
'26147',
'26148',
'26149',
'26150',
'26151',
'26152',
'26153',
'26154',
'26155',
'26156',
'26157',
'26158',
'26159',
'26160',
'26161',
'26162',
'26163',
'26164',
'26165',
'26166',
'26167',
'26168',
'26169',
'26170',
'26171',
'26172',
'26173',
'26174',
'26175',
'26176',
'26177',
'26178',
'26179',
'26180',
'26181',
'26182',
'26183',
'26184',
'26185',
'26186',
'26187',
'26188',
'26189',
'26190',
'26192',
'26193',
'26194',
'26195',
'26196',
'26197',
'26198',
'26199',
'26200',
'26201',
'26202',
'26203',
'26204',
'26205',
'26206',
'26207',
'26208',
'26209',
'26210',
'26211',
'26214',
'26215',
'26217',
'26218',
'26219',
'26220',
'26221',
'26222',
'26223',
'26224',
'26225',
'26227',
'26228',
'26229',
'26230',
'26231',
'26232',
'26235',
'26233',
'26234',
'26236',
'26237',
'26238',
'26239',
'26240',
'26241',
'26242',
'26243',
'26244',
'26245',
'26246',
'26247',
'26248',
'26249',
'26250',
'26251',
'26252',
'26253',
'26254',
'26255',
'26256',
'26257',
'26258',
'26259',
'26260',
'26261',
'26262',
'26263',
'26264',
'26265',
'26266',
'26267',
'26268',
'26269',
'26270',
'26271',
'26274',
'26275',
'26276',
'26277',
'26278',
'26279',
'26280',
'26281',
'26282',
'26283',
'26284',
'26285',
'26286',
'26287',
'26288',
'26289',
'26290',
'26291',
'26292',
'26293',
'26294',
'26295',
'26296',
'26297',
'26298',
'33217',
'33218',
'33219',
'33220',
'33221',
'48000',
'48182',
'64274',
'64275');
		 
		  
		  if(!empty($query->query_vars['post__in']) )
		  {
			$post__in = array_diff($query->query_vars['post__in'], $custom_result);
			$query->set( 'post__in', $post__in);
		  }
		  
		  if(!empty($query->query_vars['post__not_in']) && is_singular( 'product' ) )
		  {
		  	$query->set( 'post__not_in', array_merge($query->query_vars['post__not_in'], $custom_result));
		  }
		  else
		  {
		  	$query->set( 'post__not_in', $custom_result);
		  
		  }
		  
			 
	}
}
if(!is_admin())
{
	add_action( 'pre_get_posts', 'exclude_subscriber_products', 1000);
}