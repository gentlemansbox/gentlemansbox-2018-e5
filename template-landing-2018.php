<?php /*
Template Name: Landing Page 2018
*/ ?>
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/landing-page-2018.css" />
<!-- NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'landing-page-header' ); ?>

<!-- HERO -->
<?php get_template_part( 'template-parts/content', 'landing-page-hero-custom-fields' ); ?>

<!-- BANNER 1 -->
<?php
$banner_1 = get_field('banner_1');
if($banner_1){
  get_template_part( 'template-parts/content', 'landing-page-banner-1' );
}
?>

<!-- PAGE CONTENT -->
<?php
  get_template_part( 'template-parts/content', 'page' );
?>

<!-- BANNER 2 -->
 <?php
 $banner_2 = get_field('banner_2');
 if($banner_2){
   get_template_part( 'template-parts/content', 'landing-page-banner-2' );
 }
  ?>

<!-- LANDING PAGE - SUBSCRIPTION DETAILS -->
<?php
  get_template_part( 'template-parts/content', 'landing-subscriptions' );
?>

<?php get_footer('new'); ?>
