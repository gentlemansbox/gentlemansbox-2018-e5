<?php
/**
 * Template Name: Home 2017 Page
 */
 get_header(); ?>


  <!-- HOME HEADER -->
  <div class="videoContainer fh5co-hero">
    <div class="overlay"></div>
    <div class="videoInner">
      <video preload="true" autoplay loop volume = "0" poster = "" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/img/jim_beam-august-2.jpg');"> <!--background-image-->
        <!-- NO VIDEO RIGHT NOW
        <source src="split-test-resources/img/G-Box_Intro.mp4" type="video/mp4">
        <source src="split-test-resources/img/G-Box_Intro.webm" type="video/webm">
        -->
      </video>
    </div>
    <a href="https://gentlemansbox.com/join/">
      <h1><span>Discover the</span>Gentleman in you.</h1>
      <a class="join-link" href="https://gentlemansbox.com/join/"><button class="transparent">Join Today!</button></a>
    </a>
    <p class="text-center">SCROLL <br/> <span style="font-size: 40px; line-height: 35px;">&#x25be;</span></p>
   </div>

<!-- /////////////////// SECTION 1 /////////////////// -->

    <div class="fh5co-parallax section1" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <figure>
              <div class="turquoiseCircle"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/boxOutline.svg" aria-hidden="true" style="width: 60px; height: auto;"></div>
              <figcaption style="margin-bottom: 2em;">
                Average <br/> $100 Value
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-12">
            <figure>
              <div class="turquoiseCircle"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/thumbsup.svg" style="width: 50px; height: auto;" aria-hidden="true"></div>
              <figcaption style="margin-bottom: 2em;">
                Guaranteed <br/> Compliments
              </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-12">
            <figure>
              <div class="turquoiseCircle"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/calendarOutline.svg" style="width: 45px; height: auto;" aria-hidden="true"></div>
              <figcaption>
                Commitment <br/>Free
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>

<!-- /////////////////// SECTION 2 /////////////////// -->

    <div class="fh5co-section section2">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <?php the_field('box_details'); ?>
          </div>
          <div class="col-md-6">
            <!-- CONVERT TO ACF -->
            <div class="boxCutout"><img src="<?php bloginfo('stylesheet_directory'); ?>/library/images/whats_inside.png"></div>
            <!-- /CONVERT TO ACF -->
          </div>
          <div class="col-md-6">
            <div class="diamondList">
              <ol>
                <?php if( have_rows('box_stuff') ) {
                    while ( have_rows('box_stuff') ) : the_row();
                      ?><li><?php the_sub_field('stuff'); ?></li><?php
                    endwhile;
                } else {
                  //NOTHING
                }; ?>
              </ol>
            </div>
          </div>
          <div class="col-md-12">
            <a href="/join"><button class="turquoise">Join Now</button></a>
          </div>
        </div>
      </div>
    </div>

<!-- /////////////////// SECTION 3 /////////////////// -->

    <div class="fh5co-section section3">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
<iframe width="560" height="315" style="position:relative;margin-left:50%;left:-280px;margin-top:11em;" src="https://www.youtube.com/embed/EIs3580QZLY" frameborder="0" allowfullscreen></iframe>
            <h2 style="margin-top:1em;">How it works</h2>
          </div>

          <?php if( have_rows('how_it_works') ) {
            $i = 0;
            while ( have_rows('how_it_works') ) : the_row();
              $image = get_sub_field('step_image');
              $i++; ?>
              <div class="col-md-4 col-sm-12">
                <figure>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <div class="step"><?php echo $i; ?></div>
                  <figcaption>
                    <h4><?php the_sub_field('step_title') ?></h4>
                    <p><?php the_sub_field('step_content') ?></p>
                  </figcaption>
                </figure>
              </div>
            <?php endwhile;
          } else {
              // NOTHING
          } ?>

          <div class="col-md-12" style="text-align: center;">
            <a href="https://gentlemansbox.com/join/"><button class="turquoise">Join Now</button></a>
          </div>
        </div>
      </div>
    </div>

<!-- /////////////////// SECTION 4 /////////////////// -->

    <div class="fh5co-parallax section4" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
          <h2 class="text-center">Press</h2>
          <div class="row pressFullScreen">
          <?php if( have_rows('press_logos', 'options') ) {
            $i = 0;
            while ( have_rows('press_logos', 'options') ) : the_row();
              $image = get_sub_field('logo'); $i++; ?>
              <div class="text-center press">
                <a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
              </div>
              <?php if ( $i == 7 ) { ?>
                </div>
                <div class="row pressFullScreen">
              <?php } ?>
            <?php endwhile;
          } else {
            // NOTHING
          } ?>
        </div>

        <!-- SLIDER FOR MOBILE -->
        <div class="row pressSlider">
          <?php if( have_rows('press_logos', 'options') ) {
            $i = 0;
            while ( have_rows('press_logos', 'options') ) : the_row();
              $image = get_sub_field('logo'); $i++; ?>
              <div class="single-slide text-center">
                <a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
              </div>
            <?php endwhile;
          } else {
            // NOTHING
          } ?>
        </div>


      </div>
    </div>


<!-- /////////////////// SECTION 5 /////////////////// -->

    <div class="fh5co-section section5">
      <div class="container">

        <div class="max-width">
          <h2 style="text-align: center;">What's in Previous Boxes</h2>
          <div class="slider-for">
            <?php if( have_rows('slider', 'options') ) {
                while ( have_rows('slider', 'options') ) : the_row(); ?>

                    <div class="slick-slide clearfix">
                      <div class="slide-top full-width clearfix">
                        <h3><?php the_sub_field('box_month', 'options');?></h3>
                        <h3><?php the_sub_field('box_year', 'options');?></h3>
                      </div>
                      <div class="box-image">
                        <?php $image = get_sub_field('box_image', 'options'); ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                      </div>
                      <div class="box-info">
                        <h4><?php the_sub_field('box_title', 'options'); ?></h4>
                        <?php the_sub_field('box_items', 'options'); ?>
                        <hr>
                        <h3>TOTAL RETAIL COST OF THIS BOX:</h3>
                        <div class="box-cost vertical-align-parent">
                          <div class="vertical-align-content">$<?php the_sub_field('box_retail_cost', 'options'); ?></div>
                        </div>
                      </div>
                    </div>

                <?php endwhile;
            } else {
                // nothing
            } ?>
          </div>
          <div class="slider-nav">
            <?php if( have_rows('slider', 'options') ) {
                while ( have_rows('slider', 'options') ) : the_row(); ?>

                    <div class="slick-slide"><h3><?php the_sub_field('box_month', 'options');?></h3></div>

                <?php endwhile;
            } else {
                // nothing
            } ?>
          </div>
        </div>

      </div>
    </div>

<!-- /////////////////// SECTION 6 /////////////////// -->

    <div class="fh5co-parallax section6" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <figure>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tuxIcon.png" />
            <figcaption>
            <h3>Look Good.</h3>
            </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-12">
            <figure>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/muscleIcon.png" />
            <figcaption>
            <h3>Feel Good.</h3>
            </figcaption>
            </figure>
          </div>
          <div class="col-md-4 col-sm-12">
            <figure>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/handshakeIcon.png" />
            <figcaption>
            <h3>Be Good.</h3>
            </figcaption>
            </figure>
          </div>
          <div class="col-md-12 text-center">
            <a href="https://gentlemansbox.com/join/"><button class="transparent">Join Today!</button></a>
          </div>
        </div>
      </div>
    </div>

<!-- /////////////////// SECTION 7 /////////////////// -->

    <div class="fh5co-section section7">
      <div class="container">
        <div class="row">
          <h2 class="text-center">Insights</h2>

          <div class="post-preview-container">
            <?php
              $args = array( 'category__not_in' => array( 28, 29 ), 'posts_per_page' => 3 );
              $loop = new WP_Query( $args );
              while ( $loop->have_posts() ) : $loop->the_post();
            ?>
              <div class="col-md-4 col-sm-12">
                <a href="<?php the_permalink(); ?>">
                  <figure>
                    <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 300,200 ), false, '' ); ?>
                    <div class="blogPic1" style="background-image: url(<?php echo $src[0]; ?>);">
                      <p class="text-center"><span class="month"><?php the_time('M'); ?></span><br/>
                      <span class="day"><?php the_time('d'); ?></span></p>
                    </div>
                    <figcaption>
                      <h4><?php the_title(); ?></h4>
                      <p><?php echo excerpt(150); ?></p>
                    </figcaption>
                  </figure>
                </a>
              </div>
            <?php endwhile; ?>
          </div>

          <div class="col-md-12 text-center">
            <a href="https://gentlemansbox.com/blog/"><button class="turquoise">View All Posts</button></a>
          </div>
        </div>

        <div class="row hashtag">
          <a href="https://www.instagram.com/gentlemansbox/"><h4 class="text-center">#GENTLEMANSBOX</h4></a>
          <?php echo wdi_feed(array('id'=>'2')); ?>
        </div>
        </div>
      </div>
    </div>


<?php get_footer(); ?>
