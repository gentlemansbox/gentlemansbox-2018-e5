<?php

/**
 * Template Name: Join trial 2015
 */
get_header();
 ?>

 <?php 
 //
 // IS THIS TEMPLATE NEEDED? | REMOVE
 //
 ?>
  <?php if( have_rows('items', 'options') ) {
                while ( have_rows('items', 'options') ) : the_row(); ?>

                        <h3><?php the_sub_field('item_name', 'options');?></h3>
                     
                        <?php $image = get_sub_field('product_image', 'options'); ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
             
                          $<?php the_sub_field('item_price', 'options'); ?>
                          <a href="/cart/?add-to-cart=<?php the_sub_field('product_id');?>">Add to Cart</a>

                <?php endwhile;
            } else {
                // nothing
            } ?>

<?php get_footer(); ?>
