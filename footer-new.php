
<footer class="nfooter">
    <div class="wrp nfooter__d">
        <div class="nfooter__top">
            <a class="nfooter__logo" href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/mini_logo.svg" alt="Gentleman's Box Logo"></a>
            <div class="nfooter__soc">
                <a href="https://www.facebook.com/TheGentlemansBox" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                <a href="https://twitter.com/gentlemansbox/" target="_blank"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
                <a href="https://www.instagram.com/gentlemansbox/" target="_blank"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a>
                <a href="https://www.pinterest.com/gentlemansbox/" target="_blank"><span><i class="fa fa-pinterest" aria-hidden="true"></i></span></a>
                <a href="https://www.youtube.com/channel/UCRWfsE4lg_KtsXfxii89Ezg" target="_blank"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a>
            </div>
        </div>
        <div class="nfooter__bott">
            <div class="row">
                <div class="col-md-8">
                    <div class="colr">
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer1') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer2') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer3') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer4') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer5') ); ?></div></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="nnewsl">
                        <h3 class="nnewsl__title">the gentleman’s newsletter</h3>
                        <form action="//gentlemansbox.us9.list-manage.com/subscribe/post?u=a27b302d653aca677d832c322&amp;id=bac9623072" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*">
                            <input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button transparent">
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                            <div style="position: absolute; left: -5000px;"><input type="text" name="b_a27b302d653aca677d832c322_bac9623072" tabindex="-1" value=""></div>
                        </form>
                        <p class="nnewsl__text"><a href="/newsletter-sign-up/" id="newsletter_link">Receive exclusive Gentleman's Box offers<br/>and updates in your inbox each month.</a></p>
                    </div>
                </div>
            </div>
            <p class="ctext">&copy; Copyright <?php echo date("Y"); ?> Gentleman's Box. All Rights Reserved.</p>
        </div>
    </div>
    <div class="wrp nfooter__m">
        <div class="nfooter__top">
            <a class="nfooter__logo" href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/mini_logo.svg" alt="Gentleman's Box Logo"></a>
            <div class="nfooter__soc">
                <a href="https://facebook.com/TheGentlemansBox" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                <a href="https://twitter.com/gentlemansbox/" target="_blank"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
                <a href="https://www.instagram.com/gentlemansbox/" target="_blank"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a>
                <a href="https://www.pinterest.com/gentlemansbox/" target="_blank"><span><i class="fa fa-pinterest" aria-hidden="true"></i></span></a>
                <a href="https://www.youtube.com/channel/UCRWfsE4lg_KtsXfxii89Ezg" target="_blank"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a>
            </div>
        </div>
        <div class="nfooter__top">
            <div class="nnewsl">
                <h3 class="nnewsl__title">the gentleman’s newsletter</h3>
                <form action="//gentlemansbox.us9.list-manage.com/subscribe/post?u=a27b302d653aca677d832c322&amp;id=bac9623072" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*">
                    <input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button transparent">
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>
                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_a27b302d653aca677d832c322_bac9623072" tabindex="-1" value=""></div>
                </form>
                <p class="nnewsl__text">Receive exclusive Gentleman's Box offers<br/>and updates in your inbox each month.</p>
            </div>
        </div>
        <div class="nfooter__bott">
            <div class="fmrow">
                <div class="fmrow__col">
                    <div class="colr">
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer1') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer2') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer3') ); ?></div></div>
                    </div>
                </div>
                <div class="fmrow__col">
                    <div class="colr">
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer4') ); ?></div></div>
                        <div class="col"><div class="fmenu"><?php  wp_nav_menu( array('theme_location' => 'footer5') ); ?></div></div>
                    </div>
                </div>
            </div>
            <p class="ctext">&copy; Copyright <?php echo date("Y"); ?> Gentleman's Box. All Rights Reserved.</p>
        </div>
    </div>
    <a href="https://www.shopperapproved.com/reviews/gentlemansbox.com/" class="shopperlink"><img src="//www.shopperapproved.com/newseals/27452/333333-mini-icon.gif" style="border: 0" alt="Customer Reviews" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by Shopper Approved \251 '+d.getFullYear()+'.'); return false;" /></a><script type="text/javascript">(function() { var js = window.document.createElement("script"); js.src = '//www.shopperapproved.com/seals/certificate.js'; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); })();</script>
</footer>


<!-- JS -->
<script type="text/javascript">var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true; js.id = 'AddShoppers';js.src = ('https:' == document.location.protocol ? 'https://shop.pe/widget/' : 'http://cdn.shop.pe/widget/') + 'widget_async.js#582dce31bbddbd237b2539de';document.getElementsByTagName("head")[0].appendChild(js);</script>

<!-- REVCONTENT PIXEL -->
<?php if( is_checkout() ){ //ORDER CONFIRMATION PAGE ?>
	<img src="http://trends.revcontent.com/conv.php?t=KOP0A%2BHdvxZL7fKDQ2HUjDij7WAxEV1TPzyQRahRAfkZ670kYiAAAayhSN80W2%2BL" />
<?php } ?>
<!-- REVCONTENT PIXEL -->

<!-- SNAPCHAT CHECKOUT PIXEL -->
<?php if( is_checkout() ){ ?>
	<script type="text/javasript">
		snaptr('track', 'START_CHECKOUT');
	</script>
<?php } ?>
<!-- SNAPCHAT CHECKOUT PIXEL -->

<!-- BEGIN CRITEO TAGS -->

<!-- IF CURRENT PAGE IS HOMEPAGE, SHOP, CART, INDIVIDUAL PRODUCT -->
<!-- SALES TRANSACTION TAG IS ON ORDER-DETAILS.PHP TEMPLATE -->
<?php if( is_front_page() || is_shop() || is_cart() || is_product()) { ?>
    <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>

	<!-- SET CURRENT USER EMAIL ADDRESS -->
	<?php global $current_user;
		get_currentuserinfo();
		$email= $current_user->user_email;
	?>
	<!-- HOME PAGE TAG -->
	<?php if( is_front_page() ) { ?>
		<script type="text/javascript">window.criteo_q = window.criteo_q || [];var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";window.criteo_q.push({ event: "setAccount", account: 36897 },{ event: "setEmail", email: "<?php echo $email ?>" },{ event: "setSiteType", type: deviceType },{ event: "viewHome" });</script>
	<!-- SHOP PAGE TAG -->
	<?php } elseif ( is_shop() ) { ?>

		<!-- SET PRODUCT IDS -->
		<?php if ( have_posts() ) {
			$products = array();
		      while ( have_posts() ) {
	  			the_post();
	  			global $post;
	  			$product = new WC_Product( get_the_ID() );
	  			$productID = $product->id;
	  			array_push($products,$productID);
	  		}
	  	}?>

    <script type="text/javascript">window.criteo_q = window.criteo_q || [];var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";window.criteo_q.push({ event: "setAccount", account: 36897 },{ event: "setEmail", email: "<?php echo $email ?>" },{ event: "setSiteType", type: deviceType },{ event: "viewList", item:[ "<?php echo $products[0] ?>" , "<?php echo $products[1] ?>" , "<?php echo $products[2] ?>" ]});</script>

	<?php } elseif ( is_cart() ) { ?>
		<script type="text/javascript">
			window.criteo_q = window.criteo_q || [];
			var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
			window.criteo_q.push(
				{ event: "setAccount", account: 36897 },
				{ event: "setEmail", email: "<?php echo $email ?>" },
				{ event: "setSiteType", type: deviceType },
				{ event: "viewBasket", item: [
					<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = $cart_item['product_id'];
						$line_total = $cart_item['line_total'];
						$quantity = $cart_item['quantity'];
						$price = $line_total/$quantity;
					?>
						{id: <?php echo $product_id; ?>, price: <?php echo $price; ?>, quantity: <?php echo $quantity; ?>},
					<?php } ?>
				]}
			);
		</script>

	<?php } elseif ( is_product() ) { ?>
		<?php if ( have_posts() ) {
			the_post();
	  			global $post;
	  			$product = new WC_Product( get_the_ID() );
	  			$productID = $product->id;

		}?>

    <script type="text/javascript">window.criteo_q = window.criteo_q || [];var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";window.criteo_q.push({ event: "setAccount", account: 36897 },{ event: "setEmail", email: "<?php echo $email ?>" },{ event: "setSiteType", type: deviceType },{ event: "viewItem", item: "<?php echo $productID ?>" });</script>

	<?php } ?>

<?php } ?>
<!-- END CRITEO TAGS -->
   <!-- Add event to the add to cart button's click handler -->
<script type="text/javascript">	$("a").unbind('click').click(function() {
	var href = $(this).attr('href');
	if(href.includes('add-to-cart')) {
		var product_id = href.split("=").pop();
		fbq('track', 'AddToCart', {
			content_ids: [product_id],
			currency: 'USD'
		});
		pintrk('track', 'AddToCart', {
			product_id:".$product_id.",
			currency: 'USD'
		});
    snaptr('track', 'ADD_CART', {
      'currency': 'USD',
      'item_ids': ['.$product_id.']
    });
	} else {
		//nothing
	}
});
  </script>
    <?php if(is_archive()): ?>
        <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/js/blog-logic.js"></script>
    <?php endif; ?>
    <!-- SMOOTH SCROLLING -->
      <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.min.js"></script>

<?php wp_footer(); ?>
</body>
</html>