 <?php 
 //
 // REMOVE | UPDATED SHOP SLIDER TO A SLICK SLIDER
 //
 ?>



<?php
add_action( 'admin_menu', 'register_gentlemansbox_admin_custom_menu_page1' );
function register_gentlemansbox_admin_custom_menu_page1()
{
	add_menu_page( __( 'Product Page Top Slider Sildes' ), __( 'Product Page Top Slider Sildes' ), 'manage_options', 'is_fc-manage', 'is_fc_manage_callback', '', 14 ); 
	
}

add_action( 'wp_ajax_gentlemansboxtrade_new_admin_ajax', 'gentlemansboxtrade_new_admin_ajax_callback' );
add_action( 'wp_ajax_nopriv_gentlemansboxtrade_new_admin_ajax', 'gentlemansboxtrade_new_admin_ajax_callback' );

function gentlemansboxtrade_new_admin_ajax_callback(){
	// Check Securety issues.
	$nonce = wp_create_nonce( 'gentlemansboxtrade-nonce' );
	if ( wp_verify_nonce( $nonce, 'gentlemansboxtrade-nonce' ) ) 
	{
		// Dubble securety
		if (isset( $_POST['gentleman_setting_nonce_field'] ) ||  wp_verify_nonce( $_POST['gentleman_setting_nonce_field'], 'gentleman_setting_action' ) 
		) {
			
			// Save code here.
						
			if(isset($_REQUEST['gentleman_product_page_slide']))
			{
				update_option('gentleman_product_page_slide', $_REQUEST['gentleman_product_page_slide']);
			}
			
			
			
		
		  } 
  
	
	}
	wp_safe_redirect( wp_get_referer() );
	exit();
}


function is_fc_manage_callback()
{
	?>
	<style type="text/css">
    .clearfix:after {
    
        clear: both;
    
        content: " ";
    
        display: block;
    
        font-size: 0;
    
        height: 0;
    
        line-height: 0;
    
        visibility: hidden;
    
        width: 0;}
    .clearfix{ display:inline-block;}
    html[xmlns] .clearfix{ display:block;}
    .row{ 
      max-width:1100px; 
    
      margin:0px auto;
    
      padding:0 15px;}
    .av-wrapper{ 
    
      background:#f1f1f1;
    
      padding:20px 20px 20px 0;
    
      font-size:13px;
    
      color:#000; }
    .av-wrapper .avp-common{ margin-left:-3%;}
    .av-wrapper .field{ 
      display:block; 
      float:left;
      margin-left:3%;
    } 
    .av-wrapper .field-medium{ width:50%;}
    .av-wrapper .field-small{ width:30%;}
    .av-wrapper textarea{ 
      resize:vertical;
      width:100%;
      min-height:160px;} 
    
    .av-wrapper label{ 
      font-size:14px;
      color:#6f6d6d; }  
    
    av-wrapper input[type="text"], .av-wrapper select, .av-wrapper textarea, .av-wrapper input[type="number"]{ 
      border:1px #ccc solid;
      padding:5px 8px;
      font-size:14px;
      color:#6f6d6d;}
      
    .button{ 
    
      background:#d8d8d8;
      border: 1px solid #b0b0b0;
      border-radius:5px;
      padding:4px 10px;
      color:#606060;
      font-weight:700;
      margin:0 5px;}
    
      
    
    .button:hover{ background:#c7c7c7; }
    .field-row{ padding:12px 0px;}
    
      
    
    .av-seprator{ 
      display:block; 
      height:1px; 
      background:#dbdbdb;
      margin:10px 0;
      }
        </style>
        
        
	<div class="av-wrapper wrap">
      <div class="av-wrapper-in">
        <div class="clearfix">
          <h2 class="av-title">
            <?php _e('Product Page Top Slider Sildes'); ?>
          </h2>
          <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="main-form">
        	   <input type="hidden" name="action" value="gentlemansboxtrade_new_admin_ajax" />
               <?php
			   $gentleman_product_page_slide = get_option('gentleman_product_page_slide');
			   for($i=1; $i<=10; $i++)
			   {
					?>
                    <h3> Slide <?php echo $i; ?></h3>
					<div class="field-row clearfix">                                
						<div class="field-head">
						 <label><?php _e('Slider Image Full Path For - '); echo $i;?></label>
						</div>
						<div class="field-main"> 
							<input type="text" value="<?php echo $gentleman_product_page_slide[$i]['image']; ?>" class="field-medium field-large" name="gentleman_product_page_slide[<?php echo $i; ?>][image]" />       
						</div>
					</div>
					
					<div class="field-row clearfix">                                
						<div class="field-head">
						 <label><?php _e('Main Heading For- '); echo $i;?></label>
						</div>
						<div class="field-main"> 
							<textarea class="field-medium field-large" name="gentleman_product_page_slide[<?php echo $i; ?>][main_heading]"><?php echo $gentleman_product_page_slide[$i]['main_heading']; ?></textarea>    
						</div>
					</div>
					
					<div class="field-row clearfix">                                
						<div class="field-head">
						 <label><?php _e('Sub Heading For - '); echo $i;?></label>
						</div>
						<div class="field-main"> 
							<textarea   class="field-medium field-large" name="gentleman_product_page_slide[<?php echo $i; ?>][sub_heading]"><?php echo $gentleman_product_page_slide[$i]['sub_heading']; ?></textarea>      
						</div>
					</div>
				  
					<div class="field-row clearfix">                                
						<div class="field-head">
						 <label><?php _e('Shop Now Link - '); echo $i;?></label>
						</div>
						<div class="field-main"> 
							<input type="text" value="<?php echo $gentleman_product_page_slide[$i]['shop_link']; ?>" class="field-medium field-large" name="gentleman_product_page_slide[<?php echo $i; ?>][shop_link]" />       
						</div>
					</div>
                    
                    <hr />
                    <br />
                    <br />
					<?php
               }
               ?>      
                
                 <input type="hidden" name="_wpnonce" value="<?php echo $nonce = wp_create_nonce( 'gentlemansboxtrade-nonce' ); ?>" />
				 <?php   
                    wp_nonce_field();
                    wp_nonce_field( 'gentleman_setting_action', 'gentleman_setting_nonce_field' );			 ?>
        		 <div class="field-row clearfix"> 
                 	<?php submit_button( 'Save' ); ?>                	
                </div>
    		</form>
        </div>
        <!-- clearfix ends here--> 
      </div>
 	 <!-- av-wrapper-in ends here--> 
	</div>
	<?php
}
?>