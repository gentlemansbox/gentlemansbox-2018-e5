<?php

//
// IS THIS NEEDED | REMOVE | ADDS "APPERANCE > THEME OPTIONS"
//
	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/library/inc/options-framework.php';
	}
//
// IS THIS NEEDED | REMOVE | ADDS "APPERANCE > THEME OPTIONS"
//



	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_setup() {
		load_theme_textdomain( 'html5reset', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'html5reset_setup' );

	add_action( 'init', create_function( '', "date_default_timezone_set('America/Detroit');" ) );



//
// IS THIS NEEDED | WHY ADD JS & CSS THROUGH FUNCTIONS.PHP & HEADER.PHP | PICK ONE PLACE AND PUT THEM ALL THERE
//
	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;
        if ( function_exists( 'is_woocommerce' ) ) {
            if ( is_checkout() ) {
                wp_enqueue_script( 'minaScript', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', FALSE );
            } else {
                if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );
                wp_enqueue_style( 'html5reset-reset', get_template_directory_uri() . '/library/css/reset.css' );
                wp_enqueue_style( 'html5reset-style', get_stylesheet_uri() );
                wp_enqueue_style( 'html5reset-ie', get_template_directory_uri() . '/css/ie.css', array( 'html5reset-style' ), '20130213' );
                $wp_styles->add_data( 'html5reset-ie', 'conditional', 'lt IE 9' );
                if (strpos($_SERVER['REQUEST_URI'], '/my-account/add-payment-method') === false) {
	                wp_deregister_script( 'jquery' );
				}
				wp_enqueue_script( 'njquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.0.0', FALSE );
                wp_enqueue_script( 'retina-js', get_template_directory_uri() . '/library/js/retina.min.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'scrollto', get_template_directory_uri() . '/library/js/jquery.scrollTo.min.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'owl', get_template_directory_uri() . '/library/js/owl.carousel.min.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/library/js/jquery.bxslider.min.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'tablesaw', get_template_directory_uri() . '/library/js/tablesaw.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'Alphanum', get_template_directory_uri() . '/library/js/jquery.alphanum.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'functions', get_template_directory_uri() . '/library/js/functions.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script( 'woocommerce-functions', get_template_directory_uri() . '/library/js/woo.js', array('njquery'), '1.0.0', TRUE );
                wp_enqueue_script('colorbox-js', get_bloginfo('url').'/wp-content/plugins/lightbox-plus/js/jquery.colorbox.1.5.9-min.js');
                wp_enqueue_script( 'minaScript', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', FALSE );
            }
        }

	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );

//
// IS THIS NEEDED | WHY ADD JS & CSS THROUGH FUNCTIONS.PHP & HEADER.PHP | PICK ONE PLACE AND PUT THEM ALL THERE
//


//
// DO WE USE ALL OF THESE WIDGET AREAS?
//
	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			register_sidebar( array(
				'name'          => __( 'Footer Widgets', 'html5reset' ),
				'id'            => 'footer-widgets',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => __( 'How It Works Step 1', 'html5reset' ),
				'id'            => 'how-it-works-1',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => __( 'How It Works Step 2', 'html5reset' ),
				'id'            => 'how-it-works-2',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => __( 'How It Works Step 3', 'html5reset' ),
				'id'            => 'how-it-works-3',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}
//
// DO WE USE ALL OF THESE WIDGET AREAS?
//

	// Custom Menu
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

//
// REMOVE | DON'T USE POST NAVIGATION
//
	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}
//
// REMOVE | DON'T USE POST NAVIGATION
//

//
// IS THIS NEEDED? | REMOVE
//
	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}
//
// IS THIS NEEDED? | REMOVE
//


//
// REMOVE | DON'T USE THE TinyMCE Editor
//
	//Customize TinyMCE Editor
		function my_theme_add_editor_styles() {
			add_editor_style( 'library/css/custom-editor-style.css' );
		}
		add_action( 'init', 'my_theme_add_editor_styles' );
//
// REMOVE | DON'T USE THE TinyMCE Editor
//


	//Declare WooCommerce Support
	add_theme_support('woocommerce');

	if( empty($_REQUEST['test_user']) )
	{
		// Remove WooCommerce Stylesheets for Customization
		add_filter( 'woocommerce_enqueue_styles', '__return_false' );
	}


	//Remove Default Sorting on Products Page
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

	//Remove Reviews Tab on Products Page
	add_filter( 'woocommerce_product_tabs', 'custom_woo_remove_reviews_tab', 98);
	function custom_woo_remove_reviews_tab($tabs) {

	unset($tabs['reviews']);

	return $tabs;
	}

	// Removes tabs from their original location
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

	// Inserts tabs under the main right product content
	add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

	//Disable Admin Bar for Non-Admins (WooCommerce Account)
	add_action('after_setup_theme', 'remove_admin_bar');

	function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
	}

	//Remove Company Name from Checkout Field
	add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

	function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['shipping']['shipping_company']);
	return $fields;
	}

	//Custom State Selection (Add Puerto Rico Option)
	add_filter( 'woocommerce_states', 'custom_woocommerce_states' );

	function custom_woocommerce_states( $states ) {
		$states['US'] = array(
		'AL' => 'Alabama',
		'AK' => 'Alaska',
		'AS' => 'American Samoa',
		'AZ' => 'Arizona',
		'AR' => 'Arkansas',
		'CA' => 'California',
		'CO' => 'Colorado',
		'CT' => 'Connecticut',
		'DE' => 'Delaware',
		'DC' => 'District of Columbia',
		'FL' => 'Florida',
		'GA' => 'Georgia',
		'GU' => 'Guam',
		'HI' => 'Hawaii',
		'ID' => 'Idaho',
		'IL' => 'Illnois',
		'IN' => 'Indiana',
		'IA' => 'Iowa',
		'KS' => 'Kansas',
		'KY' => 'Kentucky',
		'LA' => 'Louisiana',
		'ME' => 'Maine',
		'MD' => 'Maryland',
		'MH' => 'Marshall Islands',
		'MA' => 'Massachusetts',
		'MI' => 'Michigan',
		'FM' => 'Micronesia',
		'MN' => 'Minnesota',
		'MS' => 'Mississippi',
		'MO' => 'Missouri',
		'MT' => 'Montana',
		'NE' => 'Nebraska',
		'NV' => 'Nevada',
		'NH' => 'New Hampshire',
		'NJ' => 'New Jersey',
		'NM' => 'New Mexico',
		'NY' => 'New York',
		'NC' => 'North Carolina',
		'ND' => 'North Dakota',
		'MP' => 'Northern Marianas',
		'OH' => 'Ohio',
		'OK' => 'Oklahoma',
		'OR' => 'Oregon',
		'PW' => 'Palau',
		'PA' => 'Pennsylvania',
		'PR' => 'Puerto Rico',
		'RI' => 'Rhode Island',
		'SC' => 'South Carolina',
		'SD' => 'South Dakota',
		'TN' => 'Tennessee',
		'TX' => 'Texas',
		'UT' => 'Utah',
		'VT' => 'Vermont',
		'VA' => 'Virginia',
		'VI' => 'Virgin Islands',
		'WA' => 'Washington',
		'WV' => 'West Virginia',
		'WI' => 'Wisconsin',
		'WY' => 'Wyoming',
		'AA' => 'Armed Forces (AA)',
		'AE' => 'Armed Forces (AE)',
		'AP' => 'Armed Forces (AP)'
	);

	return $states;
	}

//
// ARE THESE DISPLAYED ANYWHERE | NEED TO BE DISPLAYED
//
	//Register Testimonials
	function testimonial_post_type() {

	   // Labels
		$labels = array(
			'name' => _x("Testimonials", "post type general name"),
			'singular_name' => _x("Testimonial", "post type singular name"),
			'menu_name' => 'Testimonials',
			'add_new' => _x("Add New", "testimonial item"),
			'add_new_item' => __("Add New Testimonial"),
			'edit_item' => __("Edit Testimonial"),
			'new_item' => __("New Testimonial"),
			'view_item' => __("View Testimonial"),
			'search_items' => __("Search Testimonials"),
			'not_found' =>  __("No Testimonials Found"),
			'not_found_in_trash' => __("No Testimonials Found in Trash"),
			'parent_item_colon' => ''
		);

		// Register post type
		register_post_type('testimonial' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'testimonial_post_type', 0 );
//
// ARE THESE DISPLAYED ANYWHERE | NEED TO BE DISPLAYED
//


	//Register Button Shortcode
	function custom_button( $atts, $content = null ) {
	extract(shortcode_atts(array(
	'url' => '',
	'target'  => '',
	'color' => '',
	'size'  => '',
	'align' => '',
	'type'  => '',
	'download' => '',
	), $atts));

	$color = ($color) ? ' '.$color. '' : '';
	$align = ($align) ? ' '.$align. '' : '';
	$size = ($size) ? ' '.$size. '' : '';
	$target = ($target == '_blank') ? ' target="_blank"' : '';
	$type = ($type) ? ' '.$type. '' : '';
	$download = ($download == 'true') ? ' download' : '';

	$out = '<a' .$download.$target. ' class="btn' .$color.$size.$align.$type. '" href="' .$url. '">' .do_shortcode($content). '</a>';

	return $out;
	}
	add_shortcode('button', 'custom_button');

	//Register Mailchimp Email Signup Shortcode
	function gentlemansbox_mailinglist( $atts ) {
	    $html =
	        '<div id="mc_embed_signup">
	         <form action="//gentlemansbox.us9.list-manage.com/subscribe/post?u=a27b302d653aca677d832c322&amp;id=bac9623072" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*">
	            <input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button">
	            <div id="mce-responses" class="clear">
	                <div class="response" id="mce-error-response" style="display:none"></div>
	                <div class="response" id="mce-success-response" style="display:none"></div>
	            </div>
	            <div style="position: absolute; left: -5000px;"><input type="text" name="b_a27b302d653aca677d832c322_bac9623072" tabindex="-1" value=""></div>
	         </form>
	         </div>';
	      return $html;
	}
	add_shortcode('mailchimp', 'gentlemansbox_mailinglist');




//
// REMOVE | WE NOW HAVE A RECENT POSTS TEMPLATE PART
//
	//Recent Blog Post Article
	function blog_recentpost_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/recentpost' );
	return ob_get_clean();
	}
	add_shortcode( 'blogpost', 'blog_recentpost_shortcode' );
//
// REMOVE | WE NOW HAVE A RECENT POSTS TEMPLATE PART
//


//
// REMOVE | IS THIS USED OR NEEDED
//
	//Register Break Shortcode
    function break_shortcode( $atts ) {
      $html = '<div class="break"></div>';
          return $html;
    }
	add_shortcode('line_break', 'break_shortcode');
	//Register Facebook Shortcode
    function facebook_social( $atts ) {
      $html = '<a class="social facebook" href="https://www.facebook.com/TheGentlemansBox?ref=br_tf" target="_blank">Gentlemans Box on Facebook</a>';
          return $html;
    }
	add_shortcode('facebook', 'facebook_social');
	//Register Twitter Shortcode
    function twitter_social( $atts ) {
      $html = '<a class="social twitter" href="https://twitter.com/gentlemansbox/" target="_blank">Gentlemans Box on  Twitter</a>';
          return $html;
    }
	add_shortcode('twitter', 'twitter_social');
	//Register Instagram Shortcode
    function instagram_social( $atts ) {
      $html = '<a class="social instagram" href="http://instagram.com/gentlemansbox/" target="_blank">Gentlemans Box on Instagram</a>';
          return $html;
    }
	add_shortcode('instagram', 'instagram_social');
	//Register Google Plus Shortcode
    function gplus_social( $atts ) {
      $html = '<a class="social gplus" href="https://plus.google.com/" target="_blank">Gentlemans Box on Google Plus</a>';
          return $html;
    }
	add_shortcode('googleplus', 'gplus_social');
	//Register YouTube Shortcode
    function youtube_social( $atts ) {
      $html = '<a class="social youtube" href="https://www.youtube.com/GentlemansBox" target="_blank">Gentlemans Box YouTube Channel</a>';
          return $html;
    }
	add_shortcode('youtube', 'youtube_social');
	//Register Pinterest Shortcode
    function pinterest_social( $atts ) {
      $html = '<a class="social pinterest" href="https://www.pinterest.com/gentlemansbox/" target="_blank">Gentlemans Box Pinterest Channel</a>';
          return $html;
    }
	add_shortcode('pinterest', 'pinterest_social');
	//Register Hashtag #besavvy
    function hashtag_social( $atts ) {
      $html = '<div class="hashtag">#besavvy</div>';
          return $html;
    }
	add_shortcode('#besavvy', 'hashtag_social');
	//Gentleman's Quotes Shortcode
	function quotes_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/quotes' );
	return ob_get_clean();
	}
	add_shortcode( 'quotes', 'quotes_shortcode' );
	//Gentleman's Tips Shortcode
	function tips_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/tips' );
	return ob_get_clean();
	}
	add_shortcode( 'tips', 'tips_shortcode' );
//
// REMOVE | IS THIS USED OR NEEDED
//


	//Gentleman's Box Blog All Categories
	function all_categories_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/categories' );
	return ob_get_clean();
	}
	add_shortcode( 'all_categories', 'all_categories_shortcode' );

	//Register FAQ Question
	function faq_question($atts, $content = null) {
	extract(shortcode_atts(array(
    'id'	=> '',
    ), $atts));
	$id = ($id) ? ''.$id.'' : '';
	$out = '<h3 class="question" id="'.$id.'">' . do_shortcode($content) . '</h3>';
    return $out;
	}
	add_shortcode('question', 'faq_question');
	//Remove Auto Paragraph from Shortcode on FAQ Page
	add_action('pre_get_posts', 'remove_autop_from_home');
		function remove_autop_from_home() {
		// check to see if we are on the home page
		if(is_page(17)) {
	    remove_filter('the_content', 'wpautop');
		}
	}

	//Register FAQ Answer
	function faq_answer($atts, $content = null) {
	   return '<div class="answer">' . do_shortcode($content) . '</div>';
	}
	add_shortcode('answer', 'faq_answer');

	//Add Shortcode Functionally to Widgets
	add_filter('widget_text', 'do_shortcode');


//
// THESE CAN BE REMOVED ONCE BLOG/ARCHIEVE PAGES UPDATED
//
	//Replace Excerpt Ellipsis with Permalink
	function new_excerpt_more( $more ) {
	return '...';
	}
	add_filter( 'excerpt_more', 'new_excerpt_more' );
	// Custom Excerpt
	function excerpt($limit) {
	  $excerpt = explode(' ', get_the_excerpt(), $limit);
	  if (count($excerpt)>=$limit) {
	    array_pop($excerpt);
	    $excerpt = implode(" ",$excerpt).'...';
	  } else {
	    $excerpt = implode(" ",$excerpt);
	  }
	  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	  return $excerpt;
	}
	function content($limit) {
	  $content = explode(' ', get_the_content(), $limit);
	  if (count($content)>=$limit) {
	    array_pop($content);
	    $content = implode(" ",$content).'...';
	  } else {
	    $content = implode(" ",$content);
	  }
	  $content = preg_replace('/\[.+\]/','', $content);
	  $content = apply_filters('the_content', $content);
	  $content = str_replace(']]>', ']]&gt;', $content);
	  return $content;
	}
//
// THESE CAN BE REMOVED ONCE BLOG/ARCHIEVE PAGES UPDATED
//

//
// LIMIT BLOG EXCERPT | USED IN 2017 UPDATE
//
function get_excerpt(){
	$excerpt = get_the_excerpt();
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 125);
	$excerpt = $excerpt.'...';
	return $excerpt;
}
//
// LIMIT BLOG EXCERPT | USED IN 2017 UPDATE
//

//
// IS THIS NEEDED? | REMOVE
//
	//Cleanup Output of Shortcodes
	if( !function_exists('wpex_fix_shortcodes') ) {
		function wpex_fix_shortcodes($content){
			$array = array (
				'<p>[' => '[',
				']</p>' => ']',
				']<br />' => ']'
			);
			$content = strtr($content, $array);
			return $content;
		}
		add_filter('the_content', 'wpex_fix_shortcodes');
	}
//
// IS THIS NEEDED? | REMOVE
//


	// Post Thumbnails
	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'blog', 480, 270, array( 'center', 'top' ) );
		add_image_size( 'lastmonth', 800 ); // 800 pixels wide (and unlimited height)
	}



//
// IS THIS USED? | REMOVE
//
function gift_message_clear() {
    $gift_args = array(
            'post_type' => 'shop_order',
            'post_status' => 'wc-completed',
            'posts_per_page' => -1
        );
    $complete_orders_query = new WP_Query( $gift_args );
    if ( $complete_orders_query->have_posts() ) {
        while ( $complete_orders_query->have_posts() ) {
            global $post, $wpdb;
            $complete_orders_query->the_post();
            $gift_post_id = get_the_id();
            $gift_message = $post->post_excerpt;
            if (!empty($gift_message)) {
               $wpdb->update(
                   'wp_posts',
                   array(
                       'post_content' => $gift_message,
                       'post_excerpt' => '',
                   ),
                   array( 'ID' => $gift_post_id )
               );
            }
        }
    }
    wp_reset_postdata();
    $to = 'paul@gentlemansbox.com';
    $subject = 'Gift Cron Test';
    $message = 'The gift cron job run successfully';
    wp_mail( $to, $subject, $message );
}
add_action('gift_message_clear', 'gift_message_clear');
//
// IS THIS USED? | REMOVE
//


/** For subscription Plugin v2
 * Remove the "Change Payment Method" button from the My Subscriptions table.
 *
 * This isn't actually necessary because @see eg_subscription_payment_method_cannot_be_changed()
 * will prevent the button being displayed, however, it is included here as an example of how to
 * remove just the button but allow the change payment method process.
 */
function eg_remove_my_subscriptions_button( $actions, $subscription ) {


    $order = wc_get_order( $subscription );
    $orderID = $order->id;
    $items = $order->get_items();
    $productID ='';
    foreach( $items as $custom_key =>$custom_val )
    {
        if(!empty($custom_val['product_id']))
        {
            $productID = $custom_val['product_id'];
        }
    }
    $isGiftPackOptions = (get_post_meta($productID,'gift_pack1'));
    $isHideCancel =  $isGiftPackOptions[0][0];

    if ($isHideCancel) {
        foreach ( $actions as $action_key => $action ) {
            switch ( $action_key ) {
    //			case 'change_payment_method':	// Hide "Change Payment Method" button?
    //			case 'change_address':			// Hide "Change Address" button?
    //			case 'switch':					// Hide "Switch Subscription" button?
    //			case 'renew':					// Hide "Renew" button on a cancelled subscription?
    //			case 'pay':						// Hide "Pay" button on subscriptions that are "on-hold" as they require payment?
    //			case 'reactivate':				// Hide "Reactive" button on subscriptions that are "on-hold"?
                case 'cancel':					// Hide "Cancel" button on subscriptions that are "active" or "on-hold"?
                    unset( $actions[ $action_key ] );
                    break;
                default:
                    error_log( '-- $action = ' . print_r( $action, true ) );
                    break;
            }
        }
    }
	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'eg_remove_my_subscriptions_button', 100, 2 );

/*add_filter( 'woocommerce_shipment_tracking_default_provider', 'custom_woocommerce_shipment_tracking_default_provider' );

function custom_woocommerce_shipment_tracking_default_provider( $provider ) {
	$provider = 'fedex'; // Replace this with the name of the provider. See line 42 in the plugin for the full list.

	return $provider;
}*/
add_action( 'show_user_profile', 'cancel_profile_fields' );
add_action( 'edit_user_profile', 'cancel_profile_fields' );

function cancel_profile_fields( $user ) { ?>

	<h3>Cancel Subscription Information</h3>

	<table class="form-table">

		<tr>
			<th><label for="cancel_reason">Cancel Reason</label></th>

			<td>
				<input type="text" name="cancel_reason" id="cancel_reason" value="<?php echo esc_attr( get_the_author_meta( 'cancel_reason', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th><label for="cancel_comment">Cancel Comment</label></th>

			<td>
				<input type="text" name="cancel_comment" id="cancel_comment" value="<?php echo esc_attr( get_the_author_meta( 'cancel_comment', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

	</table>
<?php }

/* Subscription Cancellation additions */

define ("SUBSCRIPTION_DISCOUNT_DEBUG", false);
define ("SUBSCRIPTION_DISCOUNT_STAGE", false);// whether its staging or live TRUE means staging, false means live or activedev
define ("SUBSCRIPTION_RENEWS_ON", 10);


if (SUBSCRIPTION_DISCOUNT_STAGE) {
    define ("SUBSCRIPTION_AVAILED_FIELD", 'field_55ef6634e2eef'); //staging
}
else {
    define ("SUBSCRIPTION_AVAILED_FIELD", 'field_5605ffff5cb58'); // live
}
/*
 * Function to apply free item to subscription and email admin on the same
 *
 */
 function apply_free_item($sub){
	 // specify the order_id so WooCommerce knows which to update
	 $order_data = array(
    'order_id' => $sub,
    'customer_note' => 'Add free gift.'
		);
		// print_r($order_data);
		// update the customer_note on the order
		wc_update_order( $order_data );
}


/*
 * Main function to apply subscription discount and email admin on the same
 *
 */

function apply_subscription_discount($sub){
	$subscription_id = $sub; //works
	// header('Content-Type: application/json');
	// // template result "status" is 0 (zero) when not succsccsful
	// $ret = array("status"=> 0, "message"=> "Unable to apply discount");

	// get current user and check if
	// use has already availed discount (in any subscription)
	// stored as meta "availed_discounted_subscription" (true/false or 1/0)
	// created using ACF (Advanced Custom Field plugin) - visible in User Profile (admin) page
	$user_ID = get_current_user_id();
	// field: availed_discounted_subscription
	$user_availed_discount = get_field(SUBSCRIPTION_AVAILED_FIELD, "user_$user_ID");
	//$usermeta_availed_discount = get_user_meta ($user_ID, "availed_discounted_subscription", true);

	// If user has alrady availed discount do not allow
	if (!SUBSCRIPTION_DISCOUNT_DEBUG && $user_availed_discount) {
			$ret["message"] = "User has already availed discount. Unable to allow more discount.";
			// return back with appropriate message
			die(json_encode($ret));
	}

	$subscriptionv2 = wcs_get_subscription($subscription_id);

	if (!wcs_is_subscription($subscriptionv2)) {
			$ret["message"] = "Unable to locate Subscription!";
			die(json_encode($ret));
	}
	// Get Subscription details (associated with the user)
	$subscription_details = wcs_get_subscription_in_deprecated_structure($subscriptionv2);
	// If subscription does not exist return back with approiate error message
	if (empty($subscription_details)) {
			$ret["message"] = "Unable to locate a subscription!";
			die(json_encode($ret));
	}
	$subscription_details['subscription_id'] = $subscription_id;

	// get Product ID from subscription data/details and all
	// discount related meta
	// These meta are created usig ACF and editable from Product' Edit Page (admin)
	$discount_details = array();
	$product_id = $subscription_details['product_id'];
	$order_id = $subscription_details['order_id'];
	$order = new WC_Order($order_id);
	$discount_details['discount_amount'] = $discount_amount = get_post_meta($product_id, 'discount_amount', true ); //field_55ee0405b8812
	$discount_details['discount_duration'] = $discount_duration = get_post_meta( $product_id, "discount_duration" , true);//field_55ee04a2b8814
	//
	// If no discount amount is set, go back
	if (empty($discount_amount) || $discount_amount == 0) {
			$ret["message"] = "No discount applicable for this Subscription!";
			die(json_encode($ret));
	}

	// Retrieve Order Item from order
	///$order_item = get_order_item_by_product_id( $order_id, $product_id );
	$items = $subscriptionv2->get_items();
	$item_ids = array_keys($items);
	$item  = array_pop( $items );
	$item_id = array_pop($item_ids);
	$item['id']  = $item_id;
	$product_name = $item['name'];

	// backup original data/meta of the Order Item = Subscription
	backup_subscription_details($subscriptionv2, $subscription_id, $order, $item, $order_id, $product_id);

	// Set discounted values (replacing the original data/meta of the Order item = subscription)
	// and get the updated meta that include next payment date (as int)
	$updates = set_subscription_discount($subscriptionv2, $subscription_id, $subscription_details, $order_id, $item, $discount_details);

	// Get updated data
	$period = $updates['_d_subscription_period'];
	$start_date = $updates['_this_subscription_discounted_on'];
	//var_dump($updates['next_payment_datestamp']);
	$next_payment_date = date("Y-m-d", $updates['next_payment_datestamp']);
	// add order comment
	$order_note = sprintf( __( 'Discount offer for subscription %s was selected by the subscriber from their account page.', 'woocommerce-subscriptions' ),
					$product_name);
	$subscriptionv2->add_order_note($order_note);

	//do_action( 'customer_changed_subscription_to_' . $status_message, $_GET['subscription_key'] );
	// Get user's data
	$user = get_userdata($user_ID);
	$user_name = $user->display_name;
	$user_email = $user->user_email;
	// Get admin email
	$to = $admin_email = get_option('admin_email');

	// Send email
	$subject = "Subscription Discount for subscription #$subscription_id selected by user";
	$NL = "\n";
	$message = sprintf(__('Instead of canceling %s (%s) has selected discount option on subscription "%s" from their account page.' .$NL .$NL .
							'Details:'.$NL.
							'Subscription ID: %s'.$NL.
							'Order ID: %s'.$NL.
							'Subscription: %s'.$NL.
							'Discounted Subscription Amount: $%s'.$NL.
							'Discount Availed on: %s'.$NL.
							'Discount Duration: %s %s'.$NL.
							'Next payment on: %s'.$NL.
							'User Name: %s'.$NL.
							'User Email: %s'.$NL,
					'woocommerce-subscriptions'),
					$user_name, $user_email, $product_name,
					$subscription_id, $order_id, $product_name, $discount_amount,
					$start_date, $discount_duration, $period, $next_payment_date,
					$user_name, $user_email
			);

	$header = "From:$admin_email\r\n";
	$retval = mail ($to,$subject,$message,$header);

	$ret['status'] = 1;
	$ret['message'] = sprintf( __( 'Your discounted subscription for %s is now active for %s %s.', 'woocommerce-subscriptions' ),
					$product_name, $discount_duration, $period);
	//$ret['email'] = $message;

	// add notice
	// WC_Subscriptions::add_notice($ret['message'], 'success' );

	// Return ajax response
	die(json_encode($ret));
	//die(json_encode(get_post_meta($subscription_key)));
}

add_action( 'wp_ajax_apply_subscription_discount', 'apply_subscription_discount' );


/*
 * Function to back subscription details before discount is applied
 * @param $order_item   An Order Item  = single Subscription Product as Order Item
 * @param $order_id     Id of the Order containing the subscription
 */
function backup_subscription_details($subscription, $subscription_id, $order, $order_item, $order_id, $product_id) {
    //global $wpdb;
    //$wp_woocommerce_order_itemmeta = "wp_woocommerce_order_itemmeta";
    $period = "month";

    // get order item id
    $order_item_id = $order_item['id'];
    //$meta = $order_item['item']['item_meta'];
    $backup_prefix = "_bk";

    // Backup meta items of the subscription (order item)
    $backup_meta_map = array(
//        "_line_subtotal_tax"=> "",
//        "_line_tax"=> "",
        "_line_subtotal"=> "",
        "_line_total"=> "",


//        "_recurring_line_subtotal"=> "",
//        "_recurring_line_subtotal_tax"=> "",
//        "_recurring_line_tax"=> "",
//        "_recurring_line_total"=> "",
//        "_subscription_recurring_amount"=> "",
//        "_subscription_interval"=> "",
//        "_subscription_length"=> "",
//        "_subscription_period"=> "",

////        "_line_tax_data"=> "",
////        "_subscription_completed_payments"=> "",
//        "_subscription_end_date"=> "",
//        "_subscription_expiry_date"=> "",
//        "_subscription_failed_payments"=> "",
//        "_subscription_sign_up_fee"=> "",
//        "_subscription_start_date"=> "",
//        "_subscription_status"=> "",
//        "_subscription_suspension_count"=> "",
//        "_subscription_trial_expiry_date"=> "",
//        "_subscription_trial_length"=> "",
//        "_subscription_trial_period"=> "",
        "_has_subcription_data_backup" => 1
    );

    // Is there already a backup? if yes do not backup again
    $backup_found = wc_get_order_item_meta($order_item_id, "{$backup_prefix}_has_subcription_data_backup", true);
    if (empty($backup_found)) {
        foreach ($backup_meta_map as $key => $value) {
            // get original meta values
            if (empty($value)) {
                $backup_meta_map[$key] = wc_get_order_item_meta($order_item_id, $key, true);
            }
        }
        foreach ($backup_meta_map as $key => $value) {
            // store a backup of the original meta values
            wc_update_order_item_meta($order_item_id, $backup_prefix . $key, $value);
        }
    }

    // Retrieve order recurring total and keep a backup (not required)
    //// $order_recurring_total = get_post_meta($order_id, '_order_recurring_total', true);
    //// update_post_meta($order_id, $backup_prefix. '_order_recurring_total', $order_recurring_total);

//    // Get the original order total
//    // Store a backup
    $order_total = get_post_meta($subscription_id, '_order_total', true);
    update_post_meta($subscription_id, $backup_prefix. '_order_total', $order_total);
    $billing_period = get_post_meta($subscription_id, '_billing_period', true);
    update_post_meta($subscription_id, $backup_prefix. '_billing_period', $billing_period);
    $billing_interval = get_post_meta($subscription_id, '_billing_interval', true);
    update_post_meta($subscription_id, $backup_prefix. '_billing_interval', $billing_interval);

}

/*
 * Function to set subscription discount
 *
 */
function set_subscription_discount($subscription, $subscription_id, $subscription_details, $order_id, $order_item, $discount_details) {

//    global $wpdb;
//    $wp_woocommerce_order_itemmeta = "wp_woocommerce_order_itemmeta";
    $period = "month";

    // Get order id & order item = subscription
    $order_item_id = $order_item['id'];
//    $item = $order_item['item'];
//    $meta = $item['item_meta'];
    $backup_prefix = "_bk";

    // Get discount data
    $duration = $discount_details['discount_duration'];
    $amount = $discount_details['discount_amount'];

    // Prepare dates
    $now = strtotime("now");
    //$bill_on = strtotime("+2 {$period}");
    // Discount date
    $start_date = date('Y-m-d H:i:s', $now);
    // calculate next billing date
    if (SUBSCRIPTION_DISCOUNT_DEBUG) {
        $bill_on = strtotime("+1 day");
    }
    else {
        $bill_on = get_subscription_renewal_date($start_date, $duration, $period);// strtotime("+{$duration} {$period}");
    }
    if (is_string($bill_on)) {
        $bill_on = strtotime($bill_on);
    }
    $bill_date = date('Y-m-d H:i:s', $bill_on);

    // Update meta items
    $updates = array(
      "_d_subscription_period" => $period,
      "_d_subscription_interval" => floatval($duration),

//      "_recurring_line_subtotal" => $amount,
//      "_recurring_line_total" => $amount,
//      "_subscription_recurring_amount" => $amount,
      "_line_subtotal" => $amount,
      "_line_total" => $amount,
////      "_subscription_end_date" => 1,
////      "_subscription_expiry_date" => $exp_date
////      "_subscription_start_date" => $start_date,
//      "_this_subscription_is_discounted" => 1, // additional meta to mark discounted flag
//      "_this_subscription_discounted_on" => $start_date,  // additional meta to mark discounted date
//      "_this_subscription_discount_expires_on" => $bill_date,  // additional meta to mark discounted date
//      "_this_subscription_order_item_id" => $order_item_id,  // additional meta to mark item id
//      "_this_subscription_order_id" => $order_id,  // additional meta to mark order id
    );

    // Update discount values to subscription (order item) meta
    foreach ($updates as $key => $value) {
        wc_update_order_item_meta($order_item_id, $key, $value);
    }

    // Update recurring order total
    $new_order_recurring_total = $amount;
    update_post_meta($subscription_id, '_order_total', $new_order_recurring_total);
    update_post_meta($subscription_id, '_discounted_subscription_amount', $amount);
    update_post_meta($subscription_id, '_billing_period', $period);
    update_post_meta($subscription_id, '_billing_interval', floatval($duration));

    update_post_meta($subscription_id, '_d_subscription_period', $period);
    update_post_meta($subscription_id, '_d_subscription_interval', floatval($duration));
    update_post_meta($subscription_id, '_this_subscription_is_discounted', 1);
    update_post_meta($subscription_id, '_this_subscription_discounted_on', $start_date);
    update_post_meta($subscription_id, '_this_subscription_discount_expires_on', $bill_date);
    update_post_meta($subscription_id, '_this_subscription_order_item_id', $order_item_id);
    update_post_meta($subscription_id, '_this_subscription_order_id', $order_id);

//    // Update order total with new calculation after discount is applied
//    $original_line_total = wc_get_order_item_meta($order_item_id, $backup_prefix . '_line_total', true);
//    $order_total = get_post_meta($order_id, $backup_prefix . '_order_total', true);
//    if (floatval($original_line_total) != floatval($order_total)) {
//        $new_order_total = floatval($order_total)-floatval($original_line_total) + $amount;
//    }
//    else {
//        $new_order_total = $amount;
//    }
//    update_post_meta($order_id, '_order_total', $new_order_total);

    // Set Next payment date
    $subscription_key = wcs_get_old_subscription_key($subscription);
    ///$_SESSION['WCS1BDU_set_nextpaydate_done_'.$subscription_key] = $bill_date;
    ///$subscription->update_dates( array("next_payment" => $bill_date) );
    //$newtime = WC_Subscriptions_Manager::update_next_payment_date($bill_date, $subscription_details['subscription_key']);

    // Enable Discount availed flag to user
    $user_ID = get_current_user_id();
    // field: availed_discounted_subscription
    if (!SUBSCRIPTION_DISCOUNT_DEBUG) {
        update_field(SUBSCRIPTION_AVAILED_FIELD, true, "user_$user_ID");
    }
    //// update_user_meta ($user_ID, "availed_discounted_subscription", 1); //- This line is equivalent to above line (use any of the two)

    // Store Next payment date to updates array for later use
    $updates['next_payment_date'] = $subscription->schedule->next_payment;//$bill_date;
    $updates['next_payment_datestamp'] = strtotime($subscription->schedule->next_payment);//$bill_on;
    $updates['_this_subscription_discounted_on'] = $start_date;

    return $updates;

/*
line_subtotal: "25"
line_subtotal_tax: "0"
line_tax: "0"
line_tax_data: "a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}"
line_total: "25"
name: "Monthly Subscription (U.S.)"
qty: "1"
recurring_line_subtotal: "25"
recurring_line_subtotal_tax: "0"
recurring_line_tax: "0"
recurring_line_total: "25"
subscription_completed_payments: "a:1:{i:0;s:19:"2015-09-09 15:55:49";}"
subscription_end_date: "0"
subscription_expiry_date: "0"
subscription_failed_payments: "0"
subscription_interval: "1"
subscription_length: "0"
subscription_period: "month"
subscription_recurring_amount: "25"
subscription_sign_up_fee: "0"
subscription_start_date: "2015-09-09 15:55:49"
subscription_status: "active"
subscription_suspension_count: "1"
subscription_trial_expiry_date: "0"
subscription_trial_length: "0"
subscription_trial_period: "day"


  */


}

/*
 * Function to revert subscription discount and revert back original subscription details
 *
 */
function unset_subscription_discount($subscriptionv2, $subscription_id, $order, $order_item, $order_id, $product_id, $subscription_key) {
    //$order_item_id = $order_item['id'];
    $item = $order_item;
    $meta = $item['item_meta'];
    $backup_prefix = "_bk";

    // Meta list to revert back to oringal value
    $backup_meta_map = array(
        "_line_subtotal"=> "",
        "_line_total"=> "",
//        "_line_subtotal_tax"=> "",
//        "_line_tax"=> "",
        "_recurring_line_subtotal"=> "",
        "_recurring_line_subtotal_tax"=> "",
        "_recurring_line_tax"=> "",
        "_recurring_line_total"=> "",
        "_subscription_recurring_amount"=> "",
        "_subscription_interval"=> "",
        "_subscription_length"=> "",
        "_subscription_period"=> "",
////        "_subscription_completed_payments"=> "",
////        "_line_tax_data"=> "",
//        "_subscription_end_date"=> "",
//        "_subscription_expiry_date"=> "",
//        "_subscription_failed_payments"=> "",
//        "_subscription_sign_up_fee"=> "",
//        "_subscription_start_date"=> "",
//        "_subscription_status"=> "",
//        "_subscription_suspension_count"=> "",
//        "_subscription_trial_expiry_date"=> "",
//        "_subscription_trial_length"=> "",
//        "_subscription_trial_period"=> ""
    );

    // List of meta to be deleted
    $del_meta_map = array(
//      "_this_subscription_discounted_on" => true,
      "_line_subtotal" => true,
      "_line_total" => true
    );

    $order_item_id = get_post_meta($subscription_id, "_this_subscription_order_item_id", true);
    $order_total = get_post_meta($subscription_id, "{$backup_prefix}_order_total", true);
    update_post_meta($subscription_id, '_order_total', $order_total);
    $billing_period = get_post_meta($subscription_id, "{$backup_prefix}_billing_period", true);
    update_post_meta($subscription_id, '_billing_period', $billing_period);
    $billing_interval = get_post_meta($subscription_id, "{$backup_prefix}_billing_interval", true);
    update_post_meta($subscription_id, '_billing_interval', $billing_interval);
    delete_post_meta($subscription_id, $backup_prefix.'_order_total');
    delete_post_meta($subscription_id, $backup_prefix.'_billing_period');
    delete_post_meta($subscription_id, $backup_prefix.'_billing_interval');

    delete_post_meta($subscription_id, '_d_subscription_period');
    delete_post_meta($subscription_id, '_d_subscription_interval');
    update_post_meta($subscription_id, '_this_subscription_is_discounted', 0);
//    delete_post_meta($subscription_id, '_this_subscription_is_discounted');
//    delete_post_meta($subscription_id, '_this_subscription_discounted_on');
//    delete_post_meta($subscription_id, '_this_subscription_discount_expires_on');
//    delete_post_meta($subscription_id, '_this_subscription_order_item_id');
//    delete_post_meta($subscription_id, '_this_subscription_order_id');


    wc_delete_order_item_meta($order_item_id, $backup_prefix . '_d_subscription_period');
    wc_delete_order_item_meta($order_item_id, $backup_prefix . '_d_subscription_interval');
    $backup_found = wc_get_order_item_meta($order_item_id, "{$backup_prefix}_has_subcription_data_backup", true);
    if (!empty($backup_found)) {
        foreach ($del_meta_map as $key => $value) {
            $val = wc_get_order_item_meta($order_item_id, $backup_prefix . $key, true);
            wc_update_order_item_meta($order_item_id, $key, $val);
            wc_delete_order_item_meta($order_item_id, $backup_prefix . $key);
    }
        wc_delete_order_item_meta($order_item_id, $backup_prefix . '_has_subcription_data_backup');
    }
    else {
        return false;
    }

    // return next payment date
    $updates['next_payment_date'] = $subscription->schedule->next_payment;//$bill_date;
    $updates['next_payment_datestamp'] = strtotime($subscription->schedule->next_payment);//$bill_on;
    //
    return $updates;
}

function get_order_item_by_product_id( $order_id, $product_id = '' ) {

    $order = new WC_Order($order_id);

    foreach ( $order->get_items() as $key => $item ) {
        if ( ( WC_Subscriptions_Order::get_items_product_id( $item ) == $product_id || empty( $product_id ) ) && WC_Subscriptions_Order::is_item_subscription( $order, $item ) ) {
            return array("id" => $key, "item" => $item);
        }
    }

    return array();
}

/*
 * Function to retrieve all subscription order items
 *
 */
function get_all_subscription_order_items() {
    $ret = array();
    $users_subscription = WC_Subscriptions_Manager::get_all_users_subscriptions();

    foreach ($users_subscription as $userID => $subscriptions) {

        foreach($subscriptions as $key => $subscription) {
            $order_id = $subscription['order_id'];
            $product_id = $subscription['product_id'];
            $subscription_key = WC_Subscriptions_Manager::get_subscription_key($order_id, $product_id);
            $order_item = get_order_item_by_product_id($order_id, $product_id);
            $order_item_id = $order_item['id'];
            $item = $order_item['item'];
            $status = $subscription['status'];
            $is_discounted = isset($item['subscription_is_discounted']) && $item['subscription_is_discounted'] == "1";
            $arr = array(
                "user_id" => $userID,
                "subscription" => $subscription,
                "subscription_key" => $subscription_key,
                "order_item" => $item,
                "order_item_id" => $order_item_id,
                "order_id" => $order_id,
                "product_id" => $product_id,
                "status" => $status,
                "is_discounted" => $is_discounted,
            );
            $ret[$subscription_key] = $arr;
        }
    }

    return $ret;
}


function wc_scheduled_reset_discount_after_renewal($order_id) {
    //sendmymail ("Reset Discount initiated for order id {$order_id}", "Reset Discount initiated for order id {$order_id}");
    $order = new WC_Order($order_id);
    $order_item = get_order_item_by_product_id($order_id);
    $product_id = $order_item['product_id'];
    $user_ID = $order->user_id;
    $subscription_key = WC_Subscriptions_Manager::get_subscription_key($order_id, $product_id);
    wc_scheduled_reset_discount_after_payment($user_ID, $subscription_key);
}
/*
 * Function to reset discount when discount period is over
 */
function wc_scheduled_reset_discount_after_payment($subscriptionv2) {

    $subscription_key = wcs_get_old_subscription_key($subscriptionv2);
    $user_ID = $subscriptionv2->get_user_id();
    $subscription_id = $subscriptionv2->id;
    $isStaging = defined('SUBSCRIPTION_DISCOUNT_STAGE') ? SUBSCRIPTION_DISCOUNT_STAGE : false;
    $subscriptionResubscriableField = $isStaging ? 'field_56e4804dc2f39' : 'field_56e4861438733';
    $ends = $subscriptionv2->get_time( 'end' );

    $subscription_details = wcs_get_subscription_in_deprecated_structure($subscriptionv2);
    // If subscription does not exist return back with approiate error message
    if (empty($subscription_details)) {
        return false;
    }

    $subscription_details['subscription_key'] = $subscription_key;
    $subscription_details['subscription_id'] = $subscription_id;
    $product_id = $subscription_details['product_id'];
    $order_id = $subscription_details['order_id'];
    $order = new WC_Order($order_id);

	$items = $subscriptionv2->get_items();
    $item_ids = array_keys($items);
	$order_item  = array_pop( $items );
    $item_id = array_pop($item_ids);
	$order_item['id']  = $order_item_id = $item_id;
    $product_name = $order_item['name'];

    $availed_discount = get_post_meta($subscription_id, "_this_subscription_is_discounted", true);
    if ($availed_discount != "1") {
        return;
    }

    $item = $order_item['item'];

    $updates = unset_subscription_discount($subscriptionv2, $subscription_id, $order, $order_item, $order_id, $product_id, $subscription_key);

    $next_payment_date = date("Y-m-d", $updates['next_payment_datestamp']);

    // add order comment
    $order_note = sprintf( __( 'Discount offer for subscription %s has Expired.', 'woocommerce-subscriptions' ),
            $product_name);
    $subscriptionv2->add_order_note($order_note);

    $user = get_userdata($user_ID);
    $user_name = $user->display_name;
    $user_email = $user->user_email;
    $to = $admin_email = get_option('admin_email');
    $NL = "\n";
    $subject = "Subscription Discount Expired for subscription #$subscription_id";
    $message = sprintf(__('Discount on subscription "%s" for %s (%s) has Expired.' . $NL.$NL .
                'Details:'. $NL .
                'Subscription ID: %s'.$NL .
                'Order ID: %s'.$NL .
                'Subscription: %s'.$NL .
                'Discount Expired on: %s'.$NL .
                'Next payment on: %s'.$NL .
                'User Name: %s'.$NL .
                'User Email: %s'. $NL,
            'woocommerce-subscriptions'),
            $product_name, $user_name, $user_email,
            $subscription_id, $order_id, $product_name, date("Y-m-d H:i:s"),
            $next_payment_date,
            $user_name, $user_email
        );

    $header = "From:$admin_email\r\n";
    $retval = mail ($to,$subject,$message,$header);

}
//add_action( 'activated_subscription', 'wc_scheduled_reset_discount_after_payment', 12, 2);
add_action( 'woocommerce_subscription_status_active', 'wc_scheduled_reset_discount_after_payment', 12, 1);

function wc_force_cancel_after_pending_cancel($subscription) {
    $subscription->update_status('cancelled');
}
add_action( 'woocommerce_subscription_status_pending-cancel', 'wc_force_cancel_after_pending_cancel' , 20, 1);

// Test email sending function
function sendmymail($subject, $message) {
    $to = $admin_email = get_option('admin_email');
    $header = "From:$admin_email\r\n";
    $retval = mail ($to,$subject,$message,$header);
}


function get_subscription_renewal_date($date, $duration, $period) {
    $date_array = getdate(strtotime($date));
    $day    = $date_array['mday'];
    $month  = $date_array['mon'];
    $year   = $date_array['year'];

    if ($day < SUBSCRIPTION_RENEWS_ON) {
        $duration--;
        if ($duration < 0) {
            $duration = 0;
        }
    }

    $month += $duration;
    if ($month>12) {
        $month = $month % 12;
        $year++;
    }
    $day = SUBSCRIPTION_RENEWS_ON;

    $renewal_on = strtotime($year."-".$month."-".$day . " 6:00:00");

    return $renewal_on;
}


function customize_formatted_discounted_order_total($total, $order){
    $backup_prefix = "_bk";
    $has_subscription = WC_Subscriptions_Order::order_contains_subscription( $order );
    $is_discounted = WC_Subscriptions_Order::get_item_meta($order, "_this_subscription_is_discounted");
    $discounted_period = WC_Subscriptions_Order::get_item_meta($order, "_d_subscription_period");
    $discounted_interval = WC_Subscriptions_Order::get_item_meta($order, "_d_subscription_interval");
    $original_recurring_period = WC_Subscriptions_Order::get_item_meta($order, $backup_prefix . "_subscription_period");
    $original_recurring_interval = WC_Subscriptions_Order::get_item_meta($order, $backup_prefix . "_subscription_interval");
    $original_recurring = WC_Subscriptions_Order::get_meta($order, $backup_prefix . "_order_recurring_total");
    $discounted_amount = WC_Subscriptions_Order::get_meta($order, "_order_recurring_total");

    if (!empty($has_subscription) && !empty($is_discounted)) {
        $original_recurring = wc_price($original_recurring, array( 'currency' => $order->get_order_currency() ) );
        $discounted_amount = wc_price($discounted_amount, array( 'currency' => $order->get_order_currency() ) );
        $total = preg_replace('/THEN.+$/i', "then ", $total).
                " {$original_recurring} " .
                        ($original_recurring_interval == 1 ? "/" : " every $original_recurring_interval") .
                        " $original_recurring_period" .
                        ($original_recurring_interval == 1 ? "" : "s").
                " ($discounted_interval $discounted_period Discount Offer of {$discounted_amount})";
    }

    return $total;
}

function customize_formatted_discounted_recurring_total($total, $order){
    $backup_prefix = "_bk";
    $subscription_id = $order->id;
    $this_subscription_is_discounted = get_post_meta($subscription_id, '_this_subscription_is_discounted', true);
    $is_discounted = !empty($this_subscription_is_discounted);

    if ($is_discounted) {

        $original_recurring_interval = floatval(get_post_meta($subscription_id, $backup_prefix.'_billing_interval', true));
        $original_recurring_period = get_post_meta($subscription_id, $backup_prefix.'_billing_period', true);

        $d_subscription_interval = get_post_meta($subscription_id, '_d_subscription_interval', true);
        $d_subscription_period = get_post_meta($subscription_id, '_d_subscription_period', true);

        $original_amount =  get_post_meta($subscription_id, $backup_prefix.'_order_total', true);
        $discounted_amount =  get_post_meta($subscription_id, '_discounted_subscription_amount', true);
        $original_amount_formatted = wc_price($original_amount, array( 'currency' => $order->get_order_currency() ) );
        $discounted_amount_formatted = wc_price($discounted_amount, array( 'currency' => $order->get_order_currency() ) );
        $discounted_interval = $d_subscription_interval;
        $discounted_period = $d_subscription_period . ($discounted_interval > 1 ? 's': '');
        $offer = " ($discounted_interval $discounted_period Discount Offer of {$discounted_amount_formatted})";

        $originalTotal = " {$original_amount_formatted} " .
                        ($original_recurring_interval == 1 ? "/" : " every $original_recurring_interval") .
                        " $original_recurring_period" .
                        ($original_recurring_interval == 1 ? "" : "s");
        $total = $originalTotal . $offer;

    }

    return $total;
}
add_filter('woocommerce_get_formatted_subscription_total', 'customize_formatted_discounted_recurring_total',20,2);
//add_filter('woocommerce_get_formatted_order_total', 'customize_formatted_discounted_order_total',20,2);

add_action( 'wc_shipment_tracking_get_providers' , 'wc_shipment_tracking_add_custom_provider' );
/**
	* wc_shipment_tracking_add_custom_provider
 *
 * Adds custom provider to shipment tracking
 * Change the country name, the provider name, and the URL (it must include the %1$s)
 * Add one provider per line
*/
function wc_shipment_tracking_add_custom_provider( $providers ) {

	$providers['USA']['DHL (international)'] = 'http://webtrack.dhlglobalmail.com/?trackingnumber=%1$s';
	return $providers;

}

$args = array(
'name'          => __( 'shop_page_sidebar', 'theme_text_domain' ),
'id'            => 'shop_page_sidebar',
'description'   => 'shop_page_sidebar',
'class'         => 'shop_page_sidebar',
'before_widget' => '<li id="%1$s" class="widget %2$s">',
'after_widget'  => '</li>',
'before_title'  => '<h2 class="widgettitle">',
'after_title'   => '</h2>' );
register_sidebar( $args );


if ( ! function_exists( 'woocommerce_breadcrumb_gentleman' ) ) {

	/**
	 * Output the WooCommerce Breadcrumb
	 */
	function woocommerce_breadcrumb_gentleman( $args = array() ) {

		$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
			'delimiter'   => '&nbsp;&#47;&nbsp;',
			'wrap_before' => '<nav class="woocommerce-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
			'wrap_after'  => '</nav>',
			'before'      => '',
			'after'       => '',
			'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' )
		) ) );


		$breadcrumbs = new WC_Breadcrumb();

		if ( $args['home'] ) {
			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
		}

		$args['breadcrumb'] = $breadcrumbs->generate();



		wc_get_template( 'global/breadcrumb-new.php', $args );
	}
}


// ENABLE ACF OPTIONS PAGE
if( function_exists('acf_add_options_page') ) {
    //acf_add_options_page();
    $parent = acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'redirect' => false
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'Cart Settings',
        'menu_title' => 'Cart',
        'parent_slug' => $parent['menu_slug'],
    ));
}
// PRIMARY BUTTON SHORTCODE
  function primary_button($atts, $content = null) {
    extract( shortcode_atts( array(
      'target' => '',
      'url' => '#',
      'arrow' => ''
    ), $atts ) );
    return '<a target="'.$target.'" href="'.$url.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
  }
  add_shortcode('primary-btn', 'primary_button');
// SECONDARY BUTTON SHORTCODE
  function secondary_button($atts, $content = null) {
    extract( shortcode_atts( array(
      'target' => '',
      'url' => '#',
      'arrow' => ''
    ), $atts ) );
    return '<a target="'.$target.'" href="'.$url.'" class="secondary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
  }
  add_shortcode('secondary-btn', 'secondary_button');



//
// SOCIAL SHARING FOR CONTENT
//
	function crunchify_social_sharing_buttons($content) {
		global $post;
		if( is_single() ){

			// Get current page URL
			$crunchifyURL = urlencode(get_permalink());
			// Get current page title
			$crunchifyTitle = str_replace( ' ', '%20', get_the_title());
			// Get Post Thumbnail for pinterest
			$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			// Construct sharing URL without using any script
			$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;via=Crunchify';
			$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
			// Based on popular demand added Pinterest too
			$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;
			// Add sharing button at the end of page/page content
			$content .= '<!-- Crunchify.com social sharing. Get your copy here: http://crunchify.me/1VIxAsz -->';
			$content .= '<div class="crunchify-social">';
			$content .= '<h3 class="share-post-header">SHARE ON</h3> <a class="crunchify-link crunchify-twitter" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
			$content .= '<a class="crunchify-link crunchify-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
			$content .= '<a class="crunchify-link crunchify-pinterest" href="'.$pinterestURL.'" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>';
			$content .= '</div>';
			return $content;
		} else {
			// if not a post/page then don't include sharing button
			return $content;
		}
	};
	add_filter( 'the_content', 'crunchify_social_sharing_buttons');

	// SHIPTSTATION CUSTOM FIELD 2 AND 3 FOR WISMO LABS INTEGRATION
add_filter( 'woocommerce_shipstation_export_custom_field_2', 'shipstation_custom_field_2' );

function shipstation_custom_field_2() {
	return '_shipping_postcode'; // DESTINATION ZIP CODE
}
add_filter( 'woocommerce_shipstation_export_custom_field_3', 'shipstation_custom_field_3' );

function shipstation_custom_field_3() {
	return 'Select Your First Box'; // SELECT YOUR FIRST BOX
}

add_filter('autoptimize_filter_noptimize','my_ao_noptimize',10,0);
function my_ao_noptimize() {
	if (strpos($_SERVER['REQUEST_URI'],'checkout')!==false || strpos($_SERVER['REQUEST_URI'],'product')!==false || strpos($_SERVER['REQUEST_URI'],'my-account')!==false || strpos($_SERVER['REQUEST_URI'],'cart')!==false || strpos($_SERVER['REQUEST_URI'],'shop')!==false) {
		return true;
	} else {
		return false;
	}
}

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//
// Register new footer menus
//
register_nav_menus( array('footer1'=>__('Footer menu 1'), 'footer2'=>__('Footer menu 2'), 'footer3'=>__('Footer menu 3'), 'footer4'=>__('Footer menu 4'), 'footer5'=>__('Footer menu 5') ));

//
// Modifing checkout fields
//
add_filter( 'woocommerce_checkout_fields' , 'toptal_custom_override_checkout_fields' );
function toptal_custom_override_checkout_fields( $fields ) {
    $fields['shipping']['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping']['shipping_first_name']['label'] = 'First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping']['shipping_last_name']['label'] = 'Last Name';
    $fields['shipping']['shipping_country']['placeholder'] = 'Country';
    $fields['shipping']['shipping_country']['label'] = 'Country';
    $fields['shipping']['shipping_address_1']['placeholder'] = 'Address';
    $fields['shipping']['shipping_address_1']['label'] = 'Address';
    $fields['shipping']['shipping_address_2']['placeholder'] = 'Apt/Floor/Suite';
    $fields['shipping']['shipping_address_2']['label'] = 'Apt/Floor/Suite';
    $fields['shipping']['shipping_city']['placeholder'] = 'Town/City';
    $fields['shipping']['shipping_city']['label'] = 'Town/City';
    $fields['shipping']['shipping_state']['placeholder'] = 'State/County';
    $fields['shipping']['shipping_state']['label'] = 'State/County';
    $fields['shipping']['shipping_postcode']['placeholder'] = 'Zip Code';
    $fields['shipping']['shipping_postcode']['label'] = 'Postcode/ZIP';
    $fields['shipping']['shipping_email'] = array(
        'label' => 'Email',
        'placeholder' => _x('Email Address', 'placeholder', 'woocommerce'),
        'required' => true,
        'class' => array('form-row-wide'),
        'clear' => true,
        'type' => 'email'
    );
    $fields['shipping']['shipping_phone'] = array(
        'label' => 'Telephone',
        'placeholder' => _x('Telephone', 'placeholder', 'woocommerce'),
        'required' => true,
        'class' => array('form-row-wide'),
        'clear' => true,
        'type' => 'tel'
    );
    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_first_name']['label'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_last_name']['label'] = 'Last Name';
    $fields['billing']['billing_country']['placeholder'] = 'Country';
    $fields['billing']['billing_country']['label'] = 'Country';
    $fields['billing']['billing_address_1']['placeholder'] = 'Address';
    $fields['billing']['billing_address_1']['label'] = 'Address';
    $fields['billing']['billing_address_2']['placeholder'] = 'Apt/Floor/Suite';
    $fields['billing']['billing_address_2']['label'] = 'Apt/Floor/Suite';
    $fields['billing']['billing_city']['placeholder'] = 'City';
    $fields['billing']['billing_city']['label'] = 'Town/City';
    $fields['billing']['billing_state']['placeholder'] = 'State';
    $fields['billing']['billing_state']['label'] = 'State/County';
    $fields['billing']['billing_postcode']['placeholder'] = 'Zip Code';
    $fields['billing']['billing_postcode']['label'] = 'Postcode/ZIP';
    $fields['billing']['billing_email']['placeholder'] = 'Email Address';
    $fields['billing']['billing_email']['label'] = 'email';
    $fields['billing']['billing_phone']['placeholder'] = 'Telephone';
    $fields['billing']['billing_phone']['label'] = 'Telephone';
    return $fields;
}
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'toptal_checkout_field_display_admin_order_meta', 10, 1 );
function toptal_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Phone From Checkout Form').':</strong> ' . get_post_meta( $order->id, '_shipping_phone', true ) . '</p>';
    echo '<p><strong>'.__('Email From Checkout Form').':</strong> ' . get_post_meta( $order->id, '_shipping_email', true ) . '</p>';
}

//
// Adding browser class to body
//
function mv_browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) {
        $classes[] = 'ie';
        if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
        $classes[] = 'ie'.$browser_version[1];
    } else $classes[] = 'unknown';
    if($is_iphone) $classes[] = 'iphone';
    if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
        $classes[] = 'osx';
    } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
        $classes[] = 'linux';
    } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
        $classes[] = 'windows';
    }
    return $classes;
}
add_filter('body_class','mv_browser_body_class');

//
// Removing special subscriptions from cart
//
add_action( 'woocommerce_add_to_cart', 'toptal_action_woocommerce_add_to_cart', 10, 2 );
function toptal_action_woocommerce_add_to_cart( $cart_item_key, $product_id ) {
    global $woocommerce;
    if ( function_exists('get_field') ) {
        if ( get_field('cnbicwn_subscriptions', $product_id) ) {
            $cnbicwn_subscriptions = get_field('cnbicwn_subscriptions', $product_id);
            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
                foreach ( $cnbicwn_subscriptions as $id ) {
                    if ( $cart_item['product_id'] == $id ) {
                        $woocommerce->cart->remove_cart_item($cart_item_key);
                    }
                }
            }

        }
    }
}
function friendbuy_endpoint() {
	header("Access-Control-Allow-Origin: *");
	
	global $wp_query;

	$reward = json_decode( file_get_contents( 'php://input', true ) );
	$user   = get_user_by( 'email', $reward->conversion->referrer->customer->email );

	if( ! empty( $user ) ) {
		$account_credit = intVal( get_user_meta( $user->ID, '_account_credit', true ) );

		$new_account_credit = $account_credit + $reward->amount;

		update_user_meta( $user->id, '_account_credit', $new_account_credit, $account_credit );

		status_header( 200 );
		wp_send_json([ 
			'success' => true, 
			'status' => 'Credit Applied',
			'data' => [
				'user_id'            => $user->ID,
				'reward_amount'      => $reward->amount,
				'old_account_credit' => $account_credit,
				'new_account_credit' => $new_account_credit
			]
		]);
	}

	status_header( 404 );
	wp_send_json([ 
		'success' => false, 
		'status' => 'Unable to process the request.' 
	]);
}

add_action( 'rest_api_init', function() {
    register_rest_route( 'v1', '/friendbuy/', [
    	'show_in_rest' => true,
        'methods'      => 'POST',
        'callback'     => 'friendbuy_endpoint'
   	]);
});

//
// CUSTOMIZE THE WOOCOMMERCE "MY ACCOUNT" MENU
//
function add_my_menu_items( $items ) {
	$my_items = array(
		//  endpoint   => label
		'/shop' => __( 'Shop' ),
	);
	$my_items = array_slice( $items, 0, 1, true ) + $my_items + array_slice( $items, 1, count( $items ), true );
	return $my_items;
}
add_filter( 'woocommerce_account_menu_items', 'add_my_menu_items', 99, 1 );

/**
 * Add custom tracking code to the thank-you page
 */
add_action( 'woocommerce_thankyou', 'my_custom_tracking' );

function my_custom_tracking( $order_id ) {
	// Lets grab the order
	$order = wc_get_order( $order_id );
	// Facebook Subscription Pixel
	?>
		<script>fbq('track','Subscribe', {value: <?php echo $order->get_total(); ?>, currency: 'USD'});</script>
	<?php 	 
}