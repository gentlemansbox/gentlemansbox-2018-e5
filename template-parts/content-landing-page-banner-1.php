<?php
$banner_1 = get_field('banner_1');
if($banner_1){ ?>
  <section class="give-try dark-bg awesome-deals">
    <h2><?php the_field( 'banner_1'); ?></h2>
  </section>
<?php } ?>
