<?php

// check if the repeater field has rows of data
if( have_rows('question_and_answer') ): ?>
<?php if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>

<div class="wrap" style="margin-bottom:50px;">
<?php 	// loop through the rows of data
    while ( have_rows('question_and_answer') ) : the_row(); ?>
    <div class="faq-block">
      <button class="accordion"><?php the_sub_field('question'); ?></button>
        <div class="panel" style="display:none;">
            <p><?php the_sub_field('answer'); ?></p>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
<?php else :

    // no rows found

endif;

?>
