<?php
    $subscription_1_image = get_field( 'subscription_1_image' );
    $subscription_2_image = get_field( 'subscription_2_image' );
    $subscription_3_image = get_field( 'subscription_3_image' );
    $subscription_4_image = get_field( 'subscription_4_image' );
    $subscription_1_title = get_field('subscription_1_title');
    $subscription_2_title = get_field('subscription_2_title');
    $subscription_3_title = get_field('subscription_3_title');
    $subscription_4_title = get_field('subscription_4_title');
    $subscription_1_subtitle = get_field('subscription_1_subtitle');
    $subscription_2_subtitle = get_field('subscription_2_subtitle');
    $subscription_3_subtitle = get_field('subscription_3_subtitle');
    $subscription_4_subtitle = get_field('subscription_4_subtitle');
    $subscription_1 = get_field('subscription_1');
    $subscription_2 = get_field('subscription_2');
    $subscription_3 = get_field('subscription_3');
    $subscription_4 = get_field('subscription_4');
?>
<section class="work-purpose">
  <?php if($subscription_1_image || $subscription_1_title || $subscription_1_subtitle || $subscription_1) { ?>
    <div class="work">
      <?php if($subscription_1_image){ ?>
        <div class="one-half-full" style="background-image:url(<?php the_field('subscription_1_image'); ?>);"></div>
      <?php } if($subscription_1_title || $subscription_1_subtitle || $subscription_1) { ?>
        <div class="one-half-full vertical-align-parent">
          <div class="vertical-align-content">
            <?php if($subscription_1_title) { ?>
              <h2><?php the_field('subscription_1_title'); ?></h2>
            <?php } if($subscription_1_subtitle) { ?>
              <h3><?php the_field('subscription_1_subtitle'); ?></h3>
            <?php } if($subscription_1) { ?>
              <?php the_field('subscription_1'); ?>
            <?php }?>
          </div>
        </div>
      <?php } ?>
    </div>
  <? } if($subscription_2_image || $subscription_2_title || $subscription_2_subtitle || $subsription_2) { ?>
    <div class="purpose">
      <?php if($subscription_2_title || $subscription_2_subtitle || $subscription_2) { ?>
        <div class="one-half-full vertical-align-parent">
          <div class="vertical-align-content">
            <?php if($subscription_2_title) { ?>
              <h2><?php the_field('subscription_2_title'); ?></h2>
            <?php } if($subscription_2_subtitle) { ?>
              <h3><?php the_field('subscription_2_subtitle'); ?></h3>
            <?php } if($subscription_2) { ?>
              <?php the_field('subscription_2'); ?>
            <?php }?>
          </div>
        </div>
      <?php } if($subscription_2_image){ ?>
        <div class="one-half-full" style="background-image:url(<?php the_field('subscription_2_image'); ?>);"></div>
      <?php } ?>
    </div>
<? } if($subscription_3_image || $subscription_3_title || $subscription_3_subtitle || $subsription_3){ ?>
  <div class="work">
    <?php if($subscription_3_image){ ?>
      <div class="one-half-full" style="background-image:url(<?php the_field('subscription_3_image'); ?>);"></div>
    <?php } if($subscription_3_title || $subscription_3_subtitle || $subcsription_3) { ?>
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <?php if($subscription_3_title) { ?>
            <h2><?php the_field('subscription_3_title'); ?></h2>
          <?php } if($subscription_3_subtitle) { ?>
            <h3><?php the_field('subscription_3_subtitle'); ?></h3>
          <?php } if($subscription_3) { ?>
            <?php the_field('subscription_3'); ?>
          <?php }?>
        </div>
      </div>
    <?php } ?>
  </div>
<? } if($subscription_4_image || $subscription_4_title || $subscription_4_subtitle || $subscription_4) { ?>
  <div class="purpose">
    <?php if($subscription_4_title || $subscription_4_subtitle || $subscription_4) { ?>
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <?php if($subscription_4_title) { ?>
            <h2><?php the_field('subscription_4_title'); ?></h2>
          <?php } if($subscription_4_subtitle) { ?>
            <h3><?php the_field('subscription_4_subtitle'); ?></h3>
          <?php } if($subscription_4) { ?>
            <?php the_field('subscription_4'); ?>
          <?php }?>
        </div>
      </div>
    <?php } if($subscription_4_image){ ?>
      <div class="one-half-full" style="background-image:url(<?php the_field('subscription_4_image'); ?>);"></div>
    <?php } ?>
  </div>
<?php } ?>

</section>
