<?php
$banner_2 = get_field('banner_2');
if($banner_2){ ?>
  <section class="give-try dark-bg">
    <h2><?php the_field( 'banner_2' );?></h2>
  </section>
<?php } ?>
