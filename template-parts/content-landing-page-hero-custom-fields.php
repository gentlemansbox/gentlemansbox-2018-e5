<?php
    $bgcolor = get_field( 'background_color_b1' );
    if ( !$bgcolor) {
        $bgcolor = '#fff';
    }
    $banner_image = get_field('image_b1');
    $banner_subtitle = get_field('banner_subtitle_b1');
    $banner_title = get_field('banner_title_b1');
    $banner_text = get_field('banner_text_b1');
    $button_text = get_field('button_text_b1');
    $button_link = get_field('button_link_b1');
    $bottom_logo = get_field('bottom_logo_b2');
    $bottom_text = get_field('bottom_text_b2');
?>
<div id="topbanner" style="background-color:<?php echo $bgcolor; ?>;">
    <div class="wrp">
        <div class="banner_cont">
          <?php if($banner_image){ ?>
            <img class="banner_text__img banner_text__img__m" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
          <?php }
          if($banner_subtitle || $banner_title || $banner_text || $button_link || $button_text){ ?>
            <div class="banner_text">
              <?php if($banner_subtitle) { ?>
                <h3 class="banner_text__subtitle"><span style="background-color:<?php echo $bgcolor; ?>;"><?php the_field( 'banner_subtitle_b1' ); ?></span></h3>
              <?php } if($banner_title) { ?>
                <h1 class="banner_text__title"><?php the_field( 'banner_title_b1' ); ?></h1>
              <?php } if($banner_text) { ?>
                <p class="banner_text__text"><?php the_field( 'banner_text_b1' ); ?></p>
                <?php }
              if($button_text && $button_link) { ?>
                <p class="banner_text__button"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn"><?php the_field( 'button_text_b1' ); ?></a></p>
                <?php } ?>
            </div>
            <?php if($banner_image){ ?>
            <img class="banner_text__img banner_text__img__d" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
          <?php }
        } ?>

        </div>
    </div>
</div>
<?php if($bottom_logo || $bottom_text) { ?>
  <div class="nprowsb">
      <div class="wrp">
          <div class="nprowsb__cont">
            <?php if($bottom_logo) { ?>
              <img class="nprowsb__cont__img" src="<?php the_field('bottom_logo_b2'); ?>" alt="" title="" />
              <?php } ?>
              <?php if($bottom_text) { ?>
              <?php the_field('bottom_text_b2'); ?>
              <?php } ?>
          </div>
      </div>
  </div>
<?php } ?>
