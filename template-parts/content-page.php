<?php if ( $post->post_content=="" ) { ?>
  <?php // NOTHING ?>
<?php } else { ?>
  <section class="full-width">
    <div class="max-width">
     <?php the_content(); ?>
    </div>
  </section>
<?php } ?>
