<?php 
/*----------------------------------------------------------------*\
	HERO WITH IMAGE BACKGROUND
\*----------------------------------------------------------------*/
?>
<section class="murder-mystery-hero" style="background-image: linear-gradient(rgba(0,0,0,0.31),rgba(0,0,0,0.31)), url('<?php the_field('background_image'); ?>');">
	<div>
		<h1>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h1>
		<?php if ( get_field('title_description') ) : ?>
			<h2>
			<?php the_field('title_description'); ?>
			</h2>
		<?php endif; ?>
		<a href="#answer-form" class="smoothScroll">
			<svg viewBox="0 0 82 26">
				<path d="M41 26L82 0H0z" fill="#FFF" fill-rule="nonzero"/>
			</svg>
		</a>
	</div>
</section>
<script>
var $ = jQuery;

$(document).ready(function () {
	$('a[href*=\\#]').bind('click', function (e) {
		e.preventDefault(); // prevent hard jump, the default behavior

		var target = $(this).attr("href"); // Set the target as variable

		// perform animated scrolling by getting top-position of target-element and set it as scroll target
		$('html, body').stop().animate({
			scrollTop: $(target).offset().top - 100
		}, 600, function () {});

		return false;
	});
});
</script>
<div id="answer-form"></div>