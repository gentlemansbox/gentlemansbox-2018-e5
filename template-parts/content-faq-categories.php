<?php

// check if the repeater field has rows of data
if( have_rows('categories') ): ?>
<div class="wrap faq-categories">
<?php 	// loop through the rows of data
    while ( have_rows('categories') ) : the_row(); ?>
      <div class="faq-category-block">
        <a href="<?php the_sub_field('category_link'); ?>" class="faq-category"><?php the_sub_field('category_title'); ?></a>
      </div>
    <?php endwhile; ?>
  </div>
<?php else :

    // no rows found

endif;

?>
