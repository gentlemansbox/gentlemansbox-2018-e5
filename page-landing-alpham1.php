 <?php
/*
* Template Name: AlphaM1 Landing
*/

get_header(); ?>

<?php
    $bgcolor = get_field( 'background_color_b1' );
    if ( !$bgcolor) {
        $bgcolor = '#fff';
    }
?>
<div id="topbanner" style="background-color:<?php echo $bgcolor; ?>;">
    <div class="wrp">
      <?php if (is_page(556318)) { ?>
        <div class="banner_cont" style="padding: 0 0 0 50%;">
        <?php } else { ?>
          <div class="banner_cont">
          <?php } ?>
            <img class="banner_text__img banner_text__img__m" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" />
            <?php if (is_page(556318)) { ?> <!--AlphaM Project Landing Page -->
              <img style="left:-20%;" class="banner_text__img banner_text__img__d" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" style="top:-85px;"/>
              <div class="banner_text">
                  <h3 class="banner_text__subtitle"><span style="background-color:<?php echo $bgcolor; ?>;"><?php the_field( 'banner_subtitle_b1' ); ?></span></h3>
                  <h1 class="banner_text__title"><?php the_field( 'banner_title_b1' ); ?></h1>
                  <p class="banner_text__text"><?php the_field( 'banner_text_b1' ); ?></p>
                  <p class="banner_text__button"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn"><?php the_field( 'button_text_b1' ); ?></a></p>
                  <p class="banner_text__text" style="top: 50px;position: relative;">SPONSOR OF THE ALPHA M PROJECT SEASON 4</p>
              </div>
            <?php } else { ?>
              <div class="banner_text">
                  <h3 class="banner_text__subtitle"><span style="background-color:<?php echo $bgcolor; ?>;"><?php the_field( 'banner_subtitle_b1' ); ?></span></h3>
                  <h1 class="banner_text__title"><?php the_field( 'banner_title_b1' ); ?></h1>
                  <p class="banner_text__text"><?php the_field( 'banner_text_b1' ); ?></p>
                  <p class="banner_text__button"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn"><?php the_field( 'button_text_b1' ); ?></a></p>
              </div>
              <img class="banner_text__img banner_text__img__d" src="<?php the_field( 'image_b1' ); ?>" alt="<?php the_field( 'banner_title_b1' ); ?>" title="<?php the_field( 'banner_title_b1' ); ?>" style="top:-85px;"/>
            <?php } ?>
        </div>
    </div>
</div>

<div class="nprowsb">
    <div class="wrp">
        <div class="nprowsb__cont">
            <img class="nprowsb__cont__img" src="<?php the_field('bottom_logo_b2'); ?>" alt="" title="" />
            <?php the_field('bottom_text_b2'); ?>
        </div>
    </div>
</div>

<?php if ( have_rows('blocks_b2') ): ?>
<div class="nprows nprows1">
    <div class="wrp">
        <h2 class="nprows__title"><?php the_field('title_b2'); ?></h2>
        <h4 class="nprows__subtitle"><?php the_field('subtitle_b2'); ?></h4>
        <div class="nprows__row">
        <?php while ( have_rows('blocks_b2') ): the_row(); ?>
            <div class="nprows__el">
                <div class="nblock">
                    <img class="block__img" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>" />
                    <h3 class="block__title"><?php the_sub_field('title'); ?></h3>
                    <p class="block__subtitle"><?php the_sub_field('subtitle'); ?></p>
                    <a class="block__link" href="<?php the_sub_field('link'); ?>"></a>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="nprowsb">
    <div class="wrp">
        <div class="nprowsb__cont">
            <?php if (is_page(556318)) { ?> <!--AlphaM Project Landing Page -->
              <p style="display:inline-block;">USE PROMO CODE “ALPHAMPROJECT” FOR $10 OFF YOUR FIRST BOX.</p>
              <p class="banner_text__button" style="display:inline-block;"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn" style="text-decoration: none;background-color: #268388;padding: 10px 15px;margin-left: 10px;">ORDER NOW</a></p>
            <?php } else { ?>
              <p>USE PROMO CODE “ALPHA10” FOR $10 OFF YOUR FIRST BOX.</p>
            <?php } ?>
        </div>
    </div>
</div>

<div class="wrap" style="text-align: center;">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content(); ?>
        </div>
        <p class="banner_text__button"><a href="<?php the_field( 'button_link_b1' ); ?>" class="nbtn" style="display: inline-block;
    font-size: 15px;
    color: #fff;
    font-weight: 700;
    text-transform: uppercase;
    padding: 15px 35px;
    background: #313131;
    -webkit-transition: opacity .5s;
    transition: opacity .5s;"><?php the_field( 'button_text_b1' ); ?></a></p>
    </article>
    <?php endwhile; endif; ?>
</div>
</div>

<style type="text/css">
    @media(max-width: 600px){
        .wrap iframe {
            max-width: 300px;
        }
    }
</style>

<?php get_footer('new'); ?>
