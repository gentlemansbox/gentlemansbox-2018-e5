<?php
/**
 * Template Name: Switch Subscriptions page
*/
get_header();

$order = 0;
$order = new WC_Subscription($_GET[sub]);
$order_id = $order->id;
if($order_id !== 0){
  $order->update_status('cancelled');
}
?>
<div class="subscrpage">

    <div class="wrap">

      <div class="nptitle">
          <h2 class="nptitle__title">Select Your Subscription</h2>
      </div>
      <!-- <div class="ovwshipping">
          <p class="shipping__text">Ship To: </p>
          <select id="chose_shipping_type" name="shipping_type">
              <option value="us">U.S.</option>
              <option value="international">International</option>
          </select>
      </div> -->
      <div class="subscont subscont__ship">
        <div class="subs vis">
          <a href="/cart/?add-to-cart=509667">
            <div class="subs__img">
              <img width="316" height="233" src="https://gentlemansbox.com/wp-content/uploads/SockoftheMonth.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/SockoftheMonth.png 316w, https://gentlemansbox.com/wp-content/uploads/SockoftheMonth-300x221.png 300w" sizes="(max-width: 316px) 100vw, 316px">
            </div>
            <h3 class="subs__title">Sock of the Month (U.S.)</h3>
            <p style="color:#000;">$12.00/month</p>
            <label></label>
          </a>
        </div>
        <div class="subs vis">
          <a href="/cart/?add-to-cart=509671">
            <div class="subs__img">
              <img width="316" height="233" src="https://gentlemansbox.com/wp-content/uploads/TieoftheMonth.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/TieoftheMonth.png 316w, https://gentlemansbox.com/wp-content/uploads/TieoftheMonth-300x221.png 300w" sizes="(max-width: 316px) 100vw, 316px">
            </div>
            <h3 class="subs__title">Tie of the Month (U.S.)</h3>
            <p style="color:#000;">$15.00/month</p>
            <label></label>
          </a>
        </div>
        <div class="subs vis">
          <a href="/cart/?add-to-cart=535561">
            <div class="subs__img">
              <img width="316" height="233" src="https://gentlemansbox.com/wp-content/uploads/SockoftheMonth.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/SockoftheMonth.png 316w, https://gentlemansbox.com/wp-content/uploads/SockoftheMonth-300x221.png 300w" sizes="(max-width: 316px) 100vw, 316px">
            </div>
            <h3 class="subs__title">Sock of the Month (INT)</h3>
            <p style="color:#000;">$16.00/month</p>
            <label></label>
          </a>
        </div>
        <div class="subs vis">
          <a href="/cart/?add-to-cart=535560">
            <div class="subs__img">
              <img width="316" height="233" src="https://gentlemansbox.com/wp-content/uploads/TieoftheMonth.png" class="attachment-318x236 size-318x236 wp-post-image" alt="" srcset="https://gentlemansbox.com/wp-content/uploads/TieoftheMonth.png 316w, https://gentlemansbox.com/wp-content/uploads/TieoftheMonth-300x221.png 300w" sizes="(max-width: 316px) 100vw, 316px">
            </div>
            <h3 class="subs__title">Tie of the Month (INT)</h3>
            <p style="color:#000;">$19.00/month</p>
            <label></label>
          </a>
        </div>
      </div>
    </div>

</div>

<?php get_footer('new'); ?>
