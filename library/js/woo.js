
//Limit Billing/Shipping Address Field to 30 Characters
$(document).ready(function() {
   $('input[type="text"]#billing_address_1,input[type="text"]#shipping_address_1,input[type="text"]#billing_address_2,input[type="text"]#shipping_address_2').attr({ maxLength : 30 });
   $('<em class="max">(Street address is limited to 30 characters maximium)</em>').insertBefore('input[type="text"]#billing_address_1,input[type="text"]#shipping_address_1');
});

//Disable Special Characters on Input Fields
// $('input').bind('keypress', function (event) {
//     var regex = new RegExp("^[a-zA-Z0-9]+$");
//     var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
//     if (!regex.test(key)) {
//        event.preventDefault();
//        return false;
//     }
// });


$('.woocommerce input#billing_first_name, .woocommerce input#billing_last_name, .woocommerce input#billing_address_1, .woocommerce input#billing_address_2, .woocommerce input#billing_city, .woocommerce input#shipping_first_name, .woocommerce input#shipping_last_name, .woocommerce input#shipping_address_1, .woocommerce input#shipping_address_2, .woocommerce input#shipping_city').alphanum();


//Change Order Notes to Gift Message
$('p.notes label:contains("Order Notes")').each(function(){
    $(this).html($(this).html().split("Order Notes").join("Gift Message"));
});

//Change Gift Message (Order Comments) Placeholder Text
$("textarea#order_comments").attr("placeholder", "Sending this as a gift? Write your gift message here.");

//300 Character Limit Max on Gift Message
$('textarea#order_comments').attr({ maxLength : 300 });
$('<em class="max">(Gift Message is limited to 300 characters maximium)</em>').insertBefore('textarea#order_comments');


//Uncheck Shipping to a Different Address Checkbox
$("#ship-to-different-address-checkbox").prop("checked", false);


//Remove Labels from Header Cart
$('.woocommerce-cart .cart-collaterals .cart_totals tr td:contains("$101 - $200:")').each(function(){
    $(this).html($(this).html().split("$101 - $200:").join(""));
});


//Remove Labels from Header Cart
$('.cart-contents:contains("/ year")').each(function(){
    $(this).html($(this).html().split("/ year").join(""));
});

$('.cart-contents:contains("/ month")').each(function(){
    $(this).html($(this).html().split("/ month").join(""));
});

//Remove "Now Then" from Gifts in Header Cart
$('.cart-contents:contains("now then")').each(function(){
    $(this).html($(this).html().split("now then").join(""));
});

//Remove "$0.00" from Gifts in Header Cart
$('.cart-contents:contains("$0.00")').each(function(){
    $(this).html($(this).html().split("$0.00").join(""));
});

//Remove / month from Monthly Plan
$('aside.monthly .subscription-details:contains("/ month")').each(function(){
    $(this).html($(this).html().split("/ month").join(""));
});


//Fixes for Gift Pricing (Signup Fee)

//Remove "Free!" From Gift Pricing
$('aside:contains("Free!")').each(function(){
    $(this).html($(this).html().split("Free!").join(""));
});

//Remove "Sign Up Fee" From Gift Pricing
$('aside:contains("sign-up fee")').each(function(){
    $(this).html($(this).html().split("sign-up fee").join(""));
});

//Remove 3 Month Extra Wording for Gift Pricing
$('aside:contains(" / month for 3 months and a ")').each(function(){
    $(this).html($(this).html().split(" / month for 3 months and a ").join(""));
});

//Remove 6 Month Extra Wording for Gift Pricing
$('aside:contains(" / month for 6 months and a ")').each(function(){
    $(this).html($(this).html().split(" / month for 6 months and a ").join(""));
});

//Remove 12 Month Extra Wording for Gift Pricing
$('aside:contains(" / month for 12 months and a ")').each(function(){
    $(this).html($(this).html().split(" / month for 12 months and a ").join(""));
});

//Remove 12 Month Extra Wording for Gift Pricing
$('aside:contains(" / month and a ")').each(function(){
    $(this).html($(this).html().split(" / month and a ").join(""));
});


//Label for under Country
// $("<p class='lower'>At the present time we ONLY ship Gentleman's Box to the lower 48 United States and Canada. We promise to offer more shipping options soon!</p>").insertAfter("#billing_country_field");

//Add clear after Town/City in Checkout
$('<div class="clear" />').insertAfter('#billing_city_field');

//Label underneath Shipping to a Different Address
$("<span class='notice'>Check this box to edit the ship to address.</span>").insertAfter('input#ship-to-different-address-checkbox');

//Label underneath Shipping to a Different Address
$("<span class='restrict'>Please Note: If you're purchasing a U.S. product you cannot ship to a Canadian Address. You must purchase a Canadian Box.</span>").insertAfter('.shipping_address #shipping_country_field');

//Change Label on 'Ship to Different Address'
$("h3#ship-to-different-address label").replaceWith("<label for='ship-to-different-address-checkbox' class='checkbox'>Is this item a Gift? Are you shipping to a different address?</label>");

//Hide Cart Count if Empty
var text = $('header[role="banner"] #account span.cart-contents span.count').text();
var NothinginCart = '0'

if(text == NothinginCart){
      $('header[role="banner"] #account span.cart-contents span.count').css('display','none');
};


//Remove "Free!" From Gift Options Buttons
// ​$("section#buttons aside").text(function () {
//     return $('section#buttons aside').text().replace("Free!", "Awesome!"); 
// });​​​​​

// $("section#buttons aside").text(function() {
//     return $(this).text().replace('Free!', '');
// });


jQuery(document).ready(function($) {
    $('body').on('added_to_cart',function(e,data) {
        //alert('Added ' + data['div.widget_shopping_cart_content']);
        if ($('#hidden_cart').length == 0) { //add cart contents only once
            //$('.added_to_cart').after('<a href="#TB_inline?width=600&height=550&inlineId=hidden_cart" class="thickbox">View my inline content!</a>');
            $(this).append('<a href="#TB_inline?width=300&height=550&inlineId=hidden_cart" id="show_hidden_cart" title="<h2>Cart</h2>" class="thickbox" style="display:none"></a>');
            $(this).append('<div id="hidden_cart" style="display:none">'+data['div.widget_shopping_cart_content']+'</div>');
        }
        $('#show_hidden_cart').click();
    });
});


//Remove '/shop' String from href to on Shop Page to Make Add to Cart Redirect Work Correctly
jQuery('body.archive ul.products li a.button').attr('href', function(_, attr) {
   return attr.replace('/shop', '');
});

