//Add Class based on Touch or NonTouch Device
if ("ontouchstart" in document.documentElement) {
  $('body').addClass('touch');
}
else {
  $('body').addClass('no-touch');
}  


$(".shipto").click(function() {
  $(".shipto").toggleClass('clicked');
  $("#countrylist").slideToggle();
});

$('.shipto, .shippingto a, .changecountry').click(function(e) {
    e.preventDefault();
});


  $(document).ready(function(){
      $(".changecountry").click(function() {
        $("#countrylist").slideToggle();
      });
    });


//Responsive WP Images
$(document).ready(function($){
    $('.entry img').each(function(){ 
        $(this).removeAttr('width')
        $(this).removeAttr('height');
    });

  $('.wp-caption').each(function(){ 
      $(this).css("width", "100%");
  });
 
 
});

//Change URL of "All" Link to All Blog Page
// $('#text-10 ul li.categories ul li.cat-item-all a').attr("href", "https://gentlemansbox.com/blog/all");

$( "#text-8, #text-9" ).wrapInner( "<div class='border'></div>");

//BXSlider
// $(document).ready(function(){
//   $('.bxslider').bxSlider({
//     auto: true,
//     adaptiveHeight: true,
//     pause: 6000
//   });
// });


//Remove Empty WP <p></p> tags
$('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});

//Smooth Scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

//Mobile Menu
$( "#mobilemenu" ).click(function() {
    $(this).toggleClass('clicked');
    $("header nav").slideToggle();
});

//Prevents Nav From Hiding on Resize/Reload w/Mobile Menu
var navblock = function () {
  if ($(window).width() > 768){ 
    $('header nav').show();
    $('article#mainheading img').css("display", "block"); //Fixes Img issue on resize/reload
  }
};
$(document).ready(navblock);
$(window).resize(navblock);

//Blur Main Header Image on Scroll
$(document).ready(function() {
    $(window).scroll(function(e) {
        var s = $(window).scrollTop(),
            d = $(document).height(),
            c = $(window).height(),
            opacityVal = (s / 250);

        $('.blurred-image').css('opacity', opacityVal);
    });
});

//Parallax (Data Speed)
$('section#savvy, figure#headerimg').each(function(){ 
    var $obj = $(this); 

    if ( $(window).width() > 1200 ) {
    $(window).scroll(function() { 
      var yPos = -($(window).scrollTop() / $obj.data('speed')); 
      var bgpos = '50% '+ yPos + 'px'; 
      $obj.css('background-position', bgpos ); 
    }); 
  }
  });


//Fading Copy on Home Page
jQuery(function( $ ){
 
  function fade_home_top() {
    if ( $(window).width() > 800 ) {
        window_scroll = $(this).scrollTop();
        $("#mainheading").css({
          'opacity' : 1-(window_scroll/520)
        });
    }
  }
  $(window).scroll(function() { fade_home_top(); });

});

//Initalize Owl Carousel
// $(document).ready(function() {

//   var owl = $("ul#carousel");

//   owl.owlCarousel({
//       items : 6, //10 items above 1000px browser width
//       itemsDesktop : [1000,6], //5 items between 1000px and 768px
//       itemsTablet: [768,4], //3 items between 768px and 480px
//       itemsMobile : [480,2], // itemsMobile disabled - inherit from itemsTablet option
//       autoPlay: 3000
//   });

//   // Custom Navigation Events
//   $(".next").click(function(){
//     owl.trigger('owl.next');
//   })
//   $(".prev").click(function(){
//     owl.trigger('owl.prev');
//   })

// });


//Fading Quotes
(function() {

    var quotes = $(".quotes");
    var quoteIndex = -1;

    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(5000)
            .fadeOut(2000, showNextQuote);
    }

    showNextQuote();

})();



//Sticky Navigation
$(document).ready(function() {
  var stickyNavTop = $('header[role="banner"]').offset().top;

  var stickyNav = function(){
  var scrollTop = $(window).scrollTop();
       
  if (scrollTop > stickyNavTop) { 
      $('header[role="banner"]').addClass('sticky');
  } else {
      $('header[role="banner"]').removeClass('sticky'); 
  }
};

stickyNav();

$(window).scroll(function() {
  stickyNav();
});
});

//Stops Mobile Safari from opening Web App links in new window


//Target on Click Got To Destination...(:
jQuery(document).ready(function(){

    jQuery(".goto_comment_form").click(function(e){
        e.preventDefault();                                   
        jQuery("html, body").animate({ scrollTop: jQuery('.reviews-simple-feedback').offset().top-120 }, '1000', 'swing');
    });
    
    
    
    //Add js for empty cart
    
     var ammount = jQuery(".cart-contents").find('.count').html();
     if(ammount == 0)
     {
         jQuery(".cart-contents").addClass('empty_card');
         
      }
      
      
      
      
      jQuery("body").on("click", ".add_to_cart_button", function(e){
            //e.preventDefault();   
            //console.log("Oh!!");
            jQuery(this).parents(".main-product-box-outer").addClass("product-box-extand");
    });
      
      
      var default_text = jQuery(".shop_category_btn_link").html();
      
       jQuery("body").on("click", ".shop_category_btn_link", function(e){ 
         var link_obj = jQuery(this);                                                         
         jQuery(this).parents('.widget').find('.shop_category_links_area').slideToggle('slow',function(){
                 if (jQuery(this).is(':visible')) {
                    link_obj.text('Close');                
                } 
                 else {
                     link_obj.text( default_text );                
                }                                                                                                       });
         
       });
        
    
    
});



//////////////

    jQuery(document).ready( function() {
        
        if (jQuery(".woocommerce_account_subscriptions").length) {
            window.cancelProgress = {};    
            prepareSubscriptionCancellation();
        }
        
    });
    
    function prepareSubscriptionCancellation() {
        
        var $ = jQuery, 
            form = $('#cancel_email');
        
        form.find('#send_email').removeClass('clicked success');
        
        $("#subcerrmsg").html("").hide();

        if ($('input[name="cancel_info"]').is(':checked')) {
            $("#subcerrmsg").hide();
        }
        
        $(".try_nocancel").remove();
        $(".satisfydiv").remove();
        
        $('.lbp-inline-link-1').click(function(e){

            var $elm = jQuery(this),
                str = $elm.attr('rel') + "",
                res = str.split(" "),
                
                orderID = $elm.data('orderid'),
                productID = $elm.data('productid'),
                cancelUrl = $elm.data('cancelurl'),
                
                //orderID = res[0],
                //productID = res[1],
                //cancelUrl = res[2],
                
                url = parseUrl(cancelUrl),
                qs = url['querystring'],
                subkey = qs['subscription_id'],            
                nonce = qs['_wpnonce'];
                            
            form.data('submitted', false);
            form.find('#send_email').removeClass('clicked success');
            $("#subcerrmsg").html("").hide();

            jQuery('#order_id').val(orderID);
            jQuery('#subscription_id').val(subkey);
            jQuery('#order_id').data('subscription_id' , subkey);
            jQuery('#order_id').data('nonce' , nonce);
            jQuery('#order_id').data('productID' , productID);

            jQuery('#cancel_subs').attr('href', cancelUrl);

            jQuery("#cancel_2").hide();
            jQuery("#yes_cancel").prop("disabled", false);
            jQuery("#yes_cancel").prop("checked", false);
            jQuery("input[name='cancel_info']").prop('checked', false);

            jQuery('.satisfydiv').remove();    


        });
    
        $("input[name='cancel_info']").click(function(){


            jQuery('.satisfydiv').remove();

            var selectedValue = $(this).val().toLowerCase(),
                orderIDElement = $("#order_id"),
                orderid = orderIDElement.val(),
                subscription_id = orderIDElement.data('subscription_id'),
                productID = orderIDElement.data('productID'),

                cancel_button_item = cancel_button_config["" + productID][selectedValue] || {},

                isDiscountAvailable = (cancel_button_item.button && 
                            subdata && subdata[productID] && 
                            subdata[productID].discount_amount && 
                            subdata[productID].discount_text) ? true : false,
                discount_text = isDiscountAvailable && subdata[productID].discount_text,
                discountButtonText = discount_text,

                cancel_button_caption = cancel_button_item.caption || 'We want you to be 100% satisfied...',
                cancel_button_offer = cancel_button_item.offer || 'Before cancelling we would love to see if we can help resolve your concern....',

                contact_support_html = 
                    '<p>' +
                    '<a href="' + _contactUsUrl + '" class="btn">' +
                    'Click Here To Contact Us And We Will Assist You.' +
                    '</a>' +
                    '</p>',

                offer_button_html = isDiscountAvailable ? (
                        '<p>' +
                        '<p class="subscriptionDiscountMessage"></p>' +
                        '<p><a href="JavaScript:void(0);" onclick="getDiscount(this, ' + 
                            productID + ', \'' + 
                            subscription_id + '\');" class="btn getDiscountButton">' +
                        'Click here to ' + discountButtonText + 
                        '</a>' +'</p>'
                    )         
                    : contact_support_html,

                html = '<div class="satisfydiv">' +
                    '<p class="try_nocancel">' +cancel_button_caption + '</p>' +
                    '<p>' + cancel_button_offer + '</p>' + offer_button_html +
                    '</div>';

            jQuery(this).parent('div').append(html);

        });

        $("#yes_cancel").unbind('click').click(function(){
            
            jQuery("#yes_cancel").attr("disabled", true);
            jQuery("#cancel_2").show();
            window.setTimeout(function(){
                jQuery("#cboxLoadedContent").stop().animate({
                    scrollTop:jQuery(".sorryToHear").position().top
                }, '500', 'swing', function() {
                    //jQuery("input[name='cancel_info']:first").focus();
                });
                
            }, 100);
        });

        $(".cancel_order_rs").click(function(){
            // alert('');
        });
        

        form.find('#send_email').click( function() {

            var orderid = $( "#order_id" ).val();
            var url = $("#cancel_subs").attr('href');
            $('#cancel_url').val(url);


            var dataAlreadySubmitted = form.data('submitted');
            if (!dataAlreadySubmitted && cancelProgress[orderid] === true) {
                form.find('#send_email').html("Cancellation already in progress");
                dataAlreadySubmitted = true;
            }
            if (dataAlreadySubmitted) {            
                return false;
            }

            $("#subcerrmsg").html("").hide();

            if ($('input[name="cancel_info"]:checked').val() == "Other") {
                if ($('textarea[name="cancel_text"]').val() == "") {
                    $("#subcerrmsg").show().html("Please fill out the comment section");
                    form.find("textarea[name='cancel_text']").focus(); 
                    return;
                }
            }        
            if ($('input[name="cancel_info"]').is(':checked')) {


                      // alert(orderid);
                     // $( "#cancel_"+orderid).click();
                     // $( "#cancel_"+orderid).trigger("click");

                form.data('submitted', true);
                form.find('#send_email').html("Cancellation in progress");
                cancelProgress[orderid] = true;
                form.find('#send_email').addClass('clicked');

    //            $.ajax({
    //              type: "POST",
    //              url: form.attr( 'action' ),
    //              data: form.serialize(),
    //              success: function( response ) {
    //                //alert($("#cancel_subs").attr('href'));
    //                form.find('#send_email').html("Cancelled succcessfully");
    //                cancelProgress[orderid] = null;                
    //                form.find('#send_email').removeClass('clicked').addClass('success');                
    //                window.location.href = url;
    //              }
    //            });

                $('#cancel_email').submit();

            } else {
                $("#subcerrmsg").show().html("Please select a reason.");
                form.find("input[name='cancel_info']:first").focus();
                //alert('.')
            }

        });


        window.getDiscount = function(elm, productID, subscription_id) {

            var $elm = $(elm),
                container = $elm.closest('.satisfydiv'),
                messageBox = container.find('.subscriptionDiscountMessage'),
                parent = $elm.closest('#colorbox'),
                orderid = $("#order_id").val(),
                postdata = {
                    'action': 'apply_subscription_discount',
                    'product':  productID,
                    'subscription':  subscription_id
                };

            messageBox.removeClass('show success error');
            parent.addClass('waitSpinner');

            $.ajax({
                dataType: "json",
                type: "POST",
                url: ajaxurl,
                data: postdata,
                "success": function (response) {
                    //console.log(response);
                    var location = window.location.origin + window.location.pathname;
                    console.log(location);
                    messageBox.addClass('show');
                    messageBox.html(response.message);
                    parent.removeClass('waitSpinner');
                    if (response.status == 1) {
                        messageBox.addClass('success');
                        window.location.href = location;
                    }
                    else {
                        messageBox.addClass('error');
                    }
                }
            });

        };  

        window.trim = function (str) {
            return str.replace(/^\s|\s$/g, '');    
        }

        window.parseUrl = function (url) {
            url = url || '';
            var qs = {},
                hashSplit = url.split('#'),
                hash = hashSplit[1] || '',
                noHashUrl = hashSplit[0] || 0,
                allQSSplit = noHashUrl.split('?'),
                qsString = allQSSplit[1] || '',
                noqsURL = allQSSplit[0] || '',
                qsSplit = qsString.split('&'),
                qsVar, i;

             for(i in qsSplit) {
                 qsVar = qsSplit[i].split('=');
                 qs[qsVar[0] || i] = qsVar[1];
             }

            return {url: noqsURL, hash: hash, querystring: qs};
        }
        
        
        $('.backToMyAccount').click(function(e){
            var elm = this,
                $elm = $(this),
                box = $elm.closest('#colorbox'),
                closeBtn = box.find('#cboxClose');
                
            closeBtn.click();
            e.preventDefault();
            
        });
    }
 

