<?php // Quotes Query
$args = array('post_type' => 'post', 'cat' => 28, 'orderby'=> 'rand', 'showposts' => 1);
$tips_query = new WP_Query($args);
while($tips_query->have_posts()) : $tips_query->the_post(); ?>
<div id="post-<?php the_ID(); ?>">
	<h3><?php the_title(); ?></h3>
	<?php the_content(); ?>
</div>
<?php endwhile; wp_reset_postdata(); ?>	
