<section id="buttons" class="col2">
	<div class="wrap">


		<div class="region" id="us">
			<h3 class="us">United States</h3>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=76" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 76);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=74" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 74);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>
		</div>

		<script type="text/javascript">
			mixpanel.track_links("aside a.btn", "click monthly subscription add to cart", {
				"referrer": document.referrer
			});
		</script>

		<div class="region" id="canada">
			<h3 class="canada">Canada</h3>
			<em>Shipping to Canada is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=1162" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 1162);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=1161" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 1161);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

		</div>


		<div class="region" id="mexico">
			<h3 class="mexico">Mexico</h3>
			<em>Shipping to the Mexico‎ is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20265" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20265);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20266" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20266);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

		</div>



		<div class="region" id="uk">
			<h3 class="uk">United Kingdom</h3>
			<em>Shipping to the United Kingdom is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19600" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19600);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19703" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19703);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

		</div>

		<div class="region" id="aust">
			<h3 class="aust">Australia</h3>
			<em>Shipping to the Australia‎ is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19711" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19711);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19710" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19710);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

		</div>

		<div class="region" id="nz">
			<h3 class="nz">New Zealand</h3>
			<em>Shipping to the New Zealand‎ is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20276" class="btn maroon">Subscribe Monthly</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20276);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20275" class="btn maroon">Subscribe Annually</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20275);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>	
				<p class="value">(That's 1 month free)</p>
			</aside>

		</div>

	</div>
</section>