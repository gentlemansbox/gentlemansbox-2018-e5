<header id="header" role="banner" <?php if ( current_user_can('subscriber') ) : ?>class="subscriber"<?php endif; ?>>
	<div class="wrap">

			<div id="mobilemenu"><span></span></div>

			<nav id="nav" role="navigation">
				<?php wp_nav_menu( array('menu' => 'Primary Menu') ); ?>
				<?php echo do_shortcode( '[facebook][twitter][instagram]' ); ?>
			</nav>

			<div id="logo"><a href="<?php echo home_url(); ?>">Gentleman's Box</a></div>
			
			<div id="account">	
				<!-- WooCommerce -->
				<?php if ( is_user_logged_in() ) { ?>
					<div class="contentloggedin">
						<span class="hello">Hello <?php global $current_user; get_currentuserinfo(); echo $current_user->user_firstname . '!';?></span> <span class="logout"><a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" title="Logout">Logout</a></span>
						<span class="myaccount"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></span>
					</div>
				<?php } 
				else { ?>
					<div class="contentloggedout">
						<span class="already">Already have an account?&nbsp;</span> 
						<span><a class="login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login Here','woothemes'); ?>">
							<?php _e('<span class="log-in">Login&nbsp;</span> <span class="here">Here</span>','woothemes'); ?>
						</a></span>
					</div>
				<?php } ?>

				<?php global $woocommerce; ?>
					<span class="cart-contents">
					<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
						<span class="count"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
						<span class="total"><?php echo $woocommerce->cart->get_cart_total(); ?></span>
						<span class="countmobile"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
					</a>
					</span>
			</div><!--#account-->

	</div><!--.wrap-->
</header>
