<section id="buttons" class="col3">
	<div class="wrap">

		<!-- <ul id="countries">
			<li><a href="#us">United States</a></li>
			<li><a href="#canada">Canada</a></li>
			<li><a href="#uk">United Kingdom</a></li>
			<li><a href="#aust">Australia‎</a></li>
			<li><a href="#mexico">Mexico</a></li>
			<li><a href="#nz">New Zealand</a></li>
		</ul> -->

		<div class="region" id="us">
			<h3 class="us">United States</h3>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=124" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 124);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=125" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 125);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=175" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 175);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>
		</div>

		<script type="text/javascript">
			mixpanel.track_links("aside a.btn", "click gift add to cart", {
				"referrer": document.referrer
			});
		</script>


		<div class="region" id="canada">
			<h3 class="canada">Canada</h3>
			<em>Shipping to Canada is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=1160" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 1160);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=1159" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 1159);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=1156" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 1156);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>

		</div>


		<div class="region" id="mexico">
			<h3 class="mexico">Mexico</h3>
			<em>Shipping to the Mexico is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20267" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20267);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20268" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20268);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20269" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20269);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>

		</div>

		<div class="region" id="uk">
			<h3 class="uk">United Kingdom</h3>
			<em>Shipping to the United Kingdom is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19704" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19704);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19705" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19705);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19706" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19706);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>

		</div>


		<div class="region" id="aust">
			<h3 class="aust">Australia‎</h3>
			<em>Shipping to the Australia‎ is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19709" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19709);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19708" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19708);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=19707" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 19707);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>

		</div>


		<div class="region" id="nz">
			<h3 class="nz">New Zealand</h3>
			<em>Shipping to the New Zealand is $8/month per box<br>
			All prices are USD</em>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20274" class="btn maroon">3-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20274);
				$price_query = new WP_Query($args);
				while($price_query->have_posts()) : $price_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20273" class="btn maroon">6-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20273);
				$price2_query = new WP_Query($args);
				while($price2_query->have_posts()) : $price2_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
			</aside>

			<aside>
				<a href="<?php echo home_url(); ?>/?add-to-cart=20272" class="btn maroon">12-Month Gift</a>
				<?php //Get Product Price by ID
				$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 20272);
				$price3_query = new WP_Query($args);
				while($price3_query->have_posts()) : $price3_query->the_post(); ?>
					<?php echo $product->get_price_html(); ?>
				<?php endwhile; wp_reset_postdata(); ?>		
				<p class="value">(Includes 1 month free)</p>
			</aside>

		</div>

	</div>
</section>




<!-- Gift Info -->
<div class="wrap">
	<section id="giftinfo">
		<?php if( get_field('gift_info') ): ?>
			<?php the_field('gift_info'); ?>
		<?php endif; ?>
	</section>
</div>


