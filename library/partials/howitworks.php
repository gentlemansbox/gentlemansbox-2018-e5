<section id="howitworks">
	<a id="next" name="next" class="anchor"></a>
	<div class="wrap">
		
			<div class="left">
				<img src="<?php bloginfo('template_url'); ?>/library/images/august-trans.png" alt="Gentleman's Box" />
			</div>

			<div class="right">
				<h2>How it works</h2>
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p> -->

				<article>
					<div class="step">
						<em>Step</em>
						<span>01</span>
					</div>
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('How It Works Step 1') ) : ?><?php endif; ?>
				</article>

				<article>
					<div class="step">
						<em>Step</em>
						<span>02</span>
					</div>
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('How It Works Step 2') ) : ?><?php endif; ?>
				</article>

				<article>
					<div class="step">
						<em>Step</em>
						<span>03</span>
					</div>
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('How It Works Step 3') ) : ?><?php endif; ?>
				</article>
			</div>

		<?php if(is_front_page() ) { ?>
			<div class="block">
				<a href="<?php echo home_url(); ?>/what-to-expect" class="btn maroon large">Learn More</a>
			</div>
		<?php } ?>

	</div>
</section>