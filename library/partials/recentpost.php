<div class="recentnews">
	<?php $the_query = new WP_Query( 'showposts=1&cat=-28,-29' ); ?>
	<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
		<div class="excerpt">
			<?php echo excerpt(26); ?>
		</div>
	<?php endwhile;?>
	<div class="links">
	<a href="<?php the_permalink(); ?>" class="btn gold-transparent">Read this Post</a>
	or <a href="<?php echo get_permalink(23); ?>">View the Entire Blog</a>
	</div>
</div>

