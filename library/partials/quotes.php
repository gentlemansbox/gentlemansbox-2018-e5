<?php // Quotes Query
$args = array('post_type' => 'post', 'cat' => 29, 'orderby'=> 'rand', 'showposts' => 1);
$quotes_query = new WP_Query($args);
while($quotes_query->have_posts()) : $quotes_query->the_post(); ?>
<div id="post-<?php the_ID(); ?>">
	<blockquote><?php the_content(); ?></blockquote>
	<cite><?php the_title(); ?></cite>
</div>
<?php endwhile; wp_reset_postdata(); ?>	
