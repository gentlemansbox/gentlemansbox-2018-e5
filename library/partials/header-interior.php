<figure id="headerimg" data-speed="2">
	<div class="wrap">
		<?php if ( is_home() ) { ?>
    		<h1>Blog</h1>
		<?php } else { ?>
			<h1><?php the_title(); ?></h1>
		<?php } ?>
	</div>
</figure>