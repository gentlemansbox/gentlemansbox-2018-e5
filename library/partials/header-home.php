<section id="main">
	<div id="blurred-image-container">
		<div class="img-src"></div>
		<div class="img-src blurred-image"></div>
	</div>

	<div class="wrap">
		<article id="mainheading">
			<img src="<?php bloginfo('template_directory'); ?>/library/images/gbox-seal.png" alt="Gentleman's Box" style="visibility:hidden;" />
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="entry">
					<?php the_content(); ?>
				</div>
				<a href="<?php echo get_permalink(25819); ?>" class="btn maroon">Join Now</a>
				<a href="<?php echo get_permalink(11); ?>" class="btn gold">What to Expect</a>

				<a href="#next">
					<div class="scroll">
			 			<span>Scroll Down</span>
			 		</div>
		 		</a>
			<?php endwhile; endif; ?>
		</article>
	</div><!--.wrap-->
</section>