<section id="saying">
	<div class="wrap">
	<h2>What People Are Saying</h2>
	<ul id="testimonials">
	<?php //Testimonials Query
		query_posts( array( 'post_type' => 'testimonial', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<li class="quotes" id="post-<?php the_ID(); ?>">
			<?php the_content(); ?>

			<?php if( get_field('url') ): ?>
				<cite><a href="<?php the_field('url'); ?>" target="_blank"><?php the_title(); ?></a></cite>
			<?php else : ?>
				<cite><?php the_title(); ?></cite>
			<?php endif; ?>
		</li>
		<?php endwhile; endif; wp_reset_query(); ?>
		</ul>
	</div>	
</section>