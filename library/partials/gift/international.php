<aside>
  <div class="mostpop"><span>BEST VALUE</span></div>
  <h2>12-Month Gift</h2>
  <span class="shippingto <?php echo $post->post_name;?>"> <a href="#">Shipping
  To</a> </span>
  <div class="changecountry"><a href='/gift-subscription/'>Change Country</a></div>
  <div class="imgcontainer"> <img src="<?php bloginfo('template_url'); ?>/library/images/12monthgift.png" alt="Gentlemans Box" /> </div>
  <span class="woocommerce-Price-amount amount">
		<span class="woocommerce-Price-currencySymbol">$</span>
		371.00
	</span>
  </br>
  <span class="woocommerce-Price-amount amount free">
    1 Month Free
  </span>
  <em>All prices are USD</em>
  <a href="<?php echo home_url(); ?>/?add-to-cart=1156" class="btn maroon">Give
  12 Boxes</a> </aside>
<aside>
  <h2>6-Month Gift</h2>
  <span class="shippingto <?php echo $post->post_name;?>"> <a href="#">Shipping
  To</a> </span>
  <div class="changecountry"><a href='/gift-subscription/'>Change Country</a></div>
  <div class="imgcontainer"> <img src="<?php bloginfo('template_url'); ?>/library/images/6monthgift.png" alt="Gentlemans Box" /> </div>
  <span class="woocommerce-Price-amount amount">
		<span class="woocommerce-Price-currencySymbol">$</span>
		198.00
	</span>
  <em>All prices are USD</em> <a href="<?php echo home_url(); ?>/?add-to-cart=1159" class="btn maroon" style="margin-top:70px;">Give
  6 Boxes</a> </aside>
  <aside>
    <h2>3-Month Gift</h2>
    <span class="shippingto <?php echo $post->post_name;?>"> <a href="#">Shipping
    To</a> </span>
    <div class="changecountry"><a href='/gift-subscription/'>Change Country</a></div>
    <div class="imgcontainer"> <img src="<?php bloginfo('template_url'); ?>/library/images/3monthgift.png" alt="Gentlemans Box" /> </div>
    <span class="woocommerce-Price-amount amount">
  		<span class="woocommerce-Price-currencySymbol">$</span>
  		99.00
  	</span>
    <em>All prices are USD</em> <a href="<?php echo home_url(); ?>/?add-to-cart=1160" class="btn maroon" style="margin-top:70px;">Give
    3 Boxes</a> </aside>
