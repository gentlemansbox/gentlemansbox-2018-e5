<aside>
	<h2>3-Month Gift</h2>
	<span class="shippingto <?php echo $post->post_name;?>">
		<a href="#">Shipping To</a>
	</span>
	<div class="changecountry">Change Country</div>
	<div class="imgcontainer">
		<img src="<?php bloginfo('template_url'); ?>/library/images/boxes-3.png" alt="Gentlemans Box" />
	</div>
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 26253);
	$price_query = new WP_Query($args);
	while($price_query->have_posts()) : $price_query->the_post(); ?>
		<?php echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>	
	<em>All prices are USD</em>
	<a href="<?php echo home_url(); ?>/?add-to-cart=26253" class="btn maroon">Give 3 Boxes</a>
</aside>

<aside>
	<h2>6-Month Gift</h2>
	<span class="shippingto <?php echo $post->post_name;?>">
		<a href="#">Shipping To</a>
	</span>
	<div class="changecountry">Change Country</div>
	<div class="imgcontainer">
		<img src="<?php bloginfo('template_url'); ?>/library/images/boxes-6.png" alt="Gentlemans Box" />
	</div>
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 26254);
	$price2_query = new WP_Query($args);
	while($price2_query->have_posts()) : $price2_query->the_post(); ?>
		<?php echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>
	<em>All prices are USD</em>	
	<a href="<?php echo home_url(); ?>/?add-to-cart=26254" class="btn maroon">Give 6 Boxes</a>	
</aside>

<aside>
	<div class="mostpop"><span>Most Popular</span></div>
	<h2>12-Month Gift</h2>
	<span class="shippingto <?php echo $post->post_name;?>">
		<a href="#">Shipping To</a>
	</span>
	<div class="changecountry">Change Country</div>
	<div class="imgcontainer">
		<img src="<?php bloginfo('template_url'); ?>/library/images/boxes-12.png" alt="Gentlemans Box" />
	</div>
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 26255);
	$price3_query = new WP_Query($args);
	while($price3_query->have_posts()) : $price3_query->the_post(); ?>
		<?php echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>
	<em>All prices are USD</em>	
	<p class="value">(Includes 1 month free)</p>
	<a href="<?php echo home_url(); ?>/?add-to-cart=26255" class="btn maroon">Give 12 Boxes</a>
</aside>