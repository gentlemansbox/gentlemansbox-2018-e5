<ul>
<?php //Show All Categories
	$args = array(
		'show_option_all'    => 'All',
		'orderby'            => 'name',
		'order'              => 'ASC',
		'style'              => 'list',
		'show_count'         => 0,
		'hide_empty'         => 1,
		'use_desc_for_title' => 0,
		'child_of'           => 0,
		'hierarchical'       => 1,
		'echo'               => 1,
		'depth'              => 0,
		'current_category'   => 0,
		'pad_counts'         => 0,
		'taxonomy'           => 'category',
		'walker'             => null
	);
	wp_list_categories( $args ); 
?>
</ul>