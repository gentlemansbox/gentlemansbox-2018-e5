<?php if( is_front_page() || is_page(386867) ) { //HOME PAGE  ?>

  <!-- VIDEO BACKGROUND -->

<?php } elseif ( is_home() ) { //BLOG PAGE  ?>

  <figure id="headerimg" data-speed="2">
    <div class="wrap">
      <h1>Blog</h1>
    </div>
  </figure>

<?php } elseif ( is_product_category() ) { //STORE CATEGORY PAGE  ?>

  <figure id="headerimg" data-speed="2">
    <div class="wrap">
      <h1><?php echo  single_term_title(); ?></h1>
    </div>
  </figure>

<?php } elseif ( is_shop() ) { //STORE PAGE  ?>

  <figure id="headerimg" style="background-image: url('https://c3sgentleman.staging.wpengine.com/wp-content/uploads/2017/01/GentlemansClosetHeader.png');">
    <div class="wrap">
      <h1>GENTLEMAN'S CLOSET</h1>
      <p class="text-center" style="color:#fff; font-family: 'Playfair Display',serif; line-height: 1;font-size: 1.5em;margin-top: 1em;">Shop the latest accessories</p>
    </div>
  </figure>

<div style="clear:both"></div>

<?php } elseif (is_page(481975)) { /*VIDEO LANDING PAGE VIDEO HEADING*/ ?>

  <div class="videoContainer fh5co-hero">
  <div class="overlay"></div>
    <div class="videoInner">
      <video preload="true" autoplay muted loop volume = "0" poster = "" >
        <source src="https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/images/video_landing_page_header.mp4" type="video/mp4">
        <source src="https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/images/video_landing_page_header.webm" type="video/webm">
       
      </video>
    </div>
    <a href="https://gentlemansbox.com/">
      <h1>Welcome to Gentleman's Box!</h1>
      <h2 style="color: #fff;">Come see your box being built right now.</h2>
    </a>
   </div>
<?php } elseif ( is_page_template( 'template-landing.php' ) ) { ?> 

  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
  <figure id="headerimg" style="background-image: url('<?php echo $image[0]; ?>')">
    <div class="wrap">
      <h1><?php the_title(); ?></h1>
    </div>
  </figure>

<?php } elseif ( is_archive() ) { ?>

  <figure id="headerimg" data-speed="2">
    <div class="wrap">
      <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
      <?php /* If this is a category archive */ if (is_category()) { ?>
        <h1><?php _e('Archive:','html5reset'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php _e('Category','html5reset'); ?></h1>
      <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
        <h1><?php _e('Tagged:','html5reset'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h1>
      <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
        <h1><?php _e('Archive:','html5reset'); ?> <?php the_time('F jS, Y'); ?></h1>
      <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
        <h1><?php _e('Archive:','html5reset'); ?> <?php the_time('F, Y'); ?></h1>
      <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
        <h1 class="pagetitle"><?php _e('Archive:','html5reset'); ?> <?php the_time('Y'); ?></h1>
      <?php /* If this is an author archive */ } elseif (is_author()) { ?>
        <h1 class="pagetitle"><?php _e('Author:','html5reset'); ?></h1>
      <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
        <h1 class="pagetitle"><?php _e('Blog:','html5reset'); ?></h1>
      <?php } ?>
    </div>
  </figure>

<?php } elseif ( is_single() ) { ?>

  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
  <figure id="headerimg" class="single-post-title" style="background-image: url('<?php echo $image[0]; ?>')">
    <div class="wrap">
      <h1><?php the_title(); ?></h1>
    </div>
    <div class="overlay"></div>
  </figure>

<?php } else { ?>

  <figure id="headerimg" data-speed="2">
    <div class="wrap">
      <h1><?php the_title(); ?></h1>
    </div>
  </figure>

<?php } ?>