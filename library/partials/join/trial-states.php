<style>
.left-us-box
{
float:left;
width:48%;
margin-right:2%;
text-align:center;
}
.left-us-box aside
{
width:100% !important;
margin-bottom:20px;
}
.learn-more-text
{
font-family:'brandon_grotesquemedium', sans-serif;
color:#919191;
vertical-align:top;
display:inline-block;
padding: 4px 15px;
background:none !important;
cursor:pointer;
letter-spacing:1px;
}
.bottom-info-box-with-learn-more img
{
width:30px;
height:30px;
}

.usper
{
margin-bottom:10px;}

</style>

<?php
global $post;

$TRAIL_INTERNATIONAL_PRODUCT_ID = get_post_meta($post->ID, 'TRAIL_INTERNATIONAL_PRODUCT_ID',true);

$TRAIL_UNITED_STATES_PRODUCT_ID = get_post_meta($post->ID, 'TRAIL_UNITED_STATES_PRODUCT_ID',true);


$TRAIL_UNITED_STATES_PRODUCT_ID = !empty($TRAIL_UNITED_STATES_PRODUCT_ID) ? $TRAIL_UNITED_STATES_PRODUCT_ID:74;

$TRAIL_INTERNATIONAL_PRODUCT_ID = !empty($TRAIL_INTERNATIONAL_PRODUCT_ID) ? $TRAIL_INTERNATIONAL_PRODUCT_ID:76;

?>

<div class="left-us-box">
<aside class="monthly">
	<p class="join-box-main-heading">UNITED STATES</p>
    <span class="amount">
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => $TRAIL_UNITED_STATES_PRODUCT_ID);
	$price_query = new WP_Query($args);
	while($price_query->have_posts()) : $price_query->the_post(); ?>
		<?php //echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>	
    $0.00
    </span>
	<span class="usper">FIRST MONTH FREE</span>
	<em class="doller-text">Pay only $10 shipping.</em>
	<a href="<?php echo home_url(); ?>/?add-to-cart=<?php echo $TRAIL_UNITED_STATES_PRODUCT_ID; ?>" class="btn maroon">TRY NOW</a>
</aside>


</div>


<div class="left-us-box">
<aside class="annual">
	<p class="join-box-main-heading">INTERNATIONAL</p>
	<span class="amount">
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => $TRAIL_INTERNATIONAL_PRODUCT_ID);
	$price2_query = new WP_Query($args);
	while($price2_query->have_posts()) : $price2_query->the_post(); ?>
		<?php //echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>
    $0.00
    </span>
	<span class="usper">FIRST MONTH FREE</span>
	<em class="doller-text">Pay only $13.20 USD shipping</em>
	<a href="<?php echo home_url(); ?>/?add-to-cart=<?php echo $TRAIL_INTERNATIONAL_PRODUCT_ID; ?>" class="btn maroon">TRY NOW</a>	
</aside>
</div>
</div>



