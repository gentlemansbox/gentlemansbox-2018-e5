<aside class="monthly">
	<h2>Monthly Plan</h2>
	<span class="shippingto <?php echo $post->post_name;?>">
		<a href="#">Shipping To</a>
	</span>
	<div class="changecountry">Change Country</div>
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 26161);
	$price_query = new WP_Query($args);
	while($price_query->have_posts()) : $price_query->the_post(); ?>
		<?php echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>	
	<span class="per">Per Month</span>
	<em>All prices are USD</em>
	<a href="<?php echo home_url(); ?>/?add-to-cart=26161" class="btn maroon">Subscribe Monthly</a>
</aside>

<aside class="annual">
	<div class="mostpop"><span>Best Value</span></div>
	<h2>Annual Plan</h2>
	<span class="shippingto <?php echo $post->post_name;?>">
		<a href="#">Shipping To</a>
	</span>
	<div class="changecountry">Change Country</div>
	<?php //Get Product Price by ID
	$args = array('post_type' => 'product', 'posts_per_page' => 1, 'page_id' => 26162);
	$price2_query = new WP_Query($args);
	while($price2_query->have_posts()) : $price2_query->the_post(); ?>
		<?php echo $product->get_price_html(); ?>
	<?php endwhile; wp_reset_postdata(); ?>
	<span class="per">Per Year</span>
	<em>(That's one month free)</em>
	<em>All prices are USD</em>	
	<a href="<?php echo home_url(); ?>/?add-to-cart=26162" class="btn maroon">Subscribe Annually</a>	
</aside>
