<section id="featuredin">
	<div class="wrap">
		<h2>Featured In</h2>
		<ul>
			<?php // Partner Query
			$args = array('post_type' => 'featuredin', 'orderby'=> 'rand', 'showposts' => -1);
			$partner_query = new WP_Query($args);
			$featuredincount = 0;
			while($partner_query->have_posts()) : $partner_query->the_post(); $featuredincount++; ?>
			<li id="post-<?php the_ID(); ?>">
				<?php if( get_field('featured_url') ): ?>
				<a href="<?php the_field('featured_url'); ?>" target="_blank">
					<?php the_post_thumbnail(); ?>
				</a>
				<?php endif; ?>
			</li>
			<?php endwhile; wp_reset_postdata(); ?>				
		</ul>
	</div>
</section>
<?php if ($featuredincount == 0 ) { ?> <style> #featuredin { display:none !important; }</style> <?php } ?>