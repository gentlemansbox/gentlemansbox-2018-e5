<?php get_header(); ?>

<div class="wrap">
	
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</article>
	<?php endwhile; endif; ?>
</div>

<?php 
	if(is_page(13)): include (TEMPLATEPATH . '/library/partials/join.php' );
	elseif(is_page(15)): include (TEMPLATEPATH . '/library/partials/gift.php' );
	else : '';
endif; ?>


<?php get_footer(); ?>
