<?php /*
Template Name: Murder Mystery
*/ ?>

<?php get_header('murder-mystery'); ?>

<!-- HERO -->
<?php get_template_part( 'template-parts/content', 'murder-mystery-hero' ); ?>

<!-- PAGE CONTENT -->
<?php get_template_part( 'template-parts/content', 'page' ); ?>

<?php get_footer('new'); ?>