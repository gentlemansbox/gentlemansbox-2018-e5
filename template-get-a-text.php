<?php /*
Template Name: Get a Text
*/ ?>

<!--                                   -->
<!-- ADD TO HEADER.PHP DURING CLEAN UP -->
<!--                                   -->

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php wp_head(); ?>

<!-- FAVICON -->
  <link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- MOBILE SITE MEDIA QUERY -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- MAIN CSS -->
  <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/base-theme.min.css" />
<!-- ANIMATIONS CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.min.css" />
<!-- SLICK SLIDER -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick.min.css" />
<!-- GRAVITY FORMS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/formsmain.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/readyclass.min.css" />
<!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />


</head>

<body <?php body_class(); ?>>

<!--                                    -->
<!-- /ADD TO HEADER.PHP DURING CLEAN UP -->
<!--                                    -->


<!--                           -->
<!-- TEXT LANDING PAGE CONTENT -->
<!--                           -->

<?php
$order = 0;
$order = new WC_Subscription($_GET[sub]);
$order_id = $order->id;
if($order_id !== 0){
  $order->update_status('cancelled');
}
?>

<!-- NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'landing-page-header' ); ?>

<main class="full-width">
  <section class="landing-top">
    <div class="background clearfix">
    <?php if ( is_page(295422) || is_page(333536) || is_page(333547) || is_page(482398) || is_page(483411)) { ?>
    <!-- VALENTINES DAY 2017 AND GBOX VS SPREZZA -->
      <div class="full-bg full-width" style="background-image:url(<?php the_field('full_image'); ?>);"></div>


    <?php } else { ?>
      <div class="left-bg one-half-full" style="background-image:url(<?php the_field('left_image'); ?>);"></div>
      <div class="right-bg one-half-full" style="background-image:url(<?php the_field('right_image'); ?>);"></div>
    <?php } ?>
    </div>
    <div class="page-title vertical-align-parent">
      <div class="vertical-align-content">
        <?php the_field('main_title'); ?>
      </div>
    </div>
  </section>

<!-- GBOX VS SPREZZA LANDING PAGE -->
<?php if(is_page(333536) || is_page(333547)){ ?>
  <section class="full-width" style="background-image:url(<?php the_field('how_it_works_image'); ?>);">
    <div class="max-width">
    <?php if(is_page(333536)) { ?>
      <p style="padding-bottom: 3em;padding-top: 5em;font-size: 1.5em;">
        The easiest way for men to find new style is through subscription boxes. Most boxes contain the usual products: sock, tie, and pocket square. The box experience, quality of the products, and brand interaction makes the difference. Always compare men’s subscription boxes before you subscribe. We created a subscription box comparison chart to show you how Gentleman's Box compares to Sprezzabox.
      </p>
   <?php } else { ?>
        <p style="padding-bottom: 3em;padding-top: 5em;font-size: 1.5em;">
        The easiest way for men to find new style is through subscription boxes. Most boxes contain the usual products: sock, tie, and pocket square. The box experience, quality of the products, and brand interaction makes the difference. Always compare men’s subscription boxes before you subscribe. We created a subscription box comparison chart to show you how Gentleman's Box compares to Urban Dapper Club.
      </p>
    <?php } ?>
      <img src="<?php the_field('get_swag_image'); ?>" style="margin-bottom: 5em;"/>
    </div>
  </section>
    <section class="give-try dark-bg red-background">
    <h2>Ready to subscribe?</h2>
    <a href="/join/" class="primary-button">JOIN NOW</a>
  </section>

<?php } ?>

<?php if ( is_page(295658) ) { ?>
  <section class="gifts">
    <h2>Just for signing up, we’ll give you:</h2>
    <?php if( have_rows('gifts') ) { ?>
      <div class="clearfix max-width">
        <?php while ( have_rows('gifts') ) : the_row(); ?>
            <div class="gift-item">
              <?php $image = get_sub_field('gift_image'); ?>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              <p><?php the_sub_field('gift_title'); ?></p>
            </div>
        <?php endwhile; ?>
      </div>
    <?php } else {
        //nothing
      }
    ?>
    <h3>(JUST PAY $3 SHIPPING AND HANDLING)<br/>THAT’S ALL. SCOUT’S HONOR.</h3>
  </section>
  <section class="give-try dark-bg red-background">
    <h2>Want to give it a try?</h2>
    <a href="/on-demand/sign-up" class="primary-button">SIGN UP FOR A FREE TRIAL</a>
  </section>

  <section class="work-purpose">
    <div class="work">
      <div class="one-half-full" style="background-image:url(<?php the_field('how_it_works_image'); ?>);"></div>
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <h2>How does it work?</h2>
          <h3>IT’S EASY!</h3>
          <?php the_field('how_it_works'); ?>
        </div>
      </div>
    </div>
    <div class="purpose">
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <h2>A new way to get swag.</h2>
          <h3>WE WANT YOU TO TRY IT. NO STRINGS ATTACHED.</h3>
          <?php the_field('get_swag'); ?>
        </div>
      </div>
      <div class="one-half-full" style="background-image:url(<?php the_field('get_swag_image'); ?>);"></div>
    </div>
  </section>

  <section class="give-try dark-bg green-background">
    <h2>Want to give it a try?</h2>
    <a href="/on-demand/sign-up" class="secondary-button">SIGN UP FOR A FREE TRIAL</a>
  </section>
  <?php if ( $post->post_content=="" ) { ?>
    <?php // NOTHING ?>
  <?php } else { ?>
  	<section class="page-contents full-width">
      <div class="max-width">
  	   <?php the_content(); ?>
      </div>
  	</section>
  <?php } ?>
<?php } ?>

<?php if ( is_page(295658) || is_page(333536)  || is_page(333547) ) { ?>
  <section class="box-slider">
    <div class="max-width">
      <h2>What you’ve missed</h2>
      <div class="slider-for">
        <?php if( have_rows('slider', 'options') ) {
            while ( have_rows('slider', 'options') ) : the_row(); ?>

                <div class="slick-slide clearfix">
                  <div class="slide-top full-width clearfix">
                    <h3><?php the_sub_field('box_month', 'options');?></h3>
                    <h3><?php the_sub_field('box_year', 'options');?></h3>
                  </div>
                  <div class="box-image">
                    <?php $image = get_sub_field('box_image', 'options'); ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  </div>
                  <div class="box-info">
                    <h4><?php the_sub_field('box_title', 'options'); ?></h4>
                    <?php the_sub_field('box_items', 'options'); ?>
                    <hr>
                    <h3>TOTAL RETAIL COST OF THIS BOX:</h3>
                    <div class="box-cost vertical-align-parent">
                      <div class="vertical-align-content">$<?php the_sub_field('box_retail_cost', 'options'); ?></div>
                    </div>
                  </div>
                </div>

            <?php endwhile;
        } else {
            // nothing
        } ?>
      </div>

      <div class="slider-nav">
        <?php if( have_rows('slider', 'options') ) {
            while ( have_rows('slider', 'options') ) : the_row(); ?>

                <div class="slick-slide"><h3><?php the_sub_field('box_month', 'options');?></h3></div>

            <?php endwhile;
        } else {
            // nothing
        } ?>
      </div>
    </div>
  </section>
   <?php } else { ?>
    <?php if(is_page(482398) || is_page(483411)) { ?>
      <section class="give-try dark-bg red-background awesome-deals">
        <h2>Ready to Sign Up?</h2>
        <a href="/join/" class="primary-button">JOIN NOW</a>
      </section>
        <section class="work-purpose">
    <div class="work">
      <div class="one-half-full" style="background-image:url(<?php the_field('how_it_works_image'); ?>);"></div>
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <h2>How it Works:</h2>
          <h3>STYLE ON CRUISE CONTROL</h3>
          <?php the_field('get_swag'); ?>
        </div>
      </div>
    </div>
    <div class="purpose">
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <h2>Why Gentleman's Box?</h2>
          <h3>FOR THE GRADUATE IN YOUR LIFE</h3>
          <?php the_field('how_it_works'); ?>

        </div>
      </div>
      <div class="one-half-full" style="background-image:url(<?php the_field('get_swag_image'); ?>);"></div>
    </div>
  </section>
    <section class="give-try dark-bg green-background">
    <h2>BUY NOW!</h2>
  </section>
  <section class="gifts">
        <div class="clearfix max-width">
        <?php if(is_page(482398)) { ?>
          <a href="https://gentlemansbox.com/?add-to-cart=64660">
       <?php } else { ?>
           <a href="https://gentlemansbox.com/?add-to-cart=1162">
       <?php } ?>
            <div class="gift-item">
              <p class="gift-title">MONTHLY SUBSCRIPTION</p>
              <p class="gift-code">ADD TO CART</p>
            </div>
           </a>
        <?php if(is_page(482398)) { ?>
          <a href="https://gentlemansbox.com/?add-to-cart=124">
       <?php } else { ?>
           <a href="https://gentlemansbox.com/?add-to-cart=1160">
       <?php } ?>
            <div class="gift-item">
              <p class="gift-title">3-MONTH GIFT</p>
              <p class="gift-code">ADD TO CART</p>
            </div>
           </a>
        <?php if(is_page(482398)) { ?>
          <a href="https://gentlemansbox.com/?add-to-cart=125">
       <?php } else { ?>
           <a href="https://gentlemansbox.com/?add-to-cart=1159">
       <?php } ?>
            <div class="gift-item">
              <p class="gift-title">6-MONTH GIFT</p>
              <p class="gift-code">ADD TO CART</p>
            </div>
           </a>
        <?php if(is_page(482398)) { ?>
          <a href="https://gentlemansbox.com/?add-to-cart=175">
       <?php } else { ?>
           <a href="https://gentlemansbox.com/?add-to-cart=1156">
       <?php } ?>
            <div class="gift-item">
              <p class="gift-title">12-MONTH GIFT</p>
              <p class="gift-code">ADD TO CART</p>
            </div>
           </a>
      </div>
  </section>
    <?php  } else { ?>
   <!-- VALENTINES DAY 2017 -->
  <section class="give-try dark-bg red-background awesome-deals">
    <h2>Your gift search has come to an end.</h2>
  </section>
  <?php if ( $post->post_content=="" ) { ?>
    <?php // NOTHING ?>
  <?php } else { ?>
    <section class="page-contents full-width">
      <div class="max-width">
       <?php the_content(); ?>
      </div>
    </section>
  <section class="gifts">
      <?php if( have_rows('gifts') ) { ?>
        <div class="clearfix max-width">
          <?php while ( have_rows('gifts') ) : the_row(); ?>
           <a href="<?php the_sub_field('gift_link'); ?>">
            <div class="gift-item">
              <p class="gift-title"><?php the_sub_field('gift_title'); ?></p>
              <p class="gift-code"><?php the_sub_field('gift_code'); ?></p>
            </div>
           </a>
        <?php endwhile; ?>
      </div>
    <?php } else {
        //nothing
      }
    ?>
  </section>
    <section class="give-try dark-bg red-background">
      <h2>Ready to subscribe?</h2>
      <a href="/join/" class="primary-button">JOIN NOW</a>
    </section>
  <?php } ?>
<?php } ?>
      <?php } ?>

</main>

<!-- FOOTER -->
  <?php get_template_part( 'template-parts/content', 'landing-page-footer' ); ?>

<!--                           -->
<!-- /TEXT LANDING PAGE CONTENT -->
<!--                           -->

<!--                                   -->
<!-- ADD TO FOOTER.PHP DURING CLEAN UP -->
<!--                                   -->

<!-- SMOOTH SCROLLING -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.min.js"></script>
<!-- STICKY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
<!-- ENTRANCE ANIMATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- SLICK SLIDERS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>
<!-- DECLARATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/declarations.min.js"></script>

</body>

<?php wp_footer(); ?>

</html>

<!--                                    -->
<!-- /ADD TO FOOTER.PHP DURING CLEAN UP -->
<!--                                    -->
