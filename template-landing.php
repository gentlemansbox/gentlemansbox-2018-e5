<?php /*
Template Name: Landing Pages
*/ ?>

<?php get_header(); ?>

<!-- PAGE CONTENT -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="wrap">
    <article class="post" id="post-<?php the_ID(); ?>">
      <div class="entry">
        <?php the_content(); ?>
      </div>
    </article>
  </div>
<?php endwhile; endif; ?>

<!-- LOOHK GOOD | FEEL GOOD | BE GOOD -->
<?php if ( is_page(12408) ) { ?>
<?php //Groomsmen Gifts ?>
  <div class="fh5co-parallax section6" data-stellar-background-ratio="0.5">
    <div class="container landing-moto">
      <div class="row">
        <div class="col-md-4 col-sm-12">
          <figure>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tuxIcon.png" />
          <figcaption>
          <h3>Look Good.</h3>
          </figcaption>
          </figure>
        </div>
        <div class="col-md-4 col-sm-12">
          <figure>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/img/muscleIcon.png" />
          <figcaption>
          <h3>Feel Good.</h3>
          </figcaption>
          </figure>
        </div>
        <div class="col-md-4 col-sm-12">
          <figure>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/img/handshakeIcon.png" />
          <figcaption>
          <h3>Be Good.</h3>
          </figcaption>
          </figure>
        </div>
      </div>
    </div>
  </div>
<?php } ?>


<!-- 3 STEP CONTENT -->
<div class="fh5co-section section3 landing-3-column">
  <div class="container">
    <div class="row">
      <?php if( have_rows('3_column') ) {
        $i = 0;
        while ( have_rows('3_column') ) : the_row(); 
          $image = get_sub_field('step_image');
          $i++; ?>
          <div class="col-md-4 col-sm-12">
            <figure>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              <div class="step"><?php echo $i; ?></div>
              <figcaption>
                <h4><?php the_sub_field('step_title') ?></h4>
                <p><?php the_sub_field('step_content') ?></p>
              </figcaption>
            </figure>
          </div> 
        <?php endwhile;
      } else {
          // NOTHING
      } ?>
    </div>
  </div>
</div>  


<!-- PRESS -->
<?php if ( is_page(12408) ) { ?>
<?php //Groomsmen Gifts ?>
  <div class="fh5co-parallax section4" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <h2 class="text-center">Press</h2>
        <div class="row pressFullScreen">
        <?php if( have_rows('press_logos', 'options') ) {
          $i = 0;
          while ( have_rows('press_logos', 'options') ) : the_row(); 
            $image = get_sub_field('logo'); $i++; ?>
            <div class="text-center press">
              <a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
            </div>
            <?php if ( $i == 7 ) { ?>
              </div>
              <div class="row pressFullScreen">
            <?php } ?>
          <?php endwhile;
        } else {
          // NOTHING
        } ?>
      </div>
    </div>
  </div>
<?php } ?>


<?php get_footer(); ?>