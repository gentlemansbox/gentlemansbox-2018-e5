<?php
/**
 * Template Name: Secret Agent
 */
 ?>

  <?php 
 //
 // IS THIS TEMPLATE NEEDED? | IF SO UPDATE INTO TEMPLATE PARTS | MAYBE REDESIGN
 //
 ?>



<!DOCTYPE html>
<html>
  <head>
    <title>The Gentleman's Box</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1">
    <meta name="google" value="notranslate">
    <link rel='stylesheet' id='reset-style-css'  href='https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/css/reset.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mainstyle-style-css'  href='https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/css/mainstyle.css' type='text/css' media='all' />
    <style>
       /* body {
          background: #000;
          color: #fff;
        }

        article.post p {
          color: #fff;
        }*/
    </style>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/fontbomb/qcc5ovd.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  </head>
  <body>

    <?php //Secret Query
    $secret_query = new WP_Query("showposts=1&post_type=page&page_id=406");
    while ($secret_query->have_posts()) : $secret_query->the_post();
    $do_not_duplicate = $post->ID; ?>
      <article class="post">
        <?php the_content(); ?>
      </article>
    <?php endwhile; wp_reset_query(); ?>



  </body>
</html>
<script>
window.FONTBOMB_HIDE_CONFIRMATION = true;
(function () {try{var s = document.createElement('script');s.setAttribute('src', '<?php bloginfo('template_url'); ?>/library/js/fontbomb/main.js');document.body.appendChild(s);}catch(err){alert("Your browser is not compatible, watch the video or try with Chrome.")}}());
</script>
