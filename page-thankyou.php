<?php
/**
 * Template Name: Thank You
 */
 get_header(); ?>

 <?php 
 //
 // IS THIS TEMPLATE NEEDED? | WHY NOT USE PAGE.PHP AND ADD BING TO FOOTER WITH IF STATEMENT
 //
 ?>


<div class="wrap">
	
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</article>
	<?php endwhile; endif; ?>
</div>

<!-- Bing -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4044369"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4044369&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<?php get_footer(); ?>
