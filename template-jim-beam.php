<?php /*
Template Name: Jim Beam
*/ ?>

<!--                                   -->
<!-- ADD TO HEADER.PHP DURING CLEAN UP -->
<!--                                   -->

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php wp_head(); ?>

<!-- FAVICON -->
  <link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- MOBILE SITE MEDIA QUERY -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- MAIN CSS -->
  <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/base-theme.min.css" />
<!-- ANIMATIONS CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.min.css" />
<!-- SLICK SLIDER -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick.min.css" />
<!-- GRAVITY FORMS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/formsmain.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/readyclass.min.css" />
<!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
  
<style type="text/css">
  .page-id-485188 .landing-top video {
    height: auto;
    min-height: 100%;
    min-width: 100%;
    width: auto;
    position: absolute;
}
.page-id-485188 .main-nav a img {
    background-color: transparent;
}
.page-id-485188 .landing-top .videoContainer a h1 {
    position: relative;
    font-family: 'Playfair Display', serif;
    text-align: center;
    margin: 40px 20% 100px 20%;
}
.page-id-485188 .landing-top .videoContainer a.primary-button {
    position: relative;
    margin: 0 auto;
    left: 50%;
    margin-left: -76px;
}
.page-id-485188 .landing-top .videoContainer.jim-beam-background {
    background-image: url('https://c3sgentleman.staging.wpengine.com/wp-content/themes/gentlemansbox/library/images/barrel_background.png');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    height: 100%;
}
.page-id-485188 .landing-top .videoContainer.jbb-background {
    background-image: url('https://c3sgentleman.staging.wpengine.com/wp-content/themes/gentlemansbox/library/images/black_wood_grain.png');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    height: 100%;
}
.page-id-485188 header.main-nav .social-media svg {
    margin-top: 4px;
}
.page-id-485188 footer#footer nav.footer-nav a {
    line-height: 0;
}
.page-id-485188 .landing-top .videoContainer.jbb-background a.primary-button {
    display: block;
    text-align: center;
    left: 20%;
    width: 440px;
    height: 80px;
    padding-top: 30px;
    margin-top: 50px;
    font-size: 1em;
    letter-spacing: 2px;
    margin-left: 5px;
}
.page-id-485188 .gifts {
    background-image: none;
    padding-bottom: 0;
    padding-top: 0;

}
.page-id-485188 .gifts a {
    max-width: 100%;
}
.page-id-485188 .gifts .gift-item {
    padding: 0;
}
.page-id-485188 p.gift-code {
    text-transform: uppercase;
    color: #fff;
    background-color: #AB8461;
    margin: 0;
    height: 40px;
    padding-top: 10px;
}
.page-id-485188 .gifts .gift-item img {
    margin: 0;
    max-height: 1000px;
    width: 100%;
}
.page-id-485188 .work-purpose, .page-id-485188 .gifts {
    background-image: url('https://c3sgentleman.staging.wpengine.com/wp-content/themes/gentlemansbox/library/images/wood-white.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
}
.page-id-485188 .work {
    background-color: #fff;
    margin: 100px 50px;
    padding: 50px;
    width: auto;
}
.page-id-485188 .work .one-half-full:last-child {
    background-color: transparent;
    padding: 0;
} 
.page-id-485188 .work .one-half-full:last-child::after {
    display: none;
}
.page-id-485188 footer img {
    display: block;
    margin-left: 7%;
}
.page-id-485188 footer#footer #copyright {
    line-height: 25px;
    max-width: 30%;
    text-align: center;
}
.page-id-485188 footer#footer svg#logo-white {
    top: 65px;
}
@media(max-width: 1200px) {
    .page-id-485188 .landing-top .videoContainer a h1 {
        margin-bottom: 50px;
    }
    .page-id-485188 .landing-top .videoContainer.jbb-background a.primary-button {
        left: 0;
    }
    .page-id-485188 footer#footer nav.footer-nav {
        float: none;
    }
    .page-id-485188 footer .footer-nav p {
        max-width: 40% !important;
    }
}
@media(max-width: 800px){
    .page-id-485188 .landing-top video {
        display: none;
    }
    .page-id-485188 .landing-top .background.clearfix {
        background-image: url('https://c3sgentleman.staging.wpengine.com/wp-content/themes/gentlemansbox/library/images/jbb_mobile_header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
    }
    .page-id-485188 .work-purpose, .page-id-485188 .gifts {
        padding: 0;
    }
    .page-id-485188 p.gift-code {
        height: 70px;
        padding-top: 15px;
    }
    .page-id-485188 .landing-top .videoContainer.jbb-background .one-half-full {
        width: 100%;
        float: none;
    }
    .page-id-485188 .landing-top .videoContainer.jbb-background .one-half-full img {
        margin: 60px 27% 30px 27%!important;
    }
    .page-id-485188 .landing-top:last-of-type {
        height: 620px !important;
    }
    .page-id-485188 .landing-top .videoContainer.jbb-background a.primary-button {
        margin-top: 15px !important;
        width: 300px;
        position: relative;
        left: 50%;
        margin-left: -150px;
        padding-top: 20px;
    }
    .page-id-485188 footer img {
        position: relative;
        left: 50%;
        margin-left: -88px;
        margin-top: 75px;
    }
    .page-id-485188 footer#footer #copyright {
            line-height: 25px;
    max-width: 90%;
    text-align: center;
    position: relative;
    padding-top: 15px;
    margin: 0 auto;
    }
    .page-id-485188 footer .footer-nav p {
            max-width: 90% !important;
    float: none !important;
    position: relative;
    margin: 0 auto;
    }
}
@media(max-width: 500px) {
    .page-id-485188 header.main-nav {
        padding-top: 50px;
    }
    .page-id-485188 .work {
        margin: 0;
    }
    .page-id-485188 .work-purpose .one-half-full {
        min-height: 200px;
    }
    .page-id-485188 .landing-top .videoContainer.jbb-background .one-half-full img {
        margin: 60px 25% 30px 7% !important;
    }
    .page-id-485188 .landing-top .videoContainer.jim-beam-background a.primary-button {
        margin-left: -106px;
    }
    .page-id-485188 p.gift-code {
        padding-top: 25px;
    }
    .page-id-485188 .landing-top .videoContainer a h1 {
        margin-left: 0;
        margin-right: 0;
    }
}
</style>
</head>

<body <?php body_class(); ?>>

<!--                                    -->
<!-- /ADD TO HEADER.PHP DURING CLEAN UP -->
<!--                                    -->


<!--                           -->
<!-- TEXT LANDING PAGE CONTENT -->
<!--                           -->

<!-- NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'landing-page-header' ); ?>

<main class="full-width">
  <section class="landing-top">
    <div class="background clearfix">
      <div class="videoContainer fh5co-hero">
        <div class="overlay"></div>
        <div class="videoInner">
          <video preload="true" autoplay muted loop volume = "0" poster = "" >
            <source src="https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/images/Beam-Roll.mp4" type="video/mp4">
            <source src="https://gentlemansbox.com/wp-content/themes/gentlemansbox/library/images/Beam-Roll.WebM" type="video/webm">
          </video>
        </div>
        <a href="https://gentlemansbox.com/" class="max-width">
          <h1 style="margin-top: 200px;">Elevate Your Style with Gentleman's Box</h1>
        </a>
        <a href="/join/" class="primary-button">JOIN NOW</a>
      </div>
    </div>
  </section>
  <section class="work-purpose" style="margin-top: 0;">
    <div class="work">
      <div class="one-half-full">
        <img src="http://gentlemansbox.com/wp-content/uploads/May2017transparent-3.jpg">
      </div>
      <div class="one-half-full vertical-align-parent">
        <div class="vertical-align-content">
          <h2>What is Gentleman's Box?</h2>
          <?php the_field('how_it_works'); ?>
        </div>
      </div>
    </div>
    </section>
    <section class="gifts">
        <?php if( have_rows('gifts') ) { ?>
          <div class="clearfix full-width">
            <?php while ( have_rows('gifts') ) : the_row(); ?>
             <a href="<?php the_sub_field('gift_title'); ?>"> 
              <div class="gift-item">
                <?php $image = get_sub_field('gift_image'); ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <p class="gift-code"><?php the_sub_field('gift_code'); ?></p>
              </div>
             </a> 
          <?php endwhile; ?>
        </div>
      <?php } else {
          //nothing
        }
      ?>
  </section>

  <section class="landing-top" style="margin-top: 0; height: 440px;">
    <div class="background clearfix ">
      <div class="videoContainer fh5co-hero jim-beam-background">
        <a href="http://www.jimbeam.com/en-us/bourbons/black" target="_blank" class="max-width">
          <h1>Elevate Your Taste with Jim Beam Black &reg;</h1>
        </a>
        <a href="http://www.jimbeam.com/en-us/bourbons/black" target="_blank" class="primary-button">VISIT JIM BEAM</a>
      </div>
    </div>
  </section>   
  <section class="give-try dark-bg red-background">
    <h2>Ready to sign up?</h2>
    <a href="/join/" class="primary-button" style="background-color: #AB8461;border-color: #AB8461;">JOIN NOW</a>
  </section>
  <section class="landing-top" style="margin-top: 0; height: 440px;">
    <div class="background clearfix">
      <div class="videoContainer fh5co-hero jbb-background">
        <div class="one-half-full">
          <img src="<?php bloginfo('stylesheet_directory'); ?>/library/images/jbb_gbox.png" style="height: 300px;width: auto;margin: 60px 20%;">
        </div>
        <div class="one-half-full">
           <a href="/jim-beam-black-recipes/" class="primary-button" style="margin-top:100px;">JIM BEAM BLACK RECIPES</a>
            <a href="/join/" class="primary-button">PURCHASE THE MAY GENTLEMAN'S BOX</a>
        </div>
      </div>
    </div>
  </section>   

<!-- FOOTER -->
  <?php get_template_part( 'template-parts/content', 'landing-page-footer' ); ?>

<!--                           -->
<!-- /TEXT LANDING PAGE CONTENT -->
<!--                           -->

<!--                                   -->
<!-- ADD TO FOOTER.PHP DURING CLEAN UP -->
<!--                                   -->

<!-- SMOOTH SCROLLING -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.min.js"></script>
<!-- STICKY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
<!-- ENTRANCE ANIMATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- SLICK SLIDERS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>
<!-- DECLARATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/declarations.min.js"></script>
  
</body>

<?php wp_footer(); ?>

</html>

<!--                                    -->
<!-- /ADD TO FOOTER.PHP DURING CLEAN UP -->
<!--                                    -->