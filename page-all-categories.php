<?php
/*
 * Template Name: All Categories
 */
 get_header(); ?>

 <?php 
 //
 // IS THIS TEMPLATE NEEDED? | REMOVE AND USE BLOG/ARCHIEVES
 //
 ?>

 <div class="wrap blog">

	<div class="left">

	<?php 
	query_posts('posts_per_page=10'); //Display All Posts
	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		<?php if ( has_post_thumbnail() ) { ?>
			<figure class="featured">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog'); ?></a>
			</figure>
			<?php } else { ?>
			<figure>
				<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/library/images/blog-placeholder.jpg" alt="<?php the_title(); ?>" /></a>
			</figure>
			<?php } ?>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<p class="date"><?php the_time('F j, Y'); ?></p>
			<div class="entry">
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="btn maroon-transparent">Read More</a>
			</div>
	</article>

	<?php endwhile; ?>

	<?php post_navigation(); ?>
	<?php else : ?>
		<h2><?php _e('Nothing Found','html5reset'); ?></h2>
	<?php endif; ?>
	</div><!--.left-->

	<div class="right">
		
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Widgets') ) : ?><?php endif; ?>
		

	</div><!--.right-->

</div>

<?php get_footer(); ?>
