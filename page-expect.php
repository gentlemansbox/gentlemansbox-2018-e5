<?php
/**
 * Template Name: What to Expect
 */
 get_header(); ?>


 <?php 
 //
 // IS THIS TEMPLATE NEEDED? | CONVERT TO TEMPLATE PARTS
 //
 ?>

<section id="content-top">
	<div class="wrap">
		<article class="post">
			<div class="entry">
				<?php if( get_field('content_area') ): ?>
					<?php the_field('content_area'); ?>
				<?php endif; ?>
			</div>
		</article>
	</div>
</section>

<section id="content-bottom">
	<div class="wrap">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile; endif; ?>
	</div>
</section>

<?php get_template_part('library/partials/howitworks'); ?>

<?php get_footer(); ?>
