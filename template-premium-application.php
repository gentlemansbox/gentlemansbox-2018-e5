<?php /*
Template Name: Premium Aplication
*/

get_header(); ?>

<script>
var count = 600;
var redirect = "https://gentlemansbox.com/premium-membership-application/";
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
        if(count > 0){
            count--;
        }else{
            window.location.href = redirect;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 10,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};

</script>

<main class="full-width">
  <section class="landing-top">
    <div class="background clearfix">
      <div class="full-bg full-width" style="background-image:url(<?php the_field('full_image'); ?>);">
      </div>
    </div>
    <div class="page-title vertical-align-parent">
      <div class="vertical-align-content">
        <?php the_field('main_title'); ?>
      </div>
    </div>
  </section>
  <!-- PAGE CONTENT -->
  <?php if ( $post->post_content=="" ) { ?>
    <?php // NOTHING ?>
  <?php } else { ?>
       <?php the_content(); ?>
  <?php } ?>
</main>
<?php get_footer('new'); ?>
