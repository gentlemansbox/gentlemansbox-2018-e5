var $ = jQuery;
var $loaderBlog = $('.post-preview-container .blog-loader-contain').hide();

$(document)
  .ajaxStart(function() {
    $loaderBlog.show();
  })

.ajaxStop(function() {
  $loaderBlog.hide();
});

//////*****BLOG SETTINGS*****/////////
////Update these settings to accomidate blog////
var postURL = 'https://gentlemansbox.com/wp-json/wp/v2/posts'; //main URL for requests.
var postGetLimit = 12; //set the number of posts to be pulled. 
var lastPostIndex = 12; //use this to increase or decrease the number of posts to display.
/////////////////////////////////////////////////////////////////


var postGetURL = '?_embed&per_page='; // WordPress protocol to get posts ***DO NOT CHANGE THIS UNLESS YOU ARE SURE!!****
var postPageFilter = '&page=';
var postCategoryFilter = '&categories=';
var postFilters = [];
var currentLoadMore = 0;
var currentPageFilter;
var currentCategoryId;
var defaultImgUrl;

var queryUrl; 
var sucessUrl;
var checkBody = $("body").hasClass("category");

if(checkBody){
  currentCategoryId = $('.post-preview-container').attr('data-category');
  queryUrl = postURL + postGetURL + postGetLimit + postCategoryFilter + currentCategoryId;
  successUrl = postURL + postGetURL + postGetLimit + postCategoryFilter + currentCategoryId + postPageFilter;
  defaultImgUrl = '../../wp-content/themes/gentlemansbox/library/images/default-thumb.png';
} else{
  queryUrl = postURL + postGetURL + postGetLimit;
  successUrl = postURL + postGetURL + postGetLimit + postPageFilter;
  defaultImgUrl = '../wp-content/themes/gentlemansbox/library/images/default-thumb.png';
}
console.log(queryUrl);
getAllPosts();

////Global Click events///////
$(document).on('click', 'a.load-more-btn', function() {
  currentPageFilter++;
  loadMorePosts();
});

/////End Global Click Events//////

function getAllPosts() {
  currentPageFilter = 1
  $.ajax({
    "async": true,
    "crossDomain": true,
    "url": queryUrl, //used _embed in order to get author/image/and category data
    "method": "GET",
    "cache": false,
    "success": function(data, status, request) {
      allSuccessResponse(request.getResponseHeader('X-WP-TotalPages'));
    }
  });
}

function loadMorePosts() {
  $.ajax({
    "async": true,
    "crossDomain": true,
    "url": postURL + postGetURL + postGetLimit, //used _embed in order to get author/image/and category data
    "method": "GET",
    "cache": false,
    "success": function(data, status, request) {
      allSuccessResponse(request.getResponseHeader('X-WP-TotalPages'));
    }
  });
}

function allSuccessResponse(totalPages) {
  $('.load-more-contain').remove();
  console.log(successUrl + currentPageFilter);
  $.ajax({
    "async": true,
    "crossDomain": true,
    "url": successUrl + currentPageFilter, //used _embed in order to get author/image/and category data
    "method": "GET",
    "cache": true,
    "success": function(response) {
      var blogGrid = $('.post-preview-container');
      var loadMoreButton = $('<div class="load-more-contain"><a class="btn load-more-btn">Load More Posts</a></div>');

      response.forEach(function(post) {

        //build the initial cachable and easier to manage post object. 
        var postObject = {
          id: post.id,
          link: post.link,
          title: post.title.rendered,
          date: prettyDate(post.date),
          excerpt: limitString(post.excerpt.rendered),
          featuredImage: featuredImageCall(post)
        };

        ///begin dom build
        var elem = $('<div class="post-contain fadeIn"></div>');
        var elemLink = $('<a href="' + postObject.link + '"></a>');
        var elemTitle = $('<div class="post-head"><div class="date">' + postObject.date + '</div></div>').css('min-height', postObject.featuredImageSize);
        var elemBackground = $('<div class="background-image"></div>').css('background-image', 'url(' + postObject.featuredImage + ')');
        var elemExcerpt = $('<div class="post-content"><h2>' + postObject.title + '</h2>' + postObject.excerpt + '<span> Read More...</span></div>');

        //Build blog items
        $(elemLink).prepend(elemExcerpt);
        $(elemLink).prepend(elemTitle);
        $(elem).prepend(elemLink);
        $(elemTitle).prepend(elemBackground);
        $(blogGrid).append(elem);

      });
      //add load more button if # of posts exceeds lastPostIndex defined number
      if (currentPageFilter < totalPages) {
        $(blogGrid).append(loadMoreButton);
      }
    }
  });
}

function featuredImageCall(post) {
  var fmCheck = post._embedded['wp:featuredmedia'];
    if(fmCheck && fmCheck[0].media_details === undefined){
      return defaultImgUrl;
    }
    
    if (fmCheck) {
      if (fmCheck[0].media_details.sizes.medium) {
        return fmCheck[0].media_details.sizes.medium.source_url;
      } else if (fmCheck[0].media_details.sizes.thumbnail) {
        return fmCheck[0].media_details.sizes.thumbnail.source_url;
      }
    }

    return defaultImgUrl;
}

function prettyDate(date) {
  return moment(date).format('MMMM D');
}

//clears container of posts
function clearContainer() {
  $('.post-contain').remove();
  currentPageFilter = 1;
}

//limit post excerpts to 35 words
function limitString(data) {
  var excerptString = data;
  return excerptString.split(" ").splice(0, 50).join(" ");
}