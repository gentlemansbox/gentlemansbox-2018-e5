/* DECLARATIONS FOR ANIMATIONS, STICKY PORTIONS, SLIDERS, ETC. | ONLY MINIFIED VERSIONS ARE CALLED IN THEME */
/*ENTRANCE ANIMATIONS*/
jQuery(document).ready(function() {
    jQuery(".classorid").addClass("hidden").viewportChecker({
        classToAdd: "visible animated fadeIn",
        offset: 100
    });
});
/* STICKY PORTIONS*/
jQuery(document).ready(function() {
    var window_width = jQuery(window).width();
    if (window_width < 750) {
        //DISABLE STICKY PARTS ON MOBILE
        jQuery("#sticky-nav").trigger("sticky_kit:detach");
        jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
    } else {
        //ENABLE STICKY PARTS
        jQuery("#sticky-nav").stick_in_parent({
            parent: "body"
        });
        jQuery("#styleguide-sidebar").stick_in_parent({
            offset_top: 125
        });
    }
    jQuery(window).resize(function() {
        window_width = jQuery(window).width();
        if (window_width < 750) {
            //DISABLE STICKY PARTS ON MOBILE
            jQuery("#sticky-nav").trigger("sticky_kit:detach");
            jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
        } else {
            //ENABLE STICKY PARTS
            jQuery("#sticky-nav").stick_in_parent({
                parent: "body"
            });
            jQuery("#styleguide-sidebar").stick_in_parent({
                offset_top: 125
            });
        }
    });
});
/* SLICK SLIDER */
 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.slider-nav',
  autoplay: true,
  autoplaySpeed: 6000,
});
$('.slider-nav').slick({
  slidesToShow: 12,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});
/* SHOP SLIDER */
$('.shop-slider').slick({
  autoplay: true,
  autoplaySpeed: 6000,
  arrows: false,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true
});
/*VALENTINES DAY BLOG SLIDER*/
 $('.vday-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  autoplay: true,
  autoplaySpeed: 6000,
});

