<?php
$cssFiles = array(
	'slick.min.css',
	'animate.min.css',
	'bootstrap.min.css',
	'icomoon.min.css',
	'formsmain.min.css',
	'readyclass.min.css',
	'style.min.css',
	'new-home.css',
	'new-blog.css'
);
/**
 * Ideally, you wouldn't need to change any code beyond this point.
 */
$buffer = "";
foreach ($cssFiles as $cssFile) {
  $buffer .= file_get_contents($cssFile);
}
// Remove comments
$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
// Remove space after colons
$buffer = str_replace(': ', ':', $buffer);
// Remove whitespace
$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
// Enable GZip encoding.
ob_start("ob_gzhandler");
// Enable caching
header('Cache-Control: public');
// Expire in one day
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400) . ' GMT');
// Set the correct MIME type, because Apache won't set it for us
header("Content-type: text/css");
header("Cache-Control: max-age=2592000"); //30days (60sec * 60min * 24hours * 30days)

// Write everything out
echo($buffer);
?>
