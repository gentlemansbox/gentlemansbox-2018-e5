    <script src="http:/=a˛vmtml5shim.googlecode.com/svn/trunk/html5.js"></script>
<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">

<?php
  $current_user = wp_get_current_user();
  $user_id = $current_user->ID;
  $user_email = $current_user->user_email;
  $user_firstname = $current_user->user_firstname;
  $user_lastname = $current_user->user_lastname;
?>

<!-- FRIENDBUY SMART PIXEL -->
  <script>
    window['friendbuy'] = window['friendbuy'] || [];
    window['friendbuy'].push(['site', 'site-5d30cac8-gentlemansbox.com']);
    window['friendbuy'].push(['track', 'customer',
        {
            id: '<?php echo $user_id; ?>', //INSERT CUSTOMER ID PARAMETER
            email: '<?php echo $user_email; ?>', //INSERT CUSTOMER EMAIL PARAMETER
            first_name: '<?php echo $user_firstname; ?>', //INSERT CUSTOMER FIRST NAME PARAMETER
            last_name: '<?php echo $user_lastname; ?>' //INSERT CUSTOMER LAST NAME PARAMETER
        }
    ]);
    (function (f, r, n, d, b, y) {
        b = f.createElement(r), y = f.getElementsByTagName(r)[0];b.async = 1;b.src = n;y.parentNode.insertBefore(b, y);
    })(document, 'script', '//djnf6e5yyirys.cloudfront.net/js/friendbuy.min.js');
  </script>
<!-- FRIENDBUY SMART PIXEL -->

<!-- SNAPCHAT PIXEL -->
<script type='text/javascript'>
(function(win, doc, sdk_url){
if(win.snaptr) return;
var tr=win.snaptr=function(){
tr.handleRequest? tr.handleRequest.apply(tr, arguments):tr.queue.push(arguments);
};
tr.queue = [];
var s='script';
var new_script_section=doc.createElement(s);
new_script_section.async=!0;
new_script_section.src=sdk_url;
var insert_pos=doc.getElementsByTagName(s)[0];
insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
})(window, document, 'https://sc-static.net/scevent.min.js');

snaptr('init','1feb6fbf-4e21-4159-8d2b-beff64a085ba',{
'user_email':'<?php echo $user_email; ?>'
})
snaptr('track','PAGE_VIEW')
</script>
<!-- SNAPCHAT PIXEL -->

<!-- Start Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

 (function() {
   var elem = document.createElement('script');
   elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
   elem.async = true;
   elem.type = "text/javascript";
   var scpt = document.getElementsByTagName('script')[0];
   scpt.parentNode.insertBefore(elem, scpt);
  })();

_qevents.push({qacct: "p-82-8dhfPge5nN"});

</script>
<noscript>
 <img src="//pixel.quantserve.com/pixel/p-82-8dhfPge5nN.gif?labels=_fp.event.Default" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
</noscript>
<!-- End Quantcast tag -->

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <!--[if IE ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php if ( is_search() ) { echo '<meta name="robots" content="noindex, nofollow" />'; } ?>

    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.png">



    <!---                        -->
    <!--- WHAT IS THIS | REMOMVE -->
    <!---                        -->
        <meta name="google-site-verification" content="wR2EUsYVd5w3rq-K6svDz8l6FH_16EG7-2dDi5DK52Q" />
        <?php
            if (true == of_get_option('meta_author')) {
                echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';
            }
            if (true == of_get_option('meta_google')) {
                echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
            }
        ?>
        <meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!---                         -->
    <!--- /WHAT IS THIS | REMOMVE -->
    <!---                         -->

  <!--                                            -->
    <!-- MINIMIZE CONTENT SHOULDNT NEED ALL OF THIS -->
    <!--                                            -->
        <!--Apple Mobile Web App Metadata-->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch4.png" />
        <link rel="apple-touch-startup-image" media="(max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch5.png" />
        <link rel="apple-touch-startup-image" media="(device-width: 375px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6.png">
        <link rel="apple-touch-startup-image" media="(device-width: 414px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6plus.png">
        <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-landscape.png"  />
        <link rel="apple-touch-startup-image"  media="(device-width: 768px) and (device-height: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-portrait.png"  />
        <link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/library/images/touch-icon.png">
        <link rel="profile" href="//gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <!--                                             -->
    <!-- /MINIMIZE CONTENT SHOULDNT NEED ALL OF THIS -->
    <!--                                             -->

    <!--                       -->
    <!-- WHAT IS THIS | REMOVE -->
    <!--                       -->
        <?php if ( is_page(406) ){ ?>
            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/fontbomb/qcc5ovd.js"></script>
            <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        <?php } ?>
    <!--                        -->
    <!-- /WHAT IS THIS | REMOVE -->
    <!--                        -->

<?php wp_head(); ?>

<!--                                  -->
<!-- CSS FILES | MINIMIZE/GROUP FILES -->
<!--                                  -->
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
    <!-- SLICK SLIDER -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/loadCss.php" />
    <!-- ANIMATIONS CSS -->

    <!-- Font Awesome -->
    <script src="https://use.fontawesome.com/b1431baa1d.js"></script>
<!--                                  -->
<!-- CSS FILES | MINIMIZE/GROUP FILES -->
<!--                                  -->
<script>
$(document).ready(function(){
  $(".accordion").click(function(){
    $(this).siblings(".panel").toggle();
  });
});

</script>
</head>

<body <?php body_class(); ?>>

<!-- Hotjar Tracking Code for gentlemansbox.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:72091,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '726903087431698'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=726903087431698&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->

<?php $host = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if($host === "gentlemansbox.com/checkout/"){
echo "<script type='text/javascript'>
fbq('track', 'InitiateCheckout');
</script>";
} else {
}?>

<!-- End Facebook Pixel Code -->

<!-- CONTACT THANK YOU PAGE -->
<?php if(is_page(145)){ ?>
<!-- Facebook Subscription Pixel -->
<script>fbq('track','Contact');</script>
<?php } else {
	//nothing
} ?>


<!-- Ignite UTT -->
    <script type="text/javascript"> (function(a,b,c,d,e,f,g){e['ire_o']=c;e[c]= e[c]||function(){(e[c].a=e[c].a||[]).push(arguments)};f=d.createElement(b);g=d.getElementsByTagName(b)[0];f.async=1;f.src=a;g.parentNode.insertBefore(f,g);})('//d.impactradius-event.com/A343365-2ef3-4ed5-965f-ab1776af0d261.js','script','ire',document,window); </script>
    <noscript><img src="//tl.r7ls.net/unscripted/XXXXX" width="1" height="1"></noscript>
    <script type="text/javascript">!function(){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//www.rtb123.com/tags/fbb165cc-ecbc-4e5a-9456-bcc6f3b03060/btp.js";var t=document.getElementsByTagName("head")[0];t?t.appendChild(e,t):(t=document.getElementsByTagName("script")[0]).parentNode.insertBefore(e,t)}();</script>
<!-- End Ignite UTT -->

<!-- BEGIN YAHOO CODE -->
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10023422'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10024796'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<script type="application/javascript">
window.dotq = window.dotq || [];
window.dotq.push({
'projectId': '10000',
'properties': {
'pixelId': '10023422',
'qstrings': {
   'et': 'custom',
   'gv': '{{OrderSubTotal}}'
}
}
}); </script>
<!--END YAHOO CODE -->

<!-- BEGIN BING CODE -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5600748"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
<!-- END BING CODE -->

<!-- PINTEREST PIXEL -->
<script type="text/javascript">
!function(e){if(!window.pintrk){window.pintrk=function(){window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var n=window.pintrk;n.queue=[],n.version="3.0";var t=document.createElement("script");t.async=!0,t.src=e;var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");

pintrk('load','2620030105983');
pintrk('page');
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt=""
src="https://ct.pinterest.com/v3/?tid=2620030105983&noscript=1" />
</noscript>
<script type="text/javascript">
pintrk('track', 'pagevisit');
</script>
<!-- PINTEREST PIXEL -->

<!-- LINKEDIN PIXEL -->
<script type="text/javascript">
_linkedin_data_partner_id = "66289";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=66289&fmt=gif" />
</noscript>
<!-- LINKEDIN PIXEL -->

<!-- OUTBRAIN PIXEL -->
<script data-obct type="text/javascript">
  !function(_window, _document) {
    var OB_ADV_ID='00c9014a0c2da8e90a8536a7dc62d43e0b';
    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');
    tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
obApi('track', 'PAGE_VIEW');
</script>
<!-- OUTBRAIN PIXEL -->

	<!-- PAGE NAVIGATION -->
	<header id="header" role="banner" class="newheader<?php if ( current_user_can('subscriber') ) : ?> subscriber<?php endif; ?>">
<script>
    window['friendbuy'] = window['friendbuy'] || [];
    window['friendbuy'].push(['widget', "dz7-mwy"]);
</script>
<!-- <a href="#" class="friendbuy-dz7-mwy">Invite Friends</a> -->

    <div id="top">
			<nav class="navbar navbar-default">
                <div class="newhome container-fluid">
                    <div class="navbar-header">
                            <!-- MOBILE MENU -->
                        <div class="mobileLogin">
                  <?php if ( is_user_logged_in() ) { ?>
                      <div class="contentloggedin">
                        <span class="myaccount"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>">Account</a></span>
                      </div>
                  <?php } else { ?>
                      <a href="https://gentlemansbox.com/my-account/">Login</a>
                  <?php } ?>
                        </div>
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span>MENU</span>
                      </button>
                      <!-- HEADER LOGO -->
                            <a class="navbar-brand" href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/mini_logo.svg" alt="Gentleman's Box Logo"></a>
                    </div>

                    <!-- MAIN NAVIGATION -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <?php  wp_nav_menu( array('menu' => 'Primary Menu') ); ?>
    <!-- 			        <span class="hello" style="line-height: 2;color:#fff;margin-left: 6%;margin-right: -6%;">Call: 248-479-6677</br>Text: 248-479-6688</span>
     -->			      </ul>
                      <ul class="nav navbar-nav navbar-right navbarnav__desc">
                        <div id="account">
                                    <?php if ( is_user_logged_in() ) { ?>
                                        <div class="contentloggedin">
                                            <span class="hello" style="line-height: 20px;margin-top: 1.6em;margin-left: -7%;margin-right: 6%;">Call: 248-479-6066 </br> Text: 248-479-6688</span>
                                            <span class="logout"><a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" title="Logout">Logout</a></span>
                                            <span class="myaccount"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></span>
                                        </div>
                                    <?php }
                                    else { ?>
                                        <div class="contentloggedout">
                                            <span class="hello" style="line-height: 20px;margin-top: 1.6em;margin-left: -7%;margin-right: 6%;">Call: 248-479-6066 </br> Text: 248-479-6688</span>
                                            <span><a class="login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login Here','woothemes'); ?>">
                                                <?php _e('<span class="log-in">Login&nbsp;</span> <span class="here">Here</span>','woothemes'); ?>
                                            </a></span>
                                        </div>
                                    <?php } ?>
                                    <?php global $woocommerce; ?>
                                        <span onClick="window.location.href='<?php echo $woocommerce->cart->get_cart_url(); ?>'" class="cart-contents">
                                        <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
                                            <span class="total"><?php echo $woocommerce->cart->get_cart_total(); ?></span>
                                            <span class="countmobile"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
                                        </a>
                                        </span>
                                </div>
                      </ul>
                      <ul class="nav navbar-nav navbar-right navbarnav__mob">
                        <div id="account">
                            <?php global $woocommerce; ?>
                            <span onClick="window.location.href='<?php echo $woocommerce->cart->get_cart_url(); ?>'" class="cart-contents"><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><span class="total"><?php echo $woocommerce->cart->get_cart_total(); ?></span><span class="countmobile"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span></a></span>
                            <?php if ( is_user_logged_in() ) { ?>
                                <div class="contentloggedin">
                                    <span class="logout"><a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" title="Logout">Logout</a></span>
                                    <span class="myaccount"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></span>
                                    <span class="hello" style="line-height: 20px;margin-top: 1.6em;margin-left: -7%;margin-right: 6%;">Call: 248-479-6066 </br> Text: 248-479-6688</span>
                                </div>
                            <?php }
                            else { ?>
                                <div class="contentloggedout">
                                    <span class="nluser"><a class="login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login Here','woothemes'); ?>"><?php _e('Login Here','woothemes'); ?></a></span>
                                    <span class="hello" style="line-height: 20px;margin-top: 1.6em;margin-left: -7%;margin-right: 6%;">Call: 248-479-6066 </br> Text: 248-479-6688</span>
                                </div>
                            <?php } ?>
                        </div>
                      </ul>
                    </div>
			    </div>
			</nav>
      <?php if(is_page(6) || is_checkout() || is_page(711944)) {
        //nothing - exluding from checkout, order received and valentines-2 pages
      } else { ?>
      <!-- <div class="red-banner">
      <p id="free-shipping-message">Valentines Day Is Here. Save Up to 45%! <a href="/valentines-day/" style="color:#fff;text-decoration:underline;">SHOP</a></p>
       </div> -->
      <?php } ?>
		</div>
	</header>

    <div class="newheader_t">
        <!-- PAGE TOP = PAGE TITLE & RANDOM BACKGROUND IAMGE -->
        <?php get_template_part('library/partials/page-top'); ?>
    </div>

<!-- REACHDYNAMICS PIXEL -->
<img class="nreachdynamics" src='https://rdcdn.com/rt?aid=10297&e=1&img=1' height='1' width='1' />
<!-- REACHDYNAMICS PIXEL -->



	<div id="fh5co-wrap">
